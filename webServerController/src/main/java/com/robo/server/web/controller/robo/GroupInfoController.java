package com.robo.server.web.controller.robo;

import com.common.base.CommConstants;
import com.common.base.exception.BusinessException;
import com.common.base.response.BaseResponseDto;
import com.common.em.CommResultEnum;
import com.common.utils.CommonUtils;
import com.common.utils.EmojiUtil;
import com.robo.server.web.base.WebServerController;
import com.robo.server.web.em.cache.CacheWebScoketEnum;
import com.robo.server.web.em.robo.GroupAuthorizeStatusEnum;
import com.robo.server.web.em.robo.GroupPersonChangeTypeEnum;
import com.robo.server.web.em.robo.RoboCurrentStatusEnum;
import com.robo.server.web.entity.robo.RoboBasicInfoDto;
import com.robo.server.web.entity.robo.group.GroupBasicInfoDto;
import com.robo.server.web.entity.robo.group.GroupChatContentDto;
import com.robo.server.web.entity.robo.group.GroupPersonChangeDto;
import com.robo.server.web.entity.robo.group.GroupPersonInfoDto;
import com.robo.server.web.entity.robo.response.GroupBasicInfoResponseDto;
import com.robo.server.web.entity.robo.response.GroupPersonResponseDto;
import com.robo.server.web.entity.user.UserLoginInfoDto;
import com.robo.server.web.message.RoboMessage;
import com.robo.server.web.mq.RabbitMQUtils;
import com.robo.server.web.robo.GroupInfoService;
import com.robo.server.web.robo.RoboInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;


/**
*
* @user jianghaoming
* @date 2017-07-11 22:16:36
*/
@RestController
@RequestMapping("/v1/robocat")
public class GroupInfoController extends WebServerController {

    private static final Logger _logger = LoggerFactory.getLogger(GroupInfoController.class);

    @Autowired
    private GroupInfoService groupInfoService;

    @Autowired
    private RoboInfoService roboInfoService;

    /**
     * 保存群组基本信息
     * {userId=jianghm, my_nickname=Nick, uin=null, token=667b1297-95fc-422c-87b4-ff050919b78b,
     * group_nickname_membercount=
     * [{MemberCount=9, NickName=财富云研发团队},
     * {MemberCount=9, NickName=财富云产品业务沟通团队},
     * {MemberCount=7, NickName=R&amp;D_ROBOT},
     * {MemberCount=4, NickName=测试专用}]}
     */
    @PostMapping("/group/info")
    public ResponseEntity<BaseResponseDto> groupInfo(@RequestBody Map<String, Object> paramMap) {
        try {

            _logger.info("===>" + paramMap);

            final String uin = getMapParamAndCheckNull("uin", paramMap);//微信uin
            final String ownerName = getMapParamAndCheckNull("my_nickname", paramMap);//昵称
            final Object groupObjet = paramMap.get("group_nickname_membercount");//群组列表


            _logger.info(" uin=[" + uin + "] ownerName=[" + ownerName + "]");

            //验证token，并同时获取用户信息
            UserLoginInfoDto user = getUserAndVerifyToken(paramMap);

            List<Map<String, Object>> groupList = new ArrayList<>();
            if (groupObjet != null) {
                groupList = (List<Map<String, Object>>) groupObjet;
            }else{
                return failResponse(CommConstants.OPERATOR_FAIL);
            }

            //查询机器人基本信息
            RoboBasicInfoDto roboBasicInfoDto = roboInfoService.getRoboBasicInfoByUin(user.getUserId(),uin,RoboCurrentStatusEnum.login_ing.getValue());

            //根据uin，查询所有群组信息
            List<GroupBasicInfoDto> datalist = groupInfoService.getAuthorizedGroupBasicInfoList(user.getUserId(),uin, "","", "");

            Map<String,GroupBasicInfoDto> dataMap = new HashMap<>(); //当前数据库中有的群组 key:groupName,value: groupDto

            for(GroupBasicInfoDto gDto : datalist){
                dataMap.put(gDto.getNickName(),gDto);
            }

            List<GroupBasicInfoDto> batchSaveList = new ArrayList<>();//需要保存和更新的list

            for (Map<String, Object> map : groupList) {
                String groupNickName = map.get("NickName")+"";
                if(!CommonUtils.isBlank(groupNickName)){
                    groupNickName = EmojiUtil.resolveToByteFromEmoji(groupNickName);
                }
                String isOwner = map.get("IsOwner")==null?"0":map.get("IsOwner")+"";

                GroupBasicInfoDto groupBasicInfoDto = null;

                //判断群组是否已经存在
                if(dataMap.containsKey(groupNickName)){
                    //存在
                    groupBasicInfoDto = dataMap.get(groupNickName);
                    if(!isOwner.equals(groupBasicInfoDto.getIsOwner())
                            || ownerName.equals(groupBasicInfoDto.getOwnerName()) ){
                        //当数据有更新的时候，更新
                        _logger.info("更新群组:" + groupNickName);
                        groupBasicInfoDto.setIsOwner(isOwner);
                        groupBasicInfoDto.setOwnerName(ownerName);
                        groupInfoService.saveGroupBasicInfo(groupBasicInfoDto);
                    }

                    dataMap.remove(groupNickName);


                }else{
                    //不存在，则添加
                    groupBasicInfoDto = new GroupBasicInfoDto();
                    _logger.info("新增群组:" + groupNickName);
                    groupBasicInfoDto.setGroupId(UUID.randomUUID().toString());
                    groupBasicInfoDto.setIsOwner(isOwner);
                    groupBasicInfoDto.setMemberCount(map.get("MemberCount")+"");
                    groupBasicInfoDto.setNickName(groupNickName);
                    groupBasicInfoDto.setUserId(user.getUserId());
                    groupBasicInfoDto.setAuthorizeStatus(GroupAuthorizeStatusEnum.NO.getValue());
                    groupBasicInfoDto.setOwnerName(ownerName);
                    groupBasicInfoDto.setPyQuanPin(map.get("PYQuanPin")+"");
                    groupBasicInfoDto.setUin(uin);
                    if(roboBasicInfoDto != null) {
                        groupBasicInfoDto.setRoboId(roboBasicInfoDto.getRoboId());
                    }
                    batchSaveList.add(groupBasicInfoDto);
                }

            }

            //群组删除
            _logger.info("群组删除 ＝＝》"+dataMap);
            for(Map.Entry<String,GroupBasicInfoDto> entry : dataMap.entrySet()){
                groupInfoService.deleteGroupBasicInfoById(entry.getValue().getId());
            }

            groupInfoService.saveGroupBasicInfo(batchSaveList);

            return succResponse(CommConstants.OPERATOR_SUCC,new HashMap<String, Object>());
        } catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message, ex);
            return failResponse(ex.getErrorCode(), message);
        } catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message, ex);
            return errorResponse(message);
        } catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }

    /**
     * 保存群组用户信息
     * api data:
     * {"groupInfo":{"第一个群":["康纳","Nick","张立海","Yoki Gao"],"第二个群":["康纳","Nick","张立海"]},
     * "token":"64d79e84-b03e-416e-8dd6-f6291349f4e2","userId":"jianghm"}
     */
    @PostMapping("/group/person")
    public ResponseEntity<BaseResponseDto> groupPerson(@RequestBody Map<String, Object> paramMap) {
        try {

            //验证token，并同时获取用户信息
            UserLoginInfoDto user = getUserAndVerifyToken(paramMap);
            final Object groupInfo = paramMap.get("groupInfo");

            if (groupInfo != null) {

                List<GroupPersonInfoDto> batchSaveList = new ArrayList<>();

                Map<String, Object> groupMap = (Map<String, Object>) groupInfo;
                for (Map.Entry<String, Object> entry : groupMap.entrySet()) {
                    _logger.info("person map ==>" + entry);
                    String groupName = entry.getKey();
                    _logger.info("groupName=" + groupName);
                    String groupId = "";
                    if (!CommonUtils.isBlank(groupName)) {
                        groupName = EmojiUtil.resolveToByteFromEmoji(groupName);
                        GroupBasicInfoDto groupBasicInfoDto = groupInfoService.getGroupBasicInfoByUserIdAndName(user.getUserId(), groupName);

                        if(groupBasicInfoDto == null){
                            continue;
                        }
                       /* if (groupBasicInfoDto != null && !CommonUtils.isBlank(groupBasicInfoDto.getGroupId())) {
                            groupId = groupBasicInfoDto.getGroupId();
                            //根据groupId，先删除，再新增
                            groupInfoService.deleteGroupPersonInfoByGroupId(groupId);
                        }*/
                        List<String> personNickNames = (List<String>) entry.getValue();
                        for (String nickName : personNickNames) {
                            GroupPersonInfoDto groupPersonInfoDto = new GroupPersonInfoDto();
                            groupPersonInfoDto.setGroupId(groupBasicInfoDto.getGroupId());
                            groupPersonInfoDto.setGroupName(groupName);
                            if(!CommonUtils.isBlank(nickName)){
                                nickName = EmojiUtil.resolveToByteFromEmoji(nickName);
                            }
                            groupPersonInfoDto.setPersonNickName(nickName);
                            groupPersonInfoDto.setUserId(user.getUserId());
                            batchSaveList.add(groupPersonInfoDto);
                        }
                    }
                }

                if(batchSaveList.size()!=0) {
                    //先删除，再增加
                    groupInfoService.deleteGroupPersonInfoByUserId(user.getUserId());
                    groupInfoService.saveGroupPersonInfo(batchSaveList);
                }
            }

            return succResponse(CommConstants.OPERATOR_SUCC);
        } catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message, ex);
            return failResponse(ex.getErrorCode(), message);
        } catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message, ex);
            return errorResponse(message);
        } catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }


    /**
     * 保存群组聊天信息
     */
    @PostMapping("/group/chat")
    public ResponseEntity<BaseResponseDto> groupchat(@RequestBody Map<String, Object> paramMap) {
        try {
            final String fromNickName = getMapParam("fromNickName", paramMap);//群组昵称
            final String toNickName = getMapParam("toNickName", paramMap);//回复人昵称
            final String content = getMapParam("content", paramMap);//聊天内容
            /*
             * 消息类型
             * 1文本消息 3图片消息 34语音消息 37好友确认消息 40POSSIBLEFRIEND_MSG 42共享名片 43视频消息
             * 47动画表情 48位置消息 49分享链接 50VOIPMSG 51微信初始化消息 52VOIPNOTIFY 53VOIPINVITE
             * 62小视频 9999SYSNOTICE 10000系统消息 10002撤回消息
             */
            final String messageType = getMapParam("messageType", paramMap);

            //验证token，并同时获取用户信息
            UserLoginInfoDto user = getUserAndVerifyToken(paramMap);

            _logger.info("fromNickName=[" + fromNickName + "] toNickName=[" + toNickName + "] content=[" + content + "] messageType=["+messageType+"] userId=[" + user.getUserId() + "]");

            String groupId = "";
            if (!CommonUtils.isBlank(fromNickName) && !fromNickName.equals("unknown")) {
                GroupBasicInfoDto groupBasicInfoDto = groupInfoService.getGroupBasicInfoByUserIdAndName(user.getUserId(), fromNickName);
                if (groupBasicInfoDto != null && !CommonUtils.isBlank(groupBasicInfoDto.getGroupId())) {
                    groupId = groupBasicInfoDto.getGroupId();

                }
            }
            GroupChatContentDto groupChatContentDto = new GroupChatContentDto();
            groupChatContentDto.setUserId(user.getUserId());
            groupChatContentDto.setGroupId(groupId);
            groupChatContentDto.setGroupNickName(fromNickName);
            groupChatContentDto.setPersonNickName(toNickName);
            groupChatContentDto.setContent(content);
            groupChatContentDto.setMessageType(messageType);

            groupChatContentDto = groupInfoService.saveGroupChatContent(groupChatContentDto);
            if (groupChatContentDto == null) {
                return failResponse(CommConstants.OPERATOR_FAIL);
            }
            return succResponse(CommConstants.OPERATOR_SUCC);
        } catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message, ex);
            return failResponse(ex.getErrorCode(), message);
        } catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message, ex);
            return errorResponse(message);
        } catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }

    /**
     * 查询机器人已授权的群组列表
     */
    @GetMapping("/robo/group/list")
    public ResponseEntity<BaseResponseDto> getAuthorizedGroupInfoList() {
        try {

            final String uin = getRequestParamAndCheckNull("uin");//微信uin
            final String authorizeStatus = getRequestParam("authorizeStatus");//授权状态 0：未授权 1：已授权；可为空，为空时，查询所有授权状态
            final String keyWord = getRequestParam("keyWord");//查询条件，例如群组昵称，可为空
            final String isOwner = getRequestParam("isOwner");//是否为群主

            //验证token，并同时获取用户信息
            UserLoginInfoDto user = getUserAndVerifyToken();

            Map<String, Object> resultMap = new HashMap<String, Object>();

            RoboBasicInfoDto roboBasicInfoDto = roboInfoService.getRoboBasicInfoByUin(user.getUserId(),uin,"");
            if (roboBasicInfoDto != null && RoboCurrentStatusEnum.login_cancel.getValue().equals(roboBasicInfoDto.getCurrentStatus())) {
                //说明微信机器人已经取消追踪（已删除）
                _logger.info("-----------微信机器人已经取消追踪 userId=["+user.getUserId()+"] uin=["+uin+"]--------");
                return succResponse(CommConstants.QUERY_SUCC, resultMap);
            }

            //查询所有授权的群组
            List<GroupBasicInfoDto> groupBasicInfoList = groupInfoService.getAuthorizedGroupBasicInfoList(user.getUserId(),uin, keyWord, authorizeStatus,isOwner);
            List<GroupBasicInfoResponseDto> responseDtoList = new ArrayList<GroupBasicInfoResponseDto>();
            if (groupBasicInfoList != null && groupBasicInfoList.size() > 0) {
                for (GroupBasicInfoDto dto : groupBasicInfoList) {
                    GroupBasicInfoResponseDto responseDto = new GroupBasicInfoResponseDto();
                    responseDto.setUin(dto.getUin());
                    responseDto.setGroupId(dto.getGroupId());
                    responseDto.setNickName(dto.getNickName());
                    responseDto.setAuthorizeStatus(dto.getAuthorizeStatus());
                    responseDtoList.add(responseDto);
                }
            }

            resultMap.put("groupBasicInfoList", responseDtoList);

            return succResponse(CommConstants.QUERY_SUCC, resultMap);

        } catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message, ex);
            return failResponse(ex.getErrorCode(), message);
        } catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message, ex);
            return errorResponse(message);
        } catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }

    /**
     * 查询群组基本信息及其所有群组成员列表
     */
    @GetMapping("/group/detail")
    public ResponseEntity<BaseResponseDto> getGroupInfoDetail() {
        try {

            final String groupId = getRequestParamAndCheckNull("groupId");//群组Id

            //验证token，并同时获取用户信息
            UserLoginInfoDto user = getUserAndVerifyToken();

            _logger.info("groupId=[" + groupId + "] userId=[" + user.getUserId() + "]");

            //根据群组Id，查询群组基本信息
            GroupBasicInfoDto groupBasicInfoDto = groupInfoService.getGroupBasicInfoByGroupId(groupId);
            GroupBasicInfoResponseDto responseDto = new GroupBasicInfoResponseDto();
            if (groupBasicInfoDto != null) {
                responseDto.setGroupId(groupBasicInfoDto.getGroupId());
                responseDto.setUin(groupBasicInfoDto.getUin());
                responseDto.setNickName(groupBasicInfoDto.getNickName());
                responseDto.setAuthorizeStatus(groupBasicInfoDto.getAuthorizeStatus());
            }

            //根据群组Id，查询群组成员列表（不分页）
            List<GroupPersonInfoDto> groupPersonInfoList = groupInfoService.getGroupPersonInfoList(groupId);
            List<GroupPersonResponseDto> personResponseDtoList = new ArrayList<GroupPersonResponseDto>();
            if (groupPersonInfoList != null && groupPersonInfoList.size() > 0) {
                for (GroupPersonInfoDto dto : groupPersonInfoList) {
                    GroupPersonResponseDto personResponseDto = new GroupPersonResponseDto();
                    personResponseDto.setGroupPersonId(dto.getGroupPersonId());
                    personResponseDto.setPersonNickName(dto.getPersonNickName());
                    personResponseDtoList.add(personResponseDto);
                }
            }

            Map<String, Object> resultMap = new HashMap<String, Object>();
            resultMap.put("groupBasicInfo", responseDto);
            resultMap.put("groupPersonInfoList", personResponseDtoList);

            return succResponse(CommConstants.QUERY_SUCC, resultMap);
        } catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message, ex);
            return failResponse(ex.getErrorCode(), message);
        } catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message, ex);
            return errorResponse(message);
        } catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }

    /**
     * 根据机器人Id，查询群组基本信息列表(分页显示)
     */
    @GetMapping("/group/list")
    public ResponseEntity<BaseResponseDto> getGroupInfoList() {
        try {
            final int pageNumber = getRequestParamIntAndCheckNull("pageNumber");//当前页
            final int pageSize = getRequestParamIntAndCheckNull("pageSize");//每页条数
            final String uin = getRequestParamAndCheckNull("uin");//微信uin
            final String keyWord = getRequestParam("keyWord");//查询条件，例如群组昵称，可为空

            //验证token，并同时获取用户信息
            UserLoginInfoDto user = getUserAndVerifyToken();

            _logger.info("pageNumber=[" + pageNumber + "] pageSize=[" + pageSize + "] uin=[" + uin + "] userId=[" + user.getUserId() + "]");

            //根据群组Id，查询群组基本信息
            Page<GroupBasicInfoDto> groupBasicInfoDtoPage = groupInfoService.getGroupBasicInfoPage(user.getUserId(),uin, keyWord, setPage(pageNumber, pageSize, new Sort(Sort.Direction.DESC, "createTime")));
            List<GroupBasicInfoResponseDto> responseDtoList = new ArrayList<GroupBasicInfoResponseDto>();
            if (groupBasicInfoDtoPage != null && groupBasicInfoDtoPage.getContent() != null && groupBasicInfoDtoPage.getContent().size() > 0) {
                for (GroupBasicInfoDto dto : groupBasicInfoDtoPage.getContent()) {
                    GroupBasicInfoResponseDto responseDto = new GroupBasicInfoResponseDto();
                    responseDto.setUin(dto.getUin());
                    responseDto.setGroupId(dto.getGroupId());
                    responseDto.setNickName(dto.getNickName());
                    responseDto.setAuthorizeStatus(dto.getAuthorizeStatus());
                    responseDtoList.add(responseDto);
                }
            }

            Map<String, Object> resultMap = new HashMap<String, Object>();
            resultMap.put("groupBasicInfoList", responseDtoList);
            resultMap.put("totalCount", groupBasicInfoDtoPage.getTotalElements());

            return succResponse(CommConstants.QUERY_SUCC, resultMap);
        } catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message, ex);
            return failResponse(ex.getErrorCode(), message);
        } catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message, ex);
            return errorResponse(message);
        } catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }


    /**
     * 根据群组ID，更新群组的授权状态
     */
    @PostMapping("/group/info/audit")
    public ResponseEntity<BaseResponseDto> groupInfoAudit(@RequestBody Map<String, Object> paramMap) {
        try {

            final String groupIds = getMapParam("groupIds", paramMap);//群组ID，多个群组Id时，以,分隔
            final String authorizeStatus = getMapParam("authorizeStatus", paramMap);//授权 0：未授权 1：已授权

            //验证token，并同时获取用户信息
            UserLoginInfoDto user = getUserAndVerifyToken(paramMap);

            _logger.info("groupId=[" + groupIds + "] authorizeStatus=[" + authorizeStatus + "] userId=[" + user.getUserId() + "]");

            int count = 0;
            if (!CommonUtils.isBlank(groupIds)) {
                String[] groupIdArr = groupIds.split(",");
                if (groupIdArr != null && groupIdArr.length > 0) {
                    for (String groupId : groupIdArr) {
                        if (!CommonUtils.isBlank(groupId)) {
                            int num = groupInfoService.updateGroupBasicInfoStatus(groupId, authorizeStatus);
                            count = num + count;
                        }
                    }
                }
            }

            String message = "";
            if (authorizeStatus.equals(GroupAuthorizeStatusEnum.NO.getValue())) {
                message = "取消授权群组" + count + "个";
            } else if (authorizeStatus.equals(GroupAuthorizeStatusEnum.YES.getValue())) {
                message = "成功授权群组" + count + "个";
            }

            Map<String, Object> resultMap = new HashMap<String, Object>();
            resultMap.put("message", message);


            final String roboId = redis.getString(CacheWebScoketEnum.roboId.getValue()+user.getUserId());
            if(!isBlank(roboId)) {
                RabbitMQUtils.refreshGroupInfo(roboId,"auth");
            }

            return succResponse(CommConstants.OPERATOR_SUCC, resultMap);

        } catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message, ex);
            return failResponse(ex.getErrorCode(), message);
        } catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message, ex);
            return errorResponse(message);
        } catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }

    /**
     * 查询群组所有成员列表
     */
    @GetMapping("/group/person/list")
    public ResponseEntity<BaseResponseDto> getGroupPersonList() {
        try {

            final String groupId = getRequestParamAndCheckNull("groupId");//群组Id

            //验证token，并同时获取用户信息
            UserLoginInfoDto user = getUserAndVerifyToken();

            _logger.info("groupId=[" + groupId + "] userId=[" + user.getUserId() + "]");


            GroupBasicInfoDto groupBasicInfoDto = groupInfoService.getGroupBasicInfoByGroupId(groupId);
            if(null == groupBasicInfoDto){
                return failResponse(CommConstants.NOT_FUND);
            }
            _logger.info("group info ===>" + groupBasicInfoDto.toLogger());
            if(!CommResultEnum.YES.getValue().equals(groupBasicInfoDto.getIsOwner())){
                return failResponse(RoboMessage.GROUP_NOT_AUTHORIZATION);
            }

            String ownerName = groupBasicInfoDto.getOwnerName(); //群主名称

            //根据群组Id，查询群组成员列表（不分页）
            List<GroupPersonInfoDto> groupPersonInfoList = groupInfoService.getGroupPersonInfoList(groupId);
            List<GroupPersonResponseDto> groupPersonList = new ArrayList<GroupPersonResponseDto>();
            if (groupPersonInfoList != null && groupPersonInfoList.size() > 0) {
                for (GroupPersonInfoDto dto : groupPersonInfoList) {
                    //去除 群主功能
                    if(ownerName.equals(dto.getPersonNickName())){
                        continue;
                    }
                    GroupPersonResponseDto responseDto = new GroupPersonResponseDto();
                    responseDto.setGroupPersonId(dto.getGroupPersonId());
                    responseDto.setPersonNickName(dto.getPersonNickName());
                    groupPersonList.add(responseDto);
                }
            }

            Map<String, Object> resultMap = new HashMap<String, Object>();
            resultMap.put("groupPersonList", groupPersonList);

            return succResponse(CommConstants.QUERY_SUCC, resultMap);
        } catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message, ex);
            return failResponse(ex.getErrorCode(), message);
        } catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message, ex);
            return errorResponse(message);
        } catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }

    /**
     * 保存群组成员变更信息
     */
    @PostMapping("/group/person/change")
    public ResponseEntity<BaseResponseDto> groupPersonChange(@RequestBody Map<String, Object> paramMap) {
        try {
            final String groupId = getMapParamAndCheckNull("groupId",paramMap);//群组Id
            final String personIds = getMapParamAndCheckNull("personIds", paramMap);//群组成员ID，多个群组成员Id时，以,分隔
            final String type = getMapParamAndCheckNull("type", paramMap);//变更类型 10：邀请加入，11:扫描二维码加入 20：主动退群，21群主踢人
            if (!GroupPersonChangeTypeEnum.getAllMap().containsKey(type)) {
                throw new BusinessException(RoboMessage.GROUP_PERSON_CHANE_TYPE_NOT_EXIST);
            }

            //验证token，并同时获取用户信息
            UserLoginInfoDto user = getUserAndVerifyToken(paramMap);

            _logger.info("groupId=["+groupId+"] personIds=[" + personIds + "] type=[" + type + "] userId=[" + user.getUserId() + "]");

            List<String> personNameList = new ArrayList<String>();
            if (!CommonUtils.isBlank(personIds)) {
                String[] personIdArr = personIds.split(",");
                if (personIdArr != null && personIdArr.length > 0) {
                    for (String personId : personIdArr) {
                        if (!CommonUtils.isBlank(personId)) {
                            GroupPersonInfoDto groupPersonInfoDto = groupInfoService.getGroupPersonInfoByGroupPersonId(personId);
                            if (groupPersonInfoDto != null) {
                                personNameList.add(groupPersonInfoDto.getPersonNickName());
                            }
                        }
                    }
                }
            }

            GroupBasicInfoDto groupBasicInfoDto = groupInfoService.getGroupBasicInfoByGroupId(groupId);
            if(groupBasicInfoDto==null){
                return failResponse(CommConstants.NOT_FUND);
            }

            //先通过MQ，与python交互，进行成员变更操作
            if(type.equals(GroupPersonChangeTypeEnum.DEL_TIREN.getValue())){
                String robotId = redis.getString(CacheWebScoketEnum.roboId.getValue()+user.getUserId());
                if(!isBlank(robotId)) {
                    RabbitMQUtils.tiRenOperate(robotId, groupBasicInfoDto.getNickName(), personNameList);
                }
            }

            Map<String, Object> resultMap = new HashMap<String, Object>();

            return succResponse(CommConstants.OPERATOR_SUCC, resultMap);

        } catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message, ex);
            return failResponse(ex.getErrorCode(), message);
        } catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message, ex);
            return errorResponse(message);
        } catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }


    /**
     * 保存群组成员变更
     */
    @PostMapping("/group/person/change/result")
    public ResponseEntity<BaseResponseDto> groupPersonChangeResult(@RequestBody Map<String, Object> paramMap) {
        try {

            final String groupName = getMapParamAndCheckNull("groupName", paramMap);//群组名称
            final Object personNameObject = paramMap.get("personNameList");//成员昵称列表
            final String type = getMapParamAndCheckNull("type", paramMap);//变更类型 10：邀请加入，11:扫描二维码加入 20：主动退群，21群主踢人
            final String memo = getMapParam("memo", paramMap);//备注

            List<String> personNameList = new ArrayList<>();
            if (personNameObject != null) {
                personNameList = (List<String>) personNameObject;
            }

            UserLoginInfoDto user = getUserAndVerifyToken(paramMap);

            if(personNameList!=null&&personNameList.size()>0){
                for(String personName:personNameList){
                    //查询群组成员信息
                    List<GroupPersonInfoDto> groupPersonInfoDtoList = groupInfoService.getGroupPersonInfoListByName(user.getUserId(),groupName,personName);
                    if(groupPersonInfoDtoList!=null&&groupPersonInfoDtoList.size()>0) {
                        for (GroupPersonInfoDto groupPersonInfoDto: groupPersonInfoDtoList) {
                            if (groupPersonInfoDto != null) {
                                GroupPersonChangeDto groupPersonChangeDto = new GroupPersonChangeDto();
                                groupPersonChangeDto.setUserId(groupPersonInfoDto.getUserId());
                                groupPersonChangeDto.setType(type);
                                groupPersonChangeDto.setMemo(memo);
                                groupPersonChangeDto.setPersonId(groupPersonInfoDto.getGroupPersonId());
                                groupPersonChangeDto.setGroupId(groupPersonInfoDto.getGroupId());
                                groupPersonChangeDto.setGroupName(groupPersonInfoDto.getGroupName());
                                groupPersonChangeDto.setPersonName(groupPersonInfoDto.getPersonNickName());
                                GroupPersonChangeDto changeDto = groupInfoService.saveGroupPersonChange(groupPersonChangeDto);
                                if (changeDto != null && (type.equals(GroupPersonChangeTypeEnum.DEL_TIREN.getValue()) ||
                                        type.equals(GroupPersonChangeTypeEnum.DEL_TUIQUN.getValue()))) {
                                        //踢人后，删除相关记录
                                        groupInfoService.deleteGroupPersonInfoByPersonId(groupPersonInfoDto.getGroupPersonId());
                                }
                            }
                        }
                    }else {
                        //当定位不到具体person时，只定义群名
                        GroupBasicInfoDto groupBasicInfoDto = groupInfoService.getGroupBasicInfoByUserIdAndName(user.getUserId(), groupName);
                        if(groupBasicInfoDto != null){
                                GroupPersonChangeDto groupPersonChangeDto = new GroupPersonChangeDto();
                                String personId = "";
                                //如果是加入人，则先添加群组关系
                               if(type.equals(GroupPersonChangeTypeEnum.ADD_YAOQING.getValue()) ||
                                        type.equals(GroupPersonChangeTypeEnum.ADD_SAOMIAO.getValue())){

                                    GroupPersonInfoDto personInfoDto = new GroupPersonInfoDto();
                                    personInfoDto.setGroupId(groupBasicInfoDto.getGroupId());
                                    personInfoDto.setGroupName(groupBasicInfoDto.getNickName());
                                    personInfoDto.setPersonNickName(personName);
                                    personInfoDto.setUserId(user.getUserId());
                                    GroupPersonInfoDto dto = groupInfoService.saveGroupPersonInfo(personInfoDto);
                                    personId = dto.getGroupPersonId();
                                }

                                groupPersonChangeDto.setUserId(user.getUserId());
                                groupPersonChangeDto.setType(type);
                                groupPersonChangeDto.setMemo(memo);
                                groupPersonChangeDto.setPersonId(personId);
                                groupPersonChangeDto.setGroupId(groupBasicInfoDto.getGroupId());
                                groupPersonChangeDto.setGroupName(groupBasicInfoDto.getNickName());
                                groupPersonChangeDto.setPersonName(personName);
                                groupInfoService.saveGroupPersonChange(groupPersonChangeDto);

                        }
                    }
                }
            }

            return succResponse(CommConstants.OPERATOR_SUCC);

        } catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message, ex);
            return failResponse(ex.getErrorCode(), message);
        } catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message, ex);
            return errorResponse(message);
        } catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }


    /**
     * 保存群组成员变更
     */
    @GetMapping("/group/refresh")
    public ResponseEntity<BaseResponseDto> refresh() {
        try {

            final String robotId = getRequestParamAndCheckNull("robotId");
            //UserLoginInfoDto user = getUserAndVerifyToken();
            RabbitMQUtils.refreshGroupInfo(robotId);
            return succResponse(CommConstants.OPERATOR_SUCC);
        } catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message, ex);
            return failResponse(ex.getErrorCode(), message);
        } catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message, ex);
            return errorResponse(message);
        } catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }



}