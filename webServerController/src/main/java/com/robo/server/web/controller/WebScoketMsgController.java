package com.robo.server.web.controller;

import com.common.base.CommConstants;
import com.common.base.exception.BusinessException;
import com.common.base.model.WebSocketMsgModel;
import com.common.base.response.BaseResponseDto;
import com.robo.server.web.base.WebServerController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by jianghaoming on 2017/7/19  22:01
 */
@RestController
@RequestMapping("/websocket/v1")
public class WebScoketMsgController extends WebServerController {

    private static final Logger _logger = LoggerFactory.getLogger(WebScoketMsgController.class);

    @PostMapping("/message")
    public ResponseEntity<BaseResponseDto> message(@RequestBody WebSocketMsgModel requestModel){

        try {
            _logger.info("request.log == > "+requestModel.toLogger());

            String messageId = requestModel.getMessageId();
            String method = requestModel.getMethod();
            String timeStamp = requestModel.getTimeStamp();

            checkParamNull(messageId,"messageId is null");
            checkParamNull(method,"method is null");
            checkParamNull(timeStamp,"timeStamp is null");

            return succResponse(CommConstants.OPERATOR_SUCC,requestModel);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }
}
