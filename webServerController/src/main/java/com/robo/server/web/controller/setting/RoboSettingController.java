package com.robo.server.web.controller.setting;

import com.common.base.CommConstants;
import com.common.base.exception.BusinessException;
import com.common.base.response.BaseResponseDto;
import com.robo.server.web.base.WebServerController;
import com.robo.server.web.em.robo.RoboCurrentStatusEnum;
import com.robo.server.web.em.setting.RoboSettingContentStatusEnum;
import com.robo.server.web.em.setting.RoboSwitchStatusEnum;
import com.robo.server.web.entity.robo.RoboBasicInfoDto;
import com.robo.server.web.entity.setting.RoboChatDictDto;
import com.robo.server.web.entity.setting.RoboSettingContentDto;
import com.robo.server.web.entity.setting.RoboSwitchStatsticsDto;
import com.robo.server.web.entity.setting.response.RoboChatDictResponseDto;
import com.robo.server.web.entity.setting.response.RoboSettingContentResponseDto;
import com.robo.server.web.entity.setting.response.RoboSwitchStatsticsResponseDto;
import com.robo.server.web.entity.user.UserLoginInfoDto;
import com.robo.server.web.message.RoboMessage;
import com.robo.server.web.mq.RabbitMQUtils;
import com.robo.server.web.robo.RoboInfoService;
import com.robo.server.web.setting.RoboSettingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

@RestController
@RequestMapping("/v1")
public class RoboSettingController extends WebServerController {

    private static final Logger _logger = LoggerFactory.getLogger(RoboSettingController.class);

    @Autowired
    private RoboSettingService roboSettingService;

    @Autowired
    private RoboInfoService roboInfoService;

    /**
     * 更新/保存机器人相关操作开关状态及切换次数统计
     * @param paramMap
     * @return
     */
    @PostMapping("/robo/switch/statstics")
    public ResponseEntity<BaseResponseDto> roboSwitchStatsics(@RequestBody Map<String,Object> paramMap) {
        try {
            final String uin = getMapParamAndCheckNull("uin",paramMap);//'微信uin',
            final String switchType = getMapParamAndCheckNull("switchType",paramMap);//开关类型 0：切换 1：自动回复 2：机器人当前状态 4：踢人 5：欢迎 6：规定 7：数据分析 8：聊天匹配 9：聊天模糊匹配
            final String switchStatus = getMapParamAndCheckNull("switchStatus",paramMap);//开关状态 0:open 1:close

            UserLoginInfoDto user = getUserAndVerifyToken(paramMap);

            _logger.info("uin=["+uin+"] switchType=["+switchType+"] switchStatus=["+switchStatus+"] userId=["+user.getUserId()+"]");

            RoboSwitchStatsticsDto roboSwitchStatsticsDto = new RoboSwitchStatsticsDto();
            roboSwitchStatsticsDto.setUin(uin);
            roboSwitchStatsticsDto.setUserId(user.getUserId());
            roboSwitchStatsticsDto.setSwitchType(switchType);
            roboSwitchStatsticsDto.setSwitchStatus(switchStatus);

            roboSwitchStatsticsDto = roboSettingService.updateRoboFuncStatstics(roboSwitchStatsticsDto);
            if(roboSwitchStatsticsDto==null){
                return succResponse(CommConstants.OPERATOR_FAIL);
            }

            //先判断机器人是否在线,在线时再调用MQ
            RoboBasicInfoDto dto = roboInfoService.getRoboBasicInfoByUin(user.getUserId(),uin, "");
            if(RoboCurrentStatusEnum.login_ing.getValue().equals(dto.getCurrentStatus())){
                //调用mq
                this.updateConfig(roboSwitchStatsticsDto.getUserId(),roboSwitchStatsticsDto.getUin());
            }

            return succResponse(CommConstants.OPERATOR_SUCC);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }

    /**
     * 查询机器人相关操作开关状态及切换次数
     * @return
     */
    @GetMapping("/robo/switch/statstics")
    public ResponseEntity<BaseResponseDto> getRoboSwitchStatsics() {
        try {
            final String uin = getRequestParamAndCheckNull("uin");//'微信uin',
            final String switchType = getRequestParamAndCheckNull("switchType");//开关类型 0：切换 1：自动回复 2：机器人当前状态 4：踢人 5：欢迎 6：规定 7：数据分析 8：聊天匹配 9：聊天模糊匹配

            UserLoginInfoDto user = getUserAndVerifyToken();

            _logger.info("uin=["+uin+"] switchType=["+switchType+"] userId=["+user.getUserId()+"]");

            RoboSwitchStatsticsDto roboSwitchStatsticsDto = roboSettingService.getRoboFuncStatstics(user.getUserId(),uin,switchType);
            if(roboSwitchStatsticsDto==null){
                roboSwitchStatsticsDto = new RoboSwitchStatsticsDto();
                roboSwitchStatsticsDto.setUserId(user.getUserId());
                roboSwitchStatsticsDto.setUin(uin);
                roboSwitchStatsticsDto.setSwitchType(switchType);
                roboSwitchStatsticsDto.setSwitchStatus(RoboSwitchStatusEnum.close.getValue());
                roboSwitchStatsticsDto.setSwitchNum("0");
            }
            RoboSwitchStatsticsResponseDto responseDto = new RoboSwitchStatsticsResponseDto();
            responseDto.setSwitchType(roboSwitchStatsticsDto.getSwitchType());
            responseDto.setSwitchStatus(roboSwitchStatsticsDto.getSwitchStatus());

            Map<String,Object> resultMap = new HashMap<String, Object>();
            resultMap.put("switchInfo",responseDto);

            return succResponse(CommConstants.QUERY_SUCC,resultMap);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }


    /**
     * 新增保存机器人聊天回复自动匹配信息
     * @param paramMap
     * @return
     */
    @PostMapping("/robo/chat/dict")
    public ResponseEntity<BaseResponseDto> roboChatDict(@RequestBody Map<String,Object> paramMap) {
        try {
            final String uin = getMapParamAndCheckNull("uin",paramMap);//微信uin
            final String chatKey = getMapParamAndCheckNull("chatKey",paramMap);//聊天内容
            final String chatValue = getMapParam("chatValue",paramMap);;//聊天匹配回复内容

            UserLoginInfoDto user = getUserAndVerifyToken(paramMap);

            _logger.info("uin=["+uin+"] chatKey=["+chatKey+"] chatValue=["+chatValue+"] userId=["+user.getUserId()+"]");

            //目前限制：最多只能添加20条
            List<RoboChatDictDto> chatDictList = roboSettingService.getRoboChatDictList(user.getUserId(),uin);
            if(chatDictList!=null&&chatDictList.size()>=20){
                return succResponse(RoboMessage.chat_dic_limit_count);
            }

            RoboChatDictDto roboChatDictDto = new RoboChatDictDto();
            roboChatDictDto.setUin(uin);
            roboChatDictDto.setUserId(user.getUserId());
            roboChatDictDto.setChatKey(chatKey);
            roboChatDictDto.setChatValue(chatValue);

            roboChatDictDto = roboSettingService.saveRoboChatDict(roboChatDictDto);
            if(roboChatDictDto==null){
                return succResponse(CommConstants.OPERATOR_FAIL);
            }

            //调用websocket
            this.updateConfig(user.getUserId(),uin);

            return succResponse(CommConstants.OPERATOR_SUCC);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }

    /**
     * 查询机器人聊天回复自动匹配信息列表
     * @return
     */
    @GetMapping("/robo/chat/dict/list")
    public ResponseEntity<BaseResponseDto> roboChatDict() {
        try{

            final String uin = getRequestParamAndCheckNull("uin");

            UserLoginInfoDto user = getUserAndVerifyToken();

            _logger.info("uin=["+uin+"] userId=["+user.getUserId()+"]");

             List<RoboChatDictDto> roboChatDictDtoList = roboSettingService.getRoboChatDictList(user.getUserId(),uin);
             List<RoboChatDictResponseDto> chatDictResponseDtoList = new ArrayList<>();
             if(roboChatDictDtoList!=null&&roboChatDictDtoList.size()>0){
                 for(RoboChatDictDto dto:roboChatDictDtoList){
                     RoboChatDictResponseDto responseDto = new RoboChatDictResponseDto();
                     responseDto.setId(dto.getId());
                     responseDto.setChatKey(dto.getChatKey());
                     responseDto.setChatValue(dto.getChatValue());
                     chatDictResponseDtoList.add(responseDto);
                 }
             }

             Map<String,Object> resultMap = new HashMap<String, Object>();
             resultMap.put("list",chatDictResponseDtoList);

            return succResponse(CommConstants.QUERY_SUCC,resultMap);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }

    @PostMapping("/robo/chat/dict/update")
    public ResponseEntity<BaseResponseDto> updateRoboChatDict(@RequestBody Map<String,Object> paramMap) {
        try{

            final String id = getMapParamAndCheckNull("id",paramMap);
            final String chatKey = getMapParamAndCheckNull("chatKey",paramMap);
            final String chatValue = getMapParamAndCheckNull("chatValue",paramMap);

            UserLoginInfoDto user = getUserAndVerifyToken(paramMap);

            _logger.info("id=["+id+"] chatKey=["+chatKey+"] chatValue=["+chatValue+"] userId=["+user.getUserId()+"]");


            RoboChatDictDto dto = roboSettingService.getRoboChatDictById(id);
            if(dto == null){
                return failResponse(CommConstants.NOT_FUND);
            }

            int count = roboSettingService.updateRoboChatDictById(id,chatKey,chatValue);
            if(count<=0){
                return succResponse(CommConstants.OPERATOR_FAIL);
            }

            //调用websocket
            this.updateConfig(dto.getUserId(),dto.getUin());
            return succResponse(CommConstants.OPERATOR_SUCC);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }

    @PostMapping("/robo/chat/dict/delete")
    public ResponseEntity<BaseResponseDto> deleteRoboChatDict(@RequestBody Map<String,Object> paramMap) {
        try{

            final String id = getMapParamAndCheckNull("id",paramMap);

            UserLoginInfoDto user = getUserAndVerifyToken(paramMap);

            _logger.info("id=["+id+"] userId=["+user.getUserId()+"]");

            RoboChatDictDto dto = roboSettingService.getRoboChatDictById(id);
            if(dto == null){
                return failResponse(CommConstants.NOT_FUND);
            }

            roboSettingService.deleteRoboChatDictById(id);

            //调用websocket
            this.updateConfig(dto.getUserId(),dto.getUin());
            return succResponse(CommConstants.OPERATOR_SUCC);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }

    /**
     * 新增保存机器人设置信息（例如欢迎语，规定等）
     * @param paramMap
     * @return
     */
    @PostMapping("/robo/setting/content")
    public ResponseEntity<BaseResponseDto> roboSettingContent(@RequestBody Map<String,Object> paramMap) {
        try {
            final String uin = getMapParamAndCheckNull("uin",paramMap);//微信uin
            final String type = getMapParamAndCheckNull("type",paramMap);//类型 0：踢人 1：欢迎 2：规定 98:切换 99：图灵机器人Key
            final String content = getMapParam("content",paramMap);;//内容

            UserLoginInfoDto user = getUserAndVerifyToken(paramMap);

            _logger.info("uin=["+uin+"] type=["+type+"] content=["+content+"] userId=["+user.getUserId()+"]");

            RoboSettingContentDto roboSettingContentDto = new RoboSettingContentDto();
            roboSettingContentDto.setUin(uin);
            roboSettingContentDto.setUserId(user.getUserId());
            roboSettingContentDto.setType(type);
            roboSettingContentDto.setContent(content);

            roboSettingContentDto = roboSettingService.saveRoboSettingContent(roboSettingContentDto);
            if(roboSettingContentDto==null){
                return succResponse(CommConstants.OPERATOR_FAIL);
            }

            //调用websocket
            this.updateConfig(user.getUserId(),uin);
            return succResponse(CommConstants.OPERATOR_SUCC);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }

    /**
     * 查询保存机器人设置提示语信息列表（例如欢迎语，规定等）
     * @return
     */
    @GetMapping("/robo/setting/content/list")
    public ResponseEntity<BaseResponseDto> roboSettingContent() {
        try {
            final String uin = getRequestParamAndCheckNull("uin");//微信uin
            final String type = getRequestParamAndCheckNull("type");//类型 0：踢人 1：欢迎 2：规定 98:切换 99：图灵机器人Key

            UserLoginInfoDto user = getUserAndVerifyToken();

            _logger.info("uin=["+uin+"] type=["+type+"] userId=["+user.getUserId()+"]");

            List<RoboSettingContentDto> roboSettingContentList = roboSettingService.getRoboSettingContentList(user.getUserId(),uin,type);
            List<RoboSettingContentResponseDto> responseDtoList = new ArrayList<>();
            if(roboSettingContentList!=null&&roboSettingContentList.size()>0){
                for(RoboSettingContentDto dto:roboSettingContentList){
                    RoboSettingContentResponseDto contentResponseDto = new RoboSettingContentResponseDto();
                    contentResponseDto.setId(dto.getId());
                    contentResponseDto.setType(dto.getType());
                    contentResponseDto.setContent(dto.getContent());
                    contentResponseDto.setStatus(dto.getStatus());
                    responseDtoList.add(contentResponseDto);
                }
            }

            Map<String,Object> resultMap = new HashMap<String, Object>();
            resultMap.put("list",responseDtoList);

            return succResponse(CommConstants.OPERATOR_SUCC,resultMap);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }


    /**
     * 根据ID，更新机器人提示语设置启用状态
     * @param paramMap
     * @return
     */
    @PostMapping("/robo/setting/content/status")
    public ResponseEntity<BaseResponseDto> updateRoboSettingContentStatus(@RequestBody Map<String,Object> paramMap) {
        try{

            final String id = getMapParamAndCheckNull("id",paramMap);
            final String status = getMapParamAndCheckNull("status",paramMap);

            UserLoginInfoDto user = getUserAndVerifyToken(paramMap);

            _logger.info("id=["+id+"] status=["+status+"] userId=["+user.getUserId()+"]");


            RoboSettingContentDto dto = roboSettingService.getRoboSettingContentById(id);
            if(dto == null){
                return failResponse(CommConstants.NOT_FUND);
            }

            int count = roboSettingService.updateRoboSettingContentStatusById(id,status);
            if(count<=0){
                return succResponse(CommConstants.OPERATOR_FAIL);
            }

            //调用websocket
            this.updateConfig(dto.getUserId(),dto.getUin());

            return succResponse(CommConstants.OPERATOR_SUCC);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }


    /**
     * 根据ID，更新机器人提示语设置信息
     * @param paramMap
     * @return
     */
    @PostMapping("/robo/setting/content/update")
    public ResponseEntity<BaseResponseDto> updateRoboSettingContent(@RequestBody Map<String,Object> paramMap) {
        try{

            final String id = getMapParamAndCheckNull("id",paramMap);
            final String content = getMapParamAndCheckNull("content",paramMap);

            UserLoginInfoDto user = getUserAndVerifyToken(paramMap);

            _logger.info("id=["+id+"] content=["+content+"] userId=["+user.getUserId()+"]");


            RoboSettingContentDto dto = roboSettingService.getRoboSettingContentById(id);
            if(dto == null){
                return failResponse(CommConstants.NOT_FUND);
            }

            int count = roboSettingService.updateRoboSettingContentById(id,content);
            if(count<=0){
                return succResponse(CommConstants.OPERATOR_FAIL);
            }

            //调用websocket
            this.updateConfig(dto.getUserId(),dto.getUin());

            return succResponse(CommConstants.OPERATOR_SUCC);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }

    /**
     * 根据ID，删除机器人提示语设置信息
     * @param paramMap
     * @return
     */
    @PostMapping("/robo/setting/content/delete")
    public ResponseEntity<BaseResponseDto> deleteRoboSettingContent(@RequestBody Map<String,Object> paramMap) {
        try{

            final String id = getMapParamAndCheckNull("id",paramMap);

            UserLoginInfoDto user = getUserAndVerifyToken(paramMap);

            _logger.info("id=["+id+"] userId=["+user.getUserId()+"]");


            RoboSettingContentDto dto = roboSettingService.getRoboSettingContentById(id);
            if(dto == null){
                return failResponse(CommConstants.NOT_FUND);
            }

            roboSettingService.deleteRoboSettingContentById(id);

            //调用websocket
            this.updateConfig(dto.getUserId(),dto.getUin());

            return succResponse(CommConstants.OPERATOR_SUCC);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }

    /**
     * 查询所有机器人设置信息列表
     * @return
     */
    @GetMapping("/robo/setting/list")
    public ResponseEntity<BaseResponseDto> roboSettingAllList() {
        try {
            final String uin = getRequestParamAndCheckNull("uin");//微信uin

            UserLoginInfoDto user = getUserAndVerifyToken();

            _logger.info("uin=["+uin+"] userId=["+user.getUserId()+"]");

            //查询所有机器人开关列表信息
            List<RoboSwitchStatsticsDto> roboSwitchStatsticsDtoList = roboSettingService.getAllRoboSwitchStaticsList(user.getUserId(),uin);
            List<RoboSwitchStatsticsResponseDto> roboSwitchStatsticsResponseDtoList = new ArrayList<RoboSwitchStatsticsResponseDto>();
            if(roboSwitchStatsticsDtoList!=null&&roboSwitchStatsticsDtoList.size()>0){
                for(RoboSwitchStatsticsDto dto:roboSwitchStatsticsDtoList){
                    if(dto!=null){
                        RoboSwitchStatsticsResponseDto roboSwitchStatsticsResponseDto = new RoboSwitchStatsticsResponseDto();
                        roboSwitchStatsticsResponseDto.setSwitchStatus(dto.getSwitchStatus());
                        roboSwitchStatsticsResponseDto.setSwitchType(dto.getSwitchType());
                        roboSwitchStatsticsResponseDtoList.add(roboSwitchStatsticsResponseDto);
                    }
                }
            }

            //查询所有提示语信息，包括图灵机器人key
            List<RoboSettingContentDto> roboSettingContentDtoList = roboSettingService.getAllRoboSettingContentList(user.getUserId(),uin, RoboSettingContentStatusEnum.OPEN.getValue());
            List<RoboSettingContentResponseDto> roboSettingContentResponseDtoList = new ArrayList<RoboSettingContentResponseDto>();
            if(roboSettingContentDtoList!=null&&roboSettingContentDtoList.size()>0){
                for(RoboSettingContentDto dto:roboSettingContentDtoList){
                    if(dto!=null){
                        RoboSettingContentResponseDto roboSettingContentResponseDto = new RoboSettingContentResponseDto();
                        roboSettingContentResponseDto.setId(dto.getId());
                        roboSettingContentResponseDto.setType(dto.getType());
                        roboSettingContentResponseDto.setContent(dto.getContent());
                        roboSettingContentResponseDto.setStatus(dto.getStatus());
                        roboSettingContentResponseDtoList.add(roboSettingContentResponseDto);
                    }
                }
            }

            //查询所有的聊天匹配信息
            List<RoboChatDictDto> roboChatDictDtoList = roboSettingService.getRoboChatDictList(user.getUserId(),uin);
            List<RoboChatDictResponseDto> roboChatDictResponseDtoList = new ArrayList<RoboChatDictResponseDto>();
            if(roboChatDictDtoList!=null&&roboChatDictDtoList.size()>0){
                for(RoboChatDictDto dto:roboChatDictDtoList){
                    if(dto!=null){
                        RoboChatDictResponseDto roboChatDictResponseDto = new RoboChatDictResponseDto();
                        roboChatDictResponseDto.setId(dto.getId());
                        roboChatDictResponseDto.setChatKey(dto.getChatKey());
                        roboChatDictResponseDto.setChatValue(dto.getChatValue());
                        roboChatDictResponseDtoList.add(roboChatDictResponseDto);
                    }
                }
            }

            Map<String,Object> resultMap = new HashMap<String, Object>();
            resultMap.put("switchList",roboSwitchStatsticsResponseDtoList);
            resultMap.put("contentList",roboSettingContentResponseDtoList);
            resultMap.put("chatDicList",roboChatDictResponseDtoList);

            return succResponse(CommConstants.OPERATOR_SUCC,resultMap);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }

    /**
     * 查询所有机器人设置信息列表
     * @return
     */
    @GetMapping("/robo/switch/statstics/list")
    public ResponseEntity<BaseResponseDto> roboSwitchStatisticsAllList() {
        try {
            final String uin = getRequestParamAndCheckNull("uin");//微信uin

            UserLoginInfoDto user = getUserAndVerifyToken();

            _logger.info("uin=["+uin+"] userId=["+user.getUserId()+"]");

            //查询所有机器人开关列表信息
            List<RoboSwitchStatsticsDto> roboSwitchStatsticsDtoList = roboSettingService.getAllRoboSwitchStaticsList(user.getUserId(),uin);
            List<RoboSwitchStatsticsResponseDto> roboSwitchStatsticsResponseDtoList = new ArrayList<RoboSwitchStatsticsResponseDto>();
            if(roboSwitchStatsticsDtoList!=null&&roboSwitchStatsticsDtoList.size()>0){
                for(RoboSwitchStatsticsDto dto:roboSwitchStatsticsDtoList){
                    if(dto!=null){
                        RoboSwitchStatsticsResponseDto roboSwitchStatsticsResponseDto = new RoboSwitchStatsticsResponseDto();
                        roboSwitchStatsticsResponseDto.setSwitchStatus(dto.getSwitchStatus());
                        roboSwitchStatsticsResponseDto.setSwitchType(dto.getSwitchType());
                        roboSwitchStatsticsResponseDtoList.add(roboSwitchStatsticsResponseDto);
                    }
                }
            }

            Map<String,Object> resultMap = new HashMap<String, Object>();
            resultMap.put("switchList",roboSwitchStatsticsResponseDtoList);

            return succResponse(CommConstants.OPERATOR_SUCC,resultMap);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }


    /**
     * 发送配置更新请求
     * @throws Exception
     */
    /*private void sendWebsocketUpdateSetting(BaseRequestModel requestModel, String uin) throws Exception {
        //生成webchat并通过webscoket 发送到 python
        _logger.info("websocket update setting");
        WebSocketMsgModel websocketMsg = new WebSocketMsgModel();
        websocketMsg.setUserId(requestModel.getUserId());
        websocketMsg.setToken(requestModel.getToken());
        websocketMsg.setMessageId(requestModel.getUserId());
        websocketMsg.setMethod(WebsocketMethodEnum.updateSetting.getValue());
        Map<String,String> data = new HashMap<>();
        data.put("uin",uin);
        websocketMsg.setDate(data);
        websocketMsg.setTimeStamp(System.currentTimeMillis()+"");
        sendWebsocketApi(websocketMsg);
    }*/

    private void updateConfig(String userId,String uin) throws BusinessException, IOException, TimeoutException, Exception {

        RoboBasicInfoDto dto = roboInfoService.getRoboBasicInfoByUin(userId,uin, "");
            if(dto == null){
            _logger.info("uin＝［"+uin+"］不存在");
            throw new BusinessException(CommConstants.NOT_FUND);
        }
        String robotId = dto.getRoboId();
        RabbitMQUtils.updateConfig(robotId);
    }
}

