package com.robo.server.web.controller.user;

import com.common.base.CommConstants;
import com.common.base.exception.BusinessException;
import com.common.base.response.BaseResponseDto;
import com.common.utils.CommonUtils;
import com.robo.server.web.base.WebServerController;
import com.robo.server.web.em.cache.CacheStatusEnum;
import com.robo.server.web.em.cache.CacheUserEnum;
import com.robo.server.web.em.user.VerCodeEnum;
import com.robo.server.web.entity.user.UserLoginInfoDto;
import com.robo.server.web.message.UserMessage;
import com.robo.server.web.user.UserInfoService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/v1/user/common")
public class VerCodeController extends WebServerController {

    private static final Logger _logger = LoggerFactory.getLogger(VerCodeController.class);

    @Autowired
    private UserInfoService userInfoService;

    @GetMapping("/vercode")
    public ResponseEntity<BaseResponseDto> vercode(){
        try {
            final String mobile = getRequestParamAndCheckNull("mobile");
            final String type = getRequestParamAndCheckNull("type");

            _logger.info("mobile=["+mobile+"] type=["+type+"]");

            if(!VerCodeEnum.getAllMap().containsKey(type)){
                return failResponse(CommConstants.ILLEGALITY_PARAMETER);
            }

            //根据手机号码查询用户信息
            UserLoginInfoDto user = userInfoService.getUserLoginInfoByMobile(mobile);

            if(type.equals(VerCodeEnum.REGISTER.getValue())){
                //注册
               if(null != user){
                   return failResponse(UserMessage.RegisterMessage.MOBILE_ALREADY_EXIST);
               }
            }else if(type.equals(VerCodeEnum.FORGET_PASSWORD.getValue())){
                //忘记密码
                if(null == user){
                    return failResponse(UserMessage.RegisterMessage.MOBILE_NOT_EXIST);
                }
            }
            //生成token
            final String random = CommonUtils.createRandom(true,6);
            redis.set(CacheUserEnum.VERCODE.getValue()+mobile,random,60*5); //缓存五分钟

            //todo 发送验证码

            return succResponse("验证码已经发送,(测试－－》)"+random);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }


    @PostMapping("/vercode")
    public ResponseEntity<BaseResponseDto> checkVerCode(@RequestBody Map<String, Object> paramMap){
        try {
            final String vercode = getMapParamAndCheckNull("vercode",paramMap);
            final String mobile = getMapParamAndCheckNull("mobile",paramMap);
            //获取缓存中code
            final String cacheVerCode = redis.getString(CacheUserEnum.VERCODE.getValue()+mobile);
            if(isBlank(cacheVerCode)){
                _logger.info(UserMessage.VERCODE_TIMOUT);
                return failResponse(UserMessage.VERCODE_TIMOUT);
            }

            if(StringUtils.isBlank(cacheVerCode) || !vercode.equals(cacheVerCode)){
                if(!CommConstants.TEST_CODE.equals(vercode)){
                    _logger.info(UserMessage.VERCODE_FAIL);
                    return failResponse(UserMessage.VERCODE_FAIL);
                }
            }
            //储存状态缓存
            redis.set(CacheStatusEnum.PASS+mobile,vercode,5*60);
            return succResponse(UserMessage.VERCODE_SUCC);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }

}
