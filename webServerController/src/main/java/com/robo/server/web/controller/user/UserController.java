package com.robo.server.web.controller.user;

import com.common.base.CommConstants;
import com.common.base.exception.BusinessException;
import com.common.base.response.BaseResponseDto;
import com.common.utils.CheckUtils;
import com.common.utils.CommonUtils;
import com.common.utils.MD5Util;
import com.common.utils.SHAUtil;
import com.robo.server.web.base.WebServerController;
import com.robo.server.web.em.cache.CacheStatusEnum;
import com.robo.server.web.em.user.UserLoginEnum;
import com.robo.server.web.em.user.UserStatusEnum;
import com.robo.server.web.em.user.VerCodeEnum;
import com.robo.server.web.entity.robo.RoboBasicInfoDto;
import com.robo.server.web.entity.user.UserBasicInfoDto;
import com.robo.server.web.entity.user.UserLoginInfoDto;
import com.robo.server.web.message.UserMessage;
import com.robo.server.web.mq.RabbitMQUtils;
import com.robo.server.web.robo.RoboInfoService;
import com.robo.server.web.user.UserInfoService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping(value = "/v1/user")
public class UserController extends WebServerController {

    private static final Logger _logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private VerCodeController verCodeController;

    @Autowired
    private UserInfoService userInfoService;


    @Autowired
    private RoboInfoService roboInfoService;

    /**
     * 注册
     * @param paramMap
     * @return
     */
    @PostMapping("/register")
    public ResponseEntity<BaseResponseDto> register(@RequestBody Map<String,Object> paramMap){
        try {
            final String mobile = getMapParamAndCheckNull("mobile",paramMap);
            final String password = getMapParamAndCheckNull("password",paramMap);

            _logger.info("mobile=["+mobile+"]  password.size = [" + password.length() + "]");

            CheckUtils.checkMoible(mobile);

            //验证码验证
            ResponseEntity<BaseResponseDto> reponseDto = verCodeController.checkVerCode(paramMap);
            if(!reponseDto.getBody().getSuccess()){
                return reponseDto;
            }
            //验证是否已经注册
            if(userInfoService.getUserLoginInfoByMobile(mobile) != null){
                return failResponse(UserMessage.RegisterMessage.MOBILE_ALREADY_EXIST);
            }
            //clearToken(mobile); //清除token
            UserLoginInfoDto userInfoDto = new UserLoginInfoDto();
            userInfoDto.setMobile(mobile);
            //先进行SHA256加密，然后再进行MD5加密
            String passwd_md5 = MD5Util.string2MD5(SHAUtil.SHA256(password));
            userInfoDto.setPassword(passwd_md5);
            userInfoDto.setStatus(UserStatusEnum.NORMAL.getValue());

            UserLoginInfoDto registerDto =  userInfoService.saveUserLoginInfo(userInfoDto);
            if(registerDto!=null) {
                //注册成功
                Map<String,String> map = this.setToken(mobile,"");
                Map<String,Object> resultMap = new HashMap<String, Object>();
                resultMap.put("mobile",mobile);
                resultMap.put("userId",registerDto.getUserId());
                resultMap.putAll(map);
                //发送websocket消息
                //sendWebsocketLogin(registerDto.getUserId(),map.get("token"));
                return succResponse(UserMessage.RegisterMessage.RESIGER_SUCC, resultMap);
            }
            return failResponse(HttpStatus.OK.value(),UserMessage.RegisterMessage.RESIGER_FAIL, this.setToken(mobile,""));
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }

    /**
     * 登录
     * @param paramMap
     * @return
     */
    @PostMapping("/login")
    public ResponseEntity<BaseResponseDto> login(@RequestBody Map<String,Object> paramMap){
        try {
            final String type = getMapParamAndCheckNull("type",paramMap);//0：手机号密码登录 1：token登录
            String mobile = getMapParam("mobile",paramMap);
            final String password = getMapParam("password",paramMap);
            _logger.info("type=["+type+"] mobile=["+mobile+"]");
            String nickName = "";
            UserLoginInfoDto user = null;
            Map<String,String> tokenMap = new HashMap<>();
            if(type.equals(UserLoginEnum.MOBILE.getValue())) {
                //手机登录
                checkParamNull(mobile,UserMessage.FieldVALID.MOBILE);
                checkParamNull(password,UserMessage.FieldVALID.PASSWORD);
                CheckUtils.checkMoible(mobile);
                user = userInfoService.getUserLoginInfoByMobile(mobile);
                if(null == user){
                    return failResponse(UserMessage.LoginMessage.NOT_REGISTER);
                }
                //先进行SHA256加密，然后再进行MD5加密
                String passwd_md5 = MD5Util.string2MD5(SHAUtil.SHA256(password));
                if(!passwd_md5.equals(user.getPassword())){
                    return failResponse(UserMessage.LoginMessage.LOGIN_FAIL);
                }
                if(UserStatusEnum.FROST.getIntValue().equals(user.getStatus())){
                    return failResponse(UserMessage.LoginMessage.STATUS_FROST);
                }
                //清除以前的token
                this.clearToken(mobile);
            }else if(type.equals(UserLoginEnum.TOKEN.getValue())){
                //token登录
                user = getUserAndVerifyToken(paramMap);
                if(null == user){
                    return failResponse(UserMessage.LoginMessage.NOT_REGISTER);
                }
            }else{
                return failResponse(CommConstants.ILLEGALITY_PARAMETER);
            }

            user.NullReplaceBlank();
            Map<String,Object> resultMap = new HashMap<String, Object>();
            resultMap.put("mobile",user.getMobile());
            resultMap.put("userId",user.getUserId());
            if(!isBlank(mobile)) {
                tokenMap = this.setToken(mobile, nickName);
                //检查是否有在线的机器人， 更新userId和token
                List<RoboBasicInfoDto> onlineRobotList = roboInfoService.getListByOnlie();
                if(onlineRobotList != null && onlineRobotList.size()>0) {
                    String token = tokenMap.get("token");
                    String userId = user.getUserId();
                    for (RoboBasicInfoDto dto : onlineRobotList) {
                        if(!isBlank(dto.getRoboId())) {
                            _logger.info("update python user ==>  robotId=["+dto.getRoboId()+"] userid=["+userId+"] token=["+dto.getRoboId()+"]" );
                            RabbitMQUtils.updateToken(dto.getRoboId(),userId,token);
                        }
                    }
                }
            }
            //发送websocket消息
            //sendWebsocketLogin(user.getUserId(),tokenMap.get("token"));
            resultMap.putAll(tokenMap);
            return succResponse(UserMessage.LoginMessage.LOGIN_SUCC,resultMap);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }


    /**
     * 安全退出
     * @return
     */
    @GetMapping("/loginout")
    public ResponseEntity<BaseResponseDto> logout(){
        try {
            final String token = getRequestParamAndCheckNull("token");
            UserLoginInfoDto user = getUserAndVerifyToken();
            _logger.info("logout --> "+user.toLogger());
            this.clearToken(user.getMobile());
            return succResponse(UserMessage.LoginMessage.LOGIN_OUT);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }

    /**
     * 忘记密码
     * @param paramMap
     * @return
     */
    @PostMapping("/password/forget")
    public ResponseEntity<BaseResponseDto> forgetPassWord(@RequestBody Map<String,Object> paramMap){
        try {
            final String passwd = getMapParamAndCheckNull("password",paramMap);
            final String mobile = getMapParamAndCheckNull("mobile",paramMap);
            _logger.info("passwd.length ==>"+passwd.length());
            final String cecheKey = CacheStatusEnum.PASS+mobile;
            //判断状态码
            final String status = redis.getString(cecheKey);
            if(StringUtils.isBlank(status)){
                return failResponse(CommConstants.OPERATOR_TIME_OUT);
            }
            UserLoginInfoDto user = userInfoService.getUserLoginInfoByMobile(mobile);
            //先进行SHA256加密，然后再进行MD5加密
            String passwd_md5 = MD5Util.string2MD5(SHAUtil.SHA256(passwd));
            userInfoService.updatePasswordByUserId(user.getUserId(),passwd_md5);
            //清除状态码
            redis.delKey(cecheKey);
            //删除验证码
            delCacheVerCode(mobile);
            return succResponse(CommConstants.OPERATOR_SUCC);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }

    /**
     * 根据手机号码生成token，并存入缓存
     * @param mobile
     * @return
     */
    private Map<String,String> setToken(final String mobile, final String nickName) throws Exception {
        final String token = UUID.randomUUID().toString();
        redis.set(mobile,token);
        UserLoginInfoDto user = userInfoService.getUserLoginInfoByMobile(mobile);
        if(user == null){
            throw new BusinessException(UserMessage.RegisterMessage.MOBILE_NOT_EXIST);
        }

        redis.set(token, user);
        Map<String,String> map = new HashMap<String,String>();
        map.put("token", token);
        return map;
    }

    /**
     * 将mobile关联的token置为无效
     */
    private void clearToken(final String mobile){
        if(!isBlank(mobile)) {
            final String token = redis.getString(mobile);
            if (!StringUtils.isBlank(token)) {
                redis.delKey(mobile);
                redis.delKey(token);
            }
        }
    }


    /**
     * 保存用户基本信息
     */
    @PostMapping("/info")
    public ResponseEntity<BaseResponseDto> userBasicInfo(@RequestBody Map<String,Object> paramMap) {
        try {

            final String nickName = getMapParam("nickName",paramMap);//昵称
            final String comWechatScale = getMapParam("comWechatScale",paramMap);//企业微信群规模
            final String profession = getMapParam("profession",paramMap);//行业
            final String trusteeshipCount = getMapParam("trusteeshipCount",paramMap);//拟托管运行群数目

            //验证token，并同时获取用户信息
            UserLoginInfoDto user = getUserAndVerifyToken(paramMap);

            UserBasicInfoDto userBasicInfoDto = new UserBasicInfoDto();
            userBasicInfoDto.setUserId(user.getUserId());
            userBasicInfoDto.setNickName(nickName);
            userBasicInfoDto.setComWechatScale(comWechatScale);
            userBasicInfoDto.setProfession(profession);
            userBasicInfoDto.setTrusteeshipCount(trusteeshipCount);

            userBasicInfoDto = userInfoService.saveUserBasicInfo(userBasicInfoDto);
            if(userBasicInfoDto ==null){
                return failResponse(CommConstants.OPERATOR_FAIL);
            }
            return succResponse(CommConstants.OPERATOR_SUCC);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }


    /**
     * 查询用户基本信息
     */
    @GetMapping("/info")
    public ResponseEntity<BaseResponseDto> getUserInfo() {
        try {

            //验证token，并同时获取用户信息
            UserLoginInfoDto user = getUserAndVerifyToken();

            UserBasicInfoDto userBasicInfoDto = userInfoService.getUserBasicInfoById(user.getUserId());
            if(userBasicInfoDto ==null){
                //userBasicInfoDto = new UserBasicInfoDto();
                return failResponse(CommConstants.NOT_FUND);
            }
            userBasicInfoDto.NullReplaceBlank();
            return succResponse(CommConstants.OPERATOR_SUCC,userBasicInfoDto,"basicInfo");
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }



    /**
     * 修改密码或设置密码
     * @return
     */
    @PutMapping("/pwd")
    public ResponseEntity<BaseResponseDto> updatePasswrd(@RequestBody Map<String,Object> paramMap){
        try {
            String oldPassWord = getMapParam("oldPassWord",paramMap);
            final String newPassWord = getMapParamAndCheckNull("newPassWord",paramMap);

            oldPassWord = oldPassWord==null?"":oldPassWord;

            _logger.info("oldPassWord.length ==>"+oldPassWord.length()+"newPassWord.length ==>"+newPassWord.length());

            UserLoginInfoDto user = getUserAndVerifyToken(paramMap);

            UserLoginInfoDto userInfoDto = userInfoService.getUserLoginInfoByUserId(user.getUserId());

            if(!CommonUtils.isBlank(oldPassWord)){
                //修改密码
                if(oldPassWord.equals(newPassWord)){
                    return failResponse(UserMessage.PwdMessage.PWD_SAME);
                }
                //先进行SHA256加密，然后再进行MD5加密
                String oldPassWord_md5 = MD5Util.string2MD5(SHAUtil.SHA256(oldPassWord));
                if(!oldPassWord_md5.equals(userInfoDto.getPassword())){
                    return failResponse(UserMessage.PwdMessage.PWD_UNMATCHED);
                }
            }else{
                //设置密码
                if(!CommonUtils.isBlank(userInfoDto.getPassword())){
                    return failResponse(UserMessage.PwdMessage.PWD_EXIST);
                }
            }

            //先进行SHA256加密，然后再进行MD5加密
            String newPassWord_md5 = MD5Util.string2MD5(SHAUtil.SHA256(newPassWord));
            userInfoService.updatePasswordByUserId(user.getUserId(), newPassWord_md5);

            return succResponse(CommConstants.OPERATOR_SUCC);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }




    /**
     * 修改手机号码
     * @param paramMap
     * @return
     */
    @PutMapping("/mobile")
    public ResponseEntity<BaseResponseDto> updateMobile(@RequestBody Map<String,Object> paramMap){
        try {
            final String mobile = getMapParamAndCheckNull("mobile",paramMap);
            final String token = getMapParamAndCheckNull("token",paramMap);
            final String verCode = getMapParamAndCheckNull("verCode", paramMap);
            UserLoginInfoDto user = getUserAndVerifyToken(paramMap);

            //paramMap.put("type", VerCodeEnum.UPDATE_MOBILE.getValue());

            //BaseReponseDto reponseDto = verCodeController.checkVerCode(paramMap);

            //检验验证码
            //if(reponseDto.getStatus() == CommStatusEnum.SUCC.getIntValue()){

            if("9999".equals(verCode)){

                //验证手机号码是否存在
                if(userInfoService.getUserLoginInfoByMobile(mobile)!=null){
                    return failResponse(UserMessage.RegisterMessage.MOBILE_ALREADY_EXIST);
                }
                userInfoService.updateMobileByUserId(user.getUserId(),mobile);

                //修改成功，更新redis中用户信息
                user.setMobile(mobile);
                redis.set(token,user);
                redis.set(mobile,token);
                return succResponse(CommConstants.OPERATOR_SUCC);
            }else{
               return failResponse("验证失败");
            }
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }




    private void sendWebsocketLogin(String userId,String token) throws Exception {
        //生成webchat并通过webscoket 发送到 phthon
       /* WebSocketMsgModel websocketMsg = new WebSocketMsgModel();
        websocketMsg.setUserId(userId);
        websocketMsg.setToken(token);
        websocketMsg.setMessageId(userId);
        websocketMsg.setMethod(WebsocketMethodEnum.userLogin.getValue());
        websocketMsg.setDate(token);
        websocketMsg.setTimeStamp(System.currentTimeMillis()+"");
        sendWebsocketApi(websocketMsg);*/
    }


}
