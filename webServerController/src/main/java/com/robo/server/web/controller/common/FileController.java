package com.robo.server.web.controller.common;

import com.common.base.CommConstants;
import com.common.base.exception.BusinessException;
import com.common.base.response.BaseResponseDto;
import com.common.utils.BarcodeUtils;
import com.common.utils.CommonUtils;
import com.common.utils.DateFormatUtil;
import com.common.utils.HttpClientUtils;
import com.robo.server.web.base.WebServerController;
import com.robo.server.web.common.FileInfoService;
import com.robo.server.web.em.cache.CacheWebScoketEnum;
import com.robo.server.web.em.common.FileTypeEnum;
import com.robo.server.web.em.common.QrcodeResultEnum;
import com.robo.server.web.entity.File.FileInfoDto;
import com.robo.server.web.entity.File.FileUserMappingDto;
import com.robo.server.web.entity.robo.RoboBasicInfoDto;
import com.robo.server.web.entity.user.UserLoginInfoDto;
import com.robo.server.web.model.qrcode.QrCodeRequestModel;
import com.robo.server.web.mq.RabbitMQUtils;
import com.robo.server.web.robo.RoboInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

/**
 * 文件上传
 */
@RestController
@RequestMapping("/v1/common")
public class FileController extends WebServerController {

    private static final Logger _logger = LoggerFactory.getLogger(FileController.class);

    @Autowired
    private FileInfoService fileInfoService;

    @Autowired
    private RoboInfoService roboInfoService;

    @Value("${static.file.path}")
    private String staticFilePath;

    @Value("${static.base.url}")
    private String staticBaseUrl;

    @Value("${wechat.web.login.url}")
    private String wechatLoginUrl;

    @Value("${wechat.web.getuuid.url}")
    private String wechatGetUUid;

    private static final String imagePath = "/qr/";

    private static final String cacheWechatUUIid = CacheWebScoketEnum.wechatUUid.getValue();
    private static final String cacheRoboId = CacheWebScoketEnum.roboId.getValue();
    private static final String cacheLoginMsg = CacheWebScoketEnum.reboLogin_message_.getValue();

    /**
     * 生成二维码
     * @return
     */
    @PostMapping("/qrCode")
    public ResponseEntity<BaseResponseDto> qrCode(@RequestBody QrCodeRequestModel requestModel) {
        try {

            UserLoginInfoDto user = getUserAndVerifyToken(requestModel);

            requestModel.checkFieldNull();

            _logger.info("requestModel ==> "+requestModel.toLogger());

            String uuid = requestModel.getUuid();

            String robotId = requestModel.getRobotId();

            _logger.info("uuid ＝ ["+uuid+"]");

            if(isBlank(robotId)) {
                robotId = UUID.randomUUID().toString();
            }

            //redis中存储roboId关联关系
            redis.set(cacheRoboId+robotId,user, 60 * 30);

            //当uuid为空时，向python发送启动机器人命令
            if(isBlank(uuid)){

               /* //生成webchat并通过webscoket 发送到 phthon
                WebSocketMsgModel websocketMsg = new WebSocketMsgModel();
                websocketMsg.setUserId(user.getUserId());
                websocketMsg.setToken(requestModel.getToken());
                websocketMsg.setMessageId(user.getUserId());
                websocketMsg.setMethod(WebsocketMethodEnum.sendUUId.getValue());
                websocketMsg.setDate(robotId);
                websocketMsg.setTimeStamp(System.currentTimeMillis()+"");

                BaseResponseDto websocketResponse = sendWebsocketApi(websocketMsg);

                if(websocketResponse!=null && websocketResponse.getSuccess()) {
                    _logger.info("调用websocket方法成功，response ==> " + response);
                    //缓存查询uuid是否有，若有uuid生成，则返回数据
                    for(int i=1; i<=10; i++){
                        String cacheKey = cacheWechatUUIid + user.getUserId();
                        uuid = redis.getString(cacheKey);
                        _logger.info("第"+i+"次获取 uuid ="+uuid);
                        if(!isBlank(uuid)){
                            break;
                        }
                        //一秒钟取一次，取60次
                        Thread.sleep(1000);
                    }
                }*/

                RabbitMQUtils.startRobot(robotId,"");

                for(int i=1; i<=15; i++){
                    String cacheKey = cacheWechatUUIid + user.getUserId();
                    uuid = redis.getString(cacheKey);
                    _logger.info("第"+i+"次获取 uuid ="+uuid);
                    if(!isBlank(uuid)){
                        break;
                    }
                    //一秒钟取一次，取60次
                    Thread.sleep(1000);
                }
            }


            if(isBlank(uuid)){
                _logger.info("未获取到uuid");
                return failResponse(CommConstants.OPERATOR_FAIL);
            }

            String content = wechatLoginUrl.replace("{uuid}",uuid);

            _logger.info("content= ["+content+"]");

            String codeImage = requestModel.getUserId()+".jpg";

            BarcodeUtils.createQrcode(content,staticFilePath+imagePath+codeImage);
            String imageUrl = staticBaseUrl+imagePath+codeImage+"?timestamp=" + System.currentTimeMillis();

            Map<String,String> resultMap = new HashMap<>();
            resultMap.put("imageUrl",imageUrl);
            resultMap.put("uuid",uuid);
            resultMap.put("roboId",robotId);
            //redis中储存roboId
            String cacheKey_roboId = cacheRoboId+user.getUserId();
            redis.set(cacheKey_roboId,robotId);
            return succResponse(CommConstants.OPERATOR_SUCC,resultMap);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }


    /**
     * 查询二维码信息
     * @return
     */
    @GetMapping("/qrCode")
    public ResponseEntity<BaseResponseDto> queryQrCode() {
        try {



            String token = getRequestParamAndCheckNull("token");
            String userId = getRequestParamAndCheckNull("userId");
            UserLoginInfoDto user = getUserAndVerifyToken();


            // web test
           /* String reponseString = HttpClientUtils.getMethodGetResponse(wechatGetUUid);
            _logger.info("reponseString ==> "+reponseString);

            String reponseStrings[] = reponseString.split(";");
            String code = reponseStrings[0];
            String uuid = "";
            if(code.contains("200")){
                String uuidString = reponseStrings[1];
                _logger.info("uuidString ==> "+uuidString);
                uuid = uuidString.substring(uuidString.indexOf("=")+3,uuidString.length()-1);
                _logger.info("uuid ==> " + uuid);
            }else{
                _logger.info("微信服务调用失败");
                return succResponse(CommConstants.OPERATOR_FAIL);
            }*/

            QrCodeRequestModel requestModel = new QrCodeRequestModel();
            requestModel.setUserId(userId);
            requestModel.setToken(token);
            //requestModel.setUuid(uuid);
            return this.qrCode(requestModel);

           /* UserLoginInfoDto user = getUserAndVerifyToken();
            String codeImage = user.getUserId()+".jpg";
            String imageUrl = staticBaseUrl+imagePath+codeImage;*/
            //return succResponse(CommConstants.QUERY_SUCC,imageUrl,"imageUrl");
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }


    /**
     * 查询二维码扫描结果 0：成功 1：失败
     */
    @GetMapping("/qrcode/result")
    public ResponseEntity<BaseResponseDto> getWechatUin() {
        try {

            final String roboId = getRequestParamAndCheckNull("roboId");//机器人ID

            //验证token，并同时获取用户信息
            UserLoginInfoDto user = getUserAndVerifyToken();

            _logger.info("roboId=["+roboId+"] userId=["+user.getUserId()+"]");

            String result = "";
            String message ="";

            //返回微信uin和机器人登录时间
            Map<String, Object> resultMap = new HashMap<String, Object>();

            final String cechaKey = cacheLoginMsg+roboId;
            message = redis.getString(cechaKey);
            if(!isBlank(message)){
                result = QrcodeResultEnum.ERROR.getValue();
            }else {
                //根据用户ID，查询最新生效的机器人登录信息，同时获取uin
                RoboBasicInfoDto roboBasicInfoDto = roboInfoService.getRoboBasicInfo(roboId);
                if (roboBasicInfoDto != null && !CommonUtils.isBlank(roboBasicInfoDto.getUin())) {
                    result = QrcodeResultEnum.SUCCESS.getValue();
                    message = QrcodeResultEnum.getDesc(result);
                } else {
                    result = QrcodeResultEnum.FAILURE.getValue();
                    message = QrcodeResultEnum.getDesc(result);
                }
            }
            resultMap.put("result", result);
            resultMap.put("message", message);

            return succResponse(CommConstants.OPERATOR_SUCC,resultMap);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }

    @RequestMapping("/file")
    public ResponseEntity<BaseResponseDto> photoUpload(@RequestParam("file") MultipartFile files[],
                                                       @RequestParam("fileType") String fileType,
                                                       @RequestParam("userId") String userId,
                                                       @RequestParam("token") String token) {
        try {
            _logger.info("userId =["+userId+"] token=["+token+"] files.lenth=["+files.length+"]");

            UserLoginInfoDto user = getUserAndVerifyToken(userId,token);

            if(!FileTypeEnum.getAllMap().containsKey(fileType)){
                return failResponse(CommConstants.ILLEGALITY_PARAMETER);
            }

            if(files == null || files.length<0){
                return failResponse("上传失败");
            }

            List<FileInfoDto> fileList = new ArrayList<FileInfoDto>();
            //根据type 保存至不同目录
            String rootPath = FileTypeEnum.getDesc(fileType);
            for(int i=0; i<files.length; i++) {
               if (!files[i].isEmpty()) {
                   String fileName = files[i].getOriginalFilename();
                   String[] fileNameArr = fileName.split("\\.");
                   String ext = "";
                   if(fileNameArr!=null&&fileNameArr.length>0){
                       ext = fileNameArr[fileNameArr.length-1];
                   }
                   String saveName = UUID.randomUUID() +"."+ ext;
                   String savePath = rootPath + DateFormatUtil.getCurrentTime("yyyyMM")+"/"+user.getUserId()+"/";

                   //文件基本信息
                   FileInfoDto fileInfoDto = new FileInfoDto();
                   fileInfoDto.setName(fileName);
                   fileInfoDto.setSaveName(saveName);
                   fileInfoDto.setSavePath(savePath);
                   fileInfoDto.setExt(ext);
                   fileInfoDto.setSize((int)files[i].getSize());

                   //保存文件
                   CommonUtils.saveFile(files[i], staticFilePath+"/"+savePath, saveName);
                   //保存文件到数据库
                   fileInfoDto = fileInfoService.saveFileInfo(fileInfoDto);
                   if(fileInfoDto!=null){
                       //保存文件与用户关联关系
                       FileUserMappingDto fileUserMappingDto = new FileUserMappingDto();
                       fileUserMappingDto.setUserId(user.getUserId());
                       fileUserMappingDto.setFileId(fileInfoDto.getFileId());
                       fileUserMappingDto.setFileType(fileType);
                       fileUserMappingDto = fileInfoService.saveFileUserMapping(fileUserMappingDto);
                       fileList.add(fileInfoDto);
                   }

               }
           }
           return succResponse(CommConstants.OPERATOR_SUCC,fileList,"fileList");
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }

    /**
     * 查询文件
     * @return
     */
    @GetMapping("/file")
    public ResponseEntity<BaseResponseDto> getFileInfo() {
        try {
            final String fileId = getRequestParamAndCheckNull("fileId");

            //根据id，查询文件信息
            FileInfoDto fileInfoDto = fileInfoService.getFileInfoById(fileId);

            return succResponse(CommConstants.QUERY_SUCC,fileInfoDto);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }






}

