package com.robo.server.web.controller.robo;

import com.common.base.CommConstants;
import com.common.base.exception.BusinessException;
import com.common.base.model.MyPageResult;
import com.common.base.response.BaseResponseDto;
import com.common.utils.CommonUtils;
import com.common.utils.HttpClientUtils;
import com.robo.server.web.base.WebServerController;
import com.robo.server.web.em.cache.CacheRobotImgEnum;
import com.robo.server.web.em.cache.CacheWebScoketEnum;
import com.robo.server.web.em.robo.GroupAuthorizeStatusEnum;
import com.robo.server.web.em.robo.RoboCurrentStatusEnum;
import com.robo.server.web.em.robo.RoboLoginStatusEnum;
import com.robo.server.web.entity.robo.RoboBasicInfoDto;
import com.robo.server.web.entity.robo.RoboLoginInfoDto;
import com.robo.server.web.entity.robo.group.GroupBasicInfoDto;
import com.robo.server.web.entity.robo.response.RoboBasicInfoResponseDto;
import com.robo.server.web.entity.robo.response.RoboInfoResponseDto;
import com.robo.server.web.entity.user.UserLoginInfoDto;
import com.robo.server.web.message.RoboMessage;
import com.robo.server.web.mq.RabbitMQUtils;
import com.robo.server.web.robo.GroupInfoService;
import com.robo.server.web.robo.RoboInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
*
* @user jianghaoming
* @date 2017-07-11 22:16:36
*/
@RestController
@RequestMapping("/v1/robocat")
public class RoboInfoController extends WebServerController{

    private static final Logger _logger = LoggerFactory.getLogger(RoboInfoController.class);

    @Autowired
    private RoboInfoService roboInfoService;

    @Autowired
    private GroupInfoService groupInfoService;


    @Value("${static.file.path}")
    private String staticFilePath;

    private static final String headImage = "/headImg/";

    private static final String cacheRoboId = CacheWebScoketEnum.roboId.getValue();

    private static final String cacheLoginMsg = CacheWebScoketEnum.reboLogin_message_.getValue();

    private static final String cacheHeadImg = CacheRobotImgEnum.HEADIMG.getValue();

    /**
     * 根据robotid 获取头像
     */
    @GetMapping("/headimg/{robotId}")
    public void getHeadImg(@PathVariable("robotId") String robotId) {
        try {

            final String headImgFilePath = staticFilePath + headImage;
            final String defaultHeadImg = "default.jpeg";

            _logger.info("robotId = ["+robotId+"]");
            if(isBlank(robotId)){
                _logger.info("robotId is null");
                return;
            }

            final String cacheKey = cacheHeadImg+robotId;

            Object imgDataObj = redis.getObject(cacheKey);

            byte[] data = null;

            if(imgDataObj == null) {

                RoboBasicInfoDto dto = roboInfoService.getRoboBasicInfoNoStatus(robotId);

                if (dto == null) {
                    _logger.info("robot dto not fund");
                    return;
                }

                final String robotHeadImg = isBlank(dto.getHeadImg()) ? defaultHeadImg : dto.getHeadImg();
                final String robotHeadImgFile = headImgFilePath + robotHeadImg;
                _logger.info("headimg file is ＝＝》" + robotHeadImgFile);

                File file = new File(robotHeadImgFile);
                if (!(file.exists() && file.canRead())) {
                    file = new File(headImgFilePath + defaultHeadImg);
                }

                FileInputStream inputStream = new FileInputStream(file);
                data = new byte[(int) file.length()];
                int length = inputStream.read(data);
                inputStream.close();
                redis.set(cacheKey,data, 60 * 60 * 1);
            }else{
                _logger.info("img cache read .....");
                data = (byte[]) imgDataObj;
            }

            response.setContentType("image/png");
            OutputStream stream = response.getOutputStream();
            stream.write(data);
            stream.flush();
            stream.close();

        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
        }
    }


    /**
     * 根据robotid 查询userId与token
     */
    @GetMapping("/userInfo")
    public ResponseEntity<BaseResponseDto> getUserInfoByRobotId() {
        try {

            final String robotId = getRequestParamAndCheckNull("robotId");

            UserLoginInfoDto user = redis.getObject(cacheRoboId+robotId);

            if(user == null){
                return failResponse(CommConstants.NOT_FUND);
            }

            String token = redis.getString(user.getMobile());

            Map<String,String> resultMap = new HashMap<>();
            resultMap.put("userId",user.getUserId());
            resultMap.put("token",token);

            return succResponse(CommConstants.OPERATOR_SUCC,resultMap);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }


    /**
     * 保存机器人登录信息
     */
    @PostMapping("/robo/login")
    public ResponseEntity<BaseResponseDto> roboLogin(@RequestBody Map<String,Object> paramMap) {
        try {

            final String loginStatus = getMapParam("loginStatus",paramMap);//登出状态 0: login
            final String deviceType = getMapParam("deviceType",paramMap);//设备类型 0:winclient  1:pyclient  2:app

            //验证token，并同时获取用户信息
            UserLoginInfoDto user = getUserAndVerifyToken(paramMap);

            final String roboId = redis.getString(CacheWebScoketEnum.roboId.getValue()+user.getUserId());

            _logger.info("roboId=["+roboId+"] loginStatus=["+loginStatus+"] deviceType=["+deviceType+"] userId=["+user.getUserId()+"]");

            RoboLoginInfoDto roboLoginInfoDto = new RoboLoginInfoDto();
            roboLoginInfoDto.setRoboId(roboId);
            roboLoginInfoDto.setUserId(user.getUserId());
            roboLoginInfoDto.setLoginStatus(loginStatus);
            roboLoginInfoDto.setDeviceType(deviceType);

            roboLoginInfoDto = roboInfoService.saveUserLoginInfo(roboLoginInfoDto);
            if(roboLoginInfoDto ==null){
                return failResponse(CommConstants.OPERATOR_FAIL);
            }

            Map<String,Object> resultMap = new HashMap<String, Object>();
            resultMap.put("roboId", roboLoginInfoDto.getRoboId());

            return succResponse(CommConstants.OPERATOR_SUCC,resultMap);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }

    /**
     * 更新/保存机器人基本信息
     */
    @PostMapping("/robo/info")
    public ResponseEntity<BaseResponseDto> roboInfo(@RequestBody Map<String,Object> paramMap) {
        try {

            final String uin = getMapParam("uin",paramMap);
            final String nickName = getMapParam("nickName",paramMap);
            final String pyInitial = getMapParam("pyInitial",paramMap);
            final String remarkName = getMapParam("remarkName",paramMap);
            final String remarkPyInitial = getMapParam("remarkPyInitial",paramMap);
            final String province = getMapParam("province",paramMap);
            final String city = getMapParam("city",paramMap);
            final String sex = getMapParam("sex",paramMap);
            final String signature = getMapParam("signature",paramMap);
            final String snsFlag = getMapParam("snsFlag",paramMap);
            final String pythonId = getMapParam("pid",paramMap);




            final String wxsid = getMapParam("wxsid",paramMap);
            final String headImgUrl = getMapParam("headImgUrl",paramMap);
            final String authTicket = getMapParam("authTicket",paramMap);
            final String dataTicket = getMapParam("dataTicket",paramMap);
            final String mmLang = getMapParam("mmLang",paramMap);
            final String wxuvid = getMapParam("wxuvid",paramMap);
            final String wxloadtime = getMapParam("wxloadtime",paramMap);

            //验证token，并同时获取用户信息
            UserLoginInfoDto user = getUserAndVerifyToken(paramMap);

            final String roboId = redis.getString(CacheWebScoketEnum.roboId.getValue()+user.getUserId());

            _logger.info("paramMap ==> "+paramMap);

            _logger.info("roboId=["+roboId+"] uin=["+uin+"] nickName=["+nickName+"] pyInitial=["+pyInitial+"] " +
                    "remarkName=["+remarkName+"] remarkPyInitial=["+remarkPyInitial+"] province=["+province+"] city=["+city+"] " +
                    "sex=["+sex+"] signature=["+signature+"] snsFlag=["+snsFlag+"] userId=["+user.getUserId()+"] pythonId=["+pythonId+"]");

            RoboBasicInfoDto roboBasicInfoDto = new RoboBasicInfoDto();
            roboBasicInfoDto.setUserId(user.getUserId());
            roboBasicInfoDto.setRoboId(roboId);
            roboBasicInfoDto.setUin(uin);
            roboBasicInfoDto.setNickName(nickName);
            roboBasicInfoDto.setPyInitial(pyInitial);
            roboBasicInfoDto.setRemarkName(remarkName);
            roboBasicInfoDto.setRemarkPyInitial(remarkPyInitial);
            roboBasicInfoDto.setProvince(province);
            roboBasicInfoDto.setCity(city);
            roboBasicInfoDto.setSex(sex);
            roboBasicInfoDto.setSignature(signature);
            roboBasicInfoDto.setSnsFlag(snsFlag);
            roboBasicInfoDto.setCurrentStatus(RoboCurrentStatusEnum.login_ing.getValue());
            roboBasicInfoDto.setPid(pythonId);

            final String cacheKey = cacheHeadImg+roboId;

            Object imgDataObj = redis.getObject(cacheKey);

            if(imgDataObj == null) {
                //生成用户头像
                String headImg = this.saveHeadImg(uin, wxsid, dataTicket, authTicket, wxloadtime, wxuvid, mmLang, headImgUrl, roboId);
                roboBasicInfoDto.setHeadImg(headImg);
            }

            roboBasicInfoDto = roboInfoService.saveUserBasicInfo(roboBasicInfoDto);
            if(roboBasicInfoDto ==null){
                return failResponse(CommConstants.OPERATOR_FAIL);
            }

            Map<String,Object> resultMap = new HashMap<String, Object>();
            resultMap.put("uin", roboBasicInfoDto.getUin());
            //通知python绑定uin信息
            //RabbitMQUtils.bingdUin(roboId,uin);

            return succResponse(CommConstants.OPERATOR_SUCC,paramMap);

        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }


    private String saveHeadImg(String uin, String sid, String dataTicket, String authTicket,String wxloadtime, String wxuvid, String mmLang, String headUrl, String roboId){
        try {

            //2296710362  6H/ysVLsj22F7LTV  IhaYkPUDVDZeLHy7Xpdi84SK%2BuKjPC%2B7xswxMWTEok2wgKBy0KMtmn5%2Fu%2FXuKF3l
            // https://wx.qq.com/cgi-bin/mmwebwx-bin/cgi-bin/mmwebwx-bin/webwxgeticon?seq=626226559&username=@faaac6a7fa9ee1d6d6e89c3a7512cb96a4d3e521de7d1a6f67f39d6172e1af0d&skey=@crypt_cc89cfa7_a0fceada4e81c6a1157694cd5b977281
            Map<String,String> headMap = new HashMap<>();
            String cookieVal =  "wxuin="+uin+";wxsid="+sid+";webwx_data_ticket="+dataTicket+";webwx_auth_ticket="+authTicket+";mm_lang="+mmLang+";webwxuvid="+wxuvid+";wxloadtime="+wxloadtime+";";
            headMap.put("Cookie",cookieVal);
            headMap.put("User-Agent","Mozilla/5.0 (X11; Linux i686; U;) Gecko/20070322 Kazehakase/0.4.5");
            _logger.info("headparam ==> "+ headMap);
            _logger.info("headImgurl ==> "+ headUrl);
            byte[] response = HttpClientUtils.getMethodGetContent(headUrl,headMap);

            if(response == null || response.length<=0){
                return  "";
            }

            _logger.info("head img byte ==> "+response);

            String imageName = uin +".jpg";
            FileOutputStream fileOutputStream = new FileOutputStream(new File(  staticFilePath + headImage + imageName));
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            output.write(response);
            fileOutputStream.write(output.toByteArray());
            fileOutputStream.close();

            return imageName;

        }catch (Exception e){
            _logger.error("生成用户头像失败",e);
            return  "";
        }
    }


    /**
     * 根据机器人Id，查询机器人登录/登出信息和基本信息
     */
    @GetMapping("/robo/detail")
    public ResponseEntity<BaseResponseDto> getRoboInfoDetail() {
        try {

            final String roboId = getRequestParamAndCheckNull("roboId");//机器人Id

            //验证token，并同时获取用户信息
            UserLoginInfoDto user = getUserAndVerifyToken();

            _logger.info("roboId=["+roboId+"] userId=["+user.getUserId()+"]");

            //根据机器人Id，查询机器人登录/登出信息
            RoboLoginInfoDto roboLoginInfoDto = roboInfoService.getRoboLoginInfoByRoboId(roboId);

            //根据机器人Id，查询机器人基本信息
            RoboBasicInfoDto roboBasicInfoDto = roboInfoService.getRoboBasicInfo(roboId);

            Map<String,Object> resultMap = new HashMap<String, Object>();
            resultMap.put("roboLoginInfo", roboLoginInfoDto);
            resultMap.put("roboBasicInfo", roboBasicInfoDto);

            return succResponse(CommConstants.QUERY_SUCC,resultMap);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }

    /**
     * 根据用户Id，查询查询所有的机器人登出信息和基本信息列表（分页）
     */
    @GetMapping("/robo/list")
    public ResponseEntity<BaseResponseDto> getRoboInfoPage() {
        try {

            final int pageNumber = getRequestParamIntAndCheckNull("pageNumber");//当前页码
            final int pageSize = getRequestParamIntAndCheckNull("pageSize");//每页条数

            //验证token，并同时获取用户信息
            UserLoginInfoDto user = getUserAndVerifyToken();

            _logger.info("pageNumber=["+pageNumber+"] pageSize=["+pageSize+"] userId=["+user.getUserId()+"]");

            MyPageResult<RoboInfoResponseDto> myPageResult = roboInfoService.getRoboInfoPage(user.getUserId(),setPage(pageNumber,pageSize));
            if(myPageResult!=null&&!CommonUtils.isBlank(myPageResult.getResultList())){
                for(RoboInfoResponseDto dto:myPageResult.getResultList()){
                    if(dto!=null&&!CommonUtils.isBlank(dto.getUin())){
                        List<GroupBasicInfoDto> groupList = groupInfoService.getAuthorizedGroupBasicInfoList(user.getUserId(),dto.getUin(),"", GroupAuthorizeStatusEnum.YES.getValue(),"");
                        dto.setGroupList(groupList);
                    }
                }
            }

            Map<String,Object> resultMap = new HashMap<String, Object>();
            resultMap.put("roboInfoList", myPageResult.getResultList());
            resultMap.put("totalCount", myPageResult.getTotalCount());

            return succResponse(CommConstants.QUERY_SUCC,resultMap);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }


    /**
     * 保存/更新机器人状态信息
     */
    @PostMapping("/robo/status")
    public ResponseEntity<BaseResponseDto> roboStatus(@RequestBody Map<String,Object> paramMap) {
        try {

            final String roboId = getMapParamAndCheckNull("roboId",paramMap);//机器人ID
            final String status = getMapParamAndCheckNull("status",paramMap);//机器人当前状态 0: 未登录 1: 正常登录中 2：正常退出 3：异常退出 9：取消追踪

            final String memo = getMapParam("memo",paramMap);
            //验证token，并同时获取用户信息
            UserLoginInfoDto user = getUserAndVerifyToken(paramMap);

            _logger.info("roboId=["+roboId+"] status=["+status+"] userId=["+user.getUserId()+"] memo=["+memo+"]");

            if(!RoboCurrentStatusEnum.getAllMap().containsKey(status)){
                throw new BusinessException(RoboMessage.message.status_not_exist);
            }

            if(RoboCurrentStatusEnum.loginout.getValue().equals(status)||
                    RoboCurrentStatusEnum.loginout_exception.getValue().equals(status)){
                //登出
                RoboLoginInfoDto roboLoginInfoDto = new RoboLoginInfoDto();
                roboLoginInfoDto.setRoboId(roboId);
                roboLoginInfoDto.setUserId(user.getUserId());
                roboLoginInfoDto.setLoginStatus(RoboLoginStatusEnum.loginout.getValue());
                roboLoginInfoDto.setDeviceType("");
                roboInfoService.saveUserLoginInfo(roboLoginInfoDto);

                if(!isBlank(memo)) {
                    //保存redis信息
                    final String cacheKey = cacheLoginMsg + roboId;
                    redis.set(cacheKey, memo, 60 * 60);
                }

                //通知python kill 进程
                RabbitMQUtils.closeRobot(roboId);
            }

            int count = roboInfoService.updateRoboCurrentStatusByRoboId(roboId,status,memo);
            if(count == 0){
                return failResponse(CommConstants.OPERATOR_FAIL);
            }

            return succResponse(CommConstants.OPERATOR_SUCC);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }


    /**
     * 根据用户Id,查询所有微信机器人信息
     */
    @GetMapping("/robo/status/list")
    public ResponseEntity<BaseResponseDto> roboStatusList() {
        try {

            //当前状态 0: 未登录 1: 正常登录中 2：正常退出 3：异常退出 9：取消追踪
            // 为空时，查询所有有效机器人；查询多种状态时，以, 分隔开
            final String currentStatus = getRequestParam("currentStatus");

            //验证token，并同时获取用户信息
            UserLoginInfoDto user = getUserAndVerifyToken();

            _logger.info("userId=["+user.getUserId()+"]");

            List<RoboBasicInfoDto> list = new ArrayList<>();
            if(!CommonUtils.isBlank(currentStatus)){
                String[] arr = currentStatus.split(",");
                if(arr!=null&&arr.length>0) {
                    for (String status : arr) {
                        List<RoboBasicInfoDto> tempList = roboInfoService.getRoboBasicInfoList(user.getUserId(), status);
                        if (tempList != null && tempList.size() > 0) {
                            list.addAll(tempList);
                        }
                    }
                }
            }else{
                //说明当前状态为空，查询所有有效机器人
                list = roboInfoService.getRoboBasicInfoList(user.getUserId(),"");
            }

//            List<RoboBasicInfoDto> list = roboInfoService.getRoboBasicInfoList(user.getUserId(),currentStatus);
            List<RoboBasicInfoResponseDto> resultList = new ArrayList<RoboBasicInfoResponseDto>();
            if(list!=null&&list.size()>0){
                for(RoboBasicInfoDto dto:list){
                    RoboBasicInfoResponseDto roboBasicInfoResponseDto = new RoboBasicInfoResponseDto();
                    roboBasicInfoResponseDto.setRoboId(dto.getRoboId());
                    roboBasicInfoResponseDto.setNickName(dto.getNickName());
                    roboBasicInfoResponseDto.setUin(dto.getUin());
                    roboBasicInfoResponseDto.setCurrentStatus(dto.getCurrentStatus());
                    resultList.add(roboBasicInfoResponseDto);
                }
            }
            Map<String,Object> resultMap = new HashMap<String, Object>();
            resultMap.put("roboList", resultList);

            return succResponse(CommConstants.OPERATOR_SUCC,resultMap);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }

}
