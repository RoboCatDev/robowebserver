package com.robo.server.web.model.statistics;

import com.common.base.BaseRequestModel;
import com.common.base.annotation.CanNullAnnotation;

/**
 * Created by jianghaoming on 17/8/4.
 *
 * command	是	string	具体命令
 userName	是	string	使用对象
 groupId	是	string	使用的群组
 */
public class StatisticsCommandRequestModel extends BaseRequestModel {

    private String command;

    private String userName;

    private String groupId;

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }
}
