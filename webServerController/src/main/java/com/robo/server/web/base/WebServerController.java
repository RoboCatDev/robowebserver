package com.robo.server.web.base;


import com.common.base.BaseController;
import com.common.base.BaseRequestModel;
import com.common.base.CommConstants;
import com.common.base.exception.BusinessException;
import com.common.base.model.WebSocketMsgModel;
import com.common.base.response.BaseResponseDto;
import com.common.redis.RedisManager;
import com.common.utils.CommonUtils;
import com.common.utils.GsonUtils;
import com.common.utils.HttpClientUtils;
import com.robo.server.web.em.cache.CacheUserEnum;
import com.robo.server.web.entity.user.UserLoginInfoDto;
import org.apache.http.entity.StringEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;


/**
 * Created by jianghaoming on 17/2/26.
 */
public class WebServerController extends BaseController{


    @Value("${websocket.api.url}")
    private String websocketApiUrl;

    private static final Logger _baseControllerLogger = LoggerFactory.getLogger(WebServerController.class);

    protected Pageable setPage(final int pageNumber, final int pageSize) {
        return setPage(pageNumber,pageSize,null);
    }

    protected Pageable setPage(int pageNumber, final int pageSize, Sort sort) {

        if(pageNumber>0) {
            pageNumber = pageNumber - 1;
        }

        if(sort == null){
            return  new PageRequest(pageNumber,pageSize);
        }
        return new PageRequest(pageNumber,pageSize,sort);
    }

    protected boolean isBlank(final String param){
        return CommonUtils.isBlank(param);
    }

    /**
     * 验证token，并同时获取用户信息
     * @param userId
     * @param token
     * @return
     * @throws BusinessException
     */
    protected UserLoginInfoDto getUserAndVerifyToken(final String userId, final String token) throws BusinessException{
        _baseControllerLogger.info("userId=["+userId+"] token=["+token+"]");
        checkParamNull(token, CommConstants.LOGIN_OUT_MESSAGE);
        UserLoginInfoDto wechat = redis.getObject(token);
        if(null == wechat){
            throw new BusinessException(HttpStatus.UNAUTHORIZED.value(),CommConstants.LOGIN_OUT_MESSAGE);
        }
        if (!wechat.getUserId().equals(userId)) {
            throw new BusinessException(HttpStatus.UNAUTHORIZED.value(),CommConstants.LOGIN_FAIL);
        }
        return wechat;
    }

    /**
     * 从url中获取token
     * @return
     * @throws BusinessException
     */
    protected UserLoginInfoDto getUserAndVerifyToken() throws BusinessException {
        BaseRequestModel  requestModel = getBaseRequestModel();
        return getUserAndVerifyToken(requestModel.getUserId(),requestModel.getToken());
    }

    protected BaseRequestModel getBaseRequestModel(){
        String token = getRequestParam("token");
        String userId = getRequestParam("userId");
        BaseRequestModel requestModel = new BaseRequestModel();
        requestModel.setToken(token);
        requestModel.setUserId(userId);
        return requestModel;
    }


    /**
     * 从body 中获取token
     * @param paramMap
     * @return
     * @throws BusinessException
     */
    protected UserLoginInfoDto getUserAndVerifyToken(Map<String,Object> paramMap) throws BusinessException {

        BaseRequestModel  requestModel = getBaseRequestModel(paramMap);

        return getUserAndVerifyToken(requestModel.getUserId(),requestModel.getToken());
    }

    protected BaseRequestModel getBaseRequestModel(Map<String,Object> paramMap){
        String token = getMapParam("token",paramMap);
        String userId = getMapParam("userId",paramMap);
        BaseRequestModel requestModel = new BaseRequestModel();
        requestModel.setToken(token);
        requestModel.setUserId(userId);
        return requestModel;
    }

    /**
     * 从body 中获取token
     * @return
     * @throws BusinessException
     */
    protected UserLoginInfoDto getUserAndVerifyToken(BaseRequestModel requestModel) throws BusinessException {
        String token = requestModel.getToken();
        String userId = requestModel.getUserId();
        return getUserAndVerifyToken(userId,token);
    }

    /**
     * 删除验证码
     * @param mobile
     */
    protected static void delCacheVerCode(final String mobile){
        RedisManager.getRedis().delKey(CacheUserEnum.VERCODE.getValue()+mobile);
    }


    /**
     * 向websocket发送请求
     * @param websocketMsg
     */
    protected BaseResponseDto sendWebsocketApi(WebSocketMsgModel websocketMsg) throws BusinessException {
        try {
            _baseControllerLogger.info("websocketMsg == > "+websocketMsg.toLogger());
            StringEntity stringEntity = new StringEntity(GsonUtils.toJson(websocketMsg), APPLICATION_JSON_UTF8_VALUE, "utf-8");
            stringEntity.setContentType(APPLICATION_JSON_UTF8_VALUE);

            String response = HttpClientUtils.getMethodPostResponse(this.websocketApiUrl, stringEntity);
            BaseResponseDto responseDto = GsonUtils.convertObj(response, BaseResponseDto.class);

            if(!responseDto.getSuccess()){
                throw new BusinessException("websocket fail :"+responseDto.getMessage());
            }

            return responseDto;
        }catch (Exception e){
            _baseControllerLogger.error(e.getMessage(),e);
            throw new BusinessException("webscoket发送异常,");
        }
    }


}
