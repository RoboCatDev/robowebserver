package com.robo.server.web.controller.statistics;

import com.common.base.CommConstants;
import com.common.base.exception.BusinessException;
import com.common.base.response.BaseResponseDto;
import com.robo.server.web.base.WebServerController;
import com.robo.server.web.entity.statistics.StatisticsCommandDto;
import com.robo.server.web.model.statistics.StatisticsCommandRequestModel;
import com.robo.server.web.statistics.StatisticsCommandService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 数据统计
 */
@RestController
@RequestMapping("/v1/statistics")
public class StatisticsCommandController extends WebServerController {

    private static final Logger _logger = LoggerFactory.getLogger(StatisticsCommandController.class);

    @Autowired
    private StatisticsCommandService statisticsCommandService;

    @PostMapping("/command")
    public ResponseEntity<BaseResponseDto> command(@RequestBody StatisticsCommandRequestModel requestModel) {
        try {
            requestModel.checkFieldNull();
            _logger.info("request model ==> "+requestModel.toLogger());

            StatisticsCommandDto dto = new StatisticsCommandDto();
            dto.setCommand(requestModel.getCommand());
            dto.setGroupId(requestModel.getGroupId());
            dto.setUserId(requestModel.getUserId());
            dto.setUserName(requestModel.getUserName());
            statisticsCommandService.save(dto);

            return succResponse(CommConstants.OPERATOR_SUCC);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }

}

