package com.robo.server.web.controller.common;

import com.common.base.CommConstants;
import com.common.base.exception.BusinessException;
import com.common.base.response.BaseResponseDto;
import com.robo.server.web.base.WebServerController;
import com.robo.server.web.em.cache.CacheWebScoketEnum;
import com.robo.server.web.entity.user.UserLoginInfoDto;
import com.robo.server.web.model.qrcode.QrCodeRequestModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 文件上传
 */
@RestController
@RequestMapping("/v1/wechat")
public class WechatController extends WebServerController {

    private static final Logger _logger = LoggerFactory.getLogger(WechatController.class);


    private static final String cacheWechatUUIid = CacheWebScoketEnum.wechatUUid.getValue();


    /**
     * 接收传递过来的uuid
     * @param requestModel
     * @return
     */
    @PostMapping("/uuid")
    public ResponseEntity<BaseResponseDto> qrCode(@RequestBody QrCodeRequestModel requestModel) {
        try{
            UserLoginInfoDto user = getUserAndVerifyToken(requestModel);
            requestModel.checkFieldNull();

            _logger.info("request.log ==> "+requestModel.toLogger());
            String uuid = requestModel.getUuid();
            checkParamNull(uuid,"uuid is null");
            redis.set(cacheWechatUUIid+user.getUserId(),uuid, 3);

            return succResponse(CommConstants.OPERATOR_SUCC);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }





}

