package com.robo.server.web.entity.setting;

import com.robo.server.web.entity.BaseTimeDto;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by conor on 2017/8/15.
 */
@Entity
@Table(name = "t_robo_switch_statstics")
public class RoboSwitchStatsticsDto extends BaseTimeDto{

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO, generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;
    private String userId;//用户ID
    private String uin;//微信uin
    private String switchType;//开关类型 0：切换 1：自动回复 2：机器人当前状态 4：踢人 5：欢迎 6：规定 7：数据分析 8：聊天匹配 9：聊天模糊匹配
    private String switchStatus;//开关状态 0:open 1:close
    private String switchNum;//开关切换总次数

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUin() {
        return uin;
    }

    public void setUin(String uin) {
        this.uin = uin;
    }

    public String getSwitchType() {
        return switchType;
    }

    public void setSwitchType(String switchType) {
        this.switchType = switchType;
    }

    public String getSwitchStatus() {
        return switchStatus;
    }

    public void setSwitchStatus(String switchStatus) {
        this.switchStatus = switchStatus;
    }

    public String getSwitchNum() {
        return switchNum;
    }

    public void setSwitchNum(String switchNum) {
        this.switchNum = switchNum;
    }
}
