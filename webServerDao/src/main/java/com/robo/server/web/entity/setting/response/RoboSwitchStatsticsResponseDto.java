package com.robo.server.web.entity.setting.response;

import com.common.base.BaseDto;

/**
 * Created by conor on 2017/8/15.
 */
public class RoboSwitchStatsticsResponseDto extends BaseDto {

    private String switchType;//开关类型 0：切换 1：自动回复 2：机器人当前状态 4：踢人 5：欢迎 6：规定 7：数据分析 8：聊天匹配 9：聊天模糊匹配
    private String switchStatus;//开关状态 0:open 1:close

    public String getSwitchType() {
        return switchType;
    }

    public void setSwitchType(String switchType) {
        this.switchType = switchType;
    }

    public String getSwitchStatus() {
        return switchStatus;
    }

    public void setSwitchStatus(String switchStatus) {
        this.switchStatus = switchStatus;
    }

}
