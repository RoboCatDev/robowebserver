package com.robo.server.web.em.websocket;

/**
 * websocket的相关方法
 */
public enum WebsocketMethodEnum {
    sendUUId("sendUUId"), //需要uuid
    userLogin("userLogin"), //用户登录
    updateSetting("updateSetting");//更新配置

    // 定义私有变量
    private  String  value ;

    private WebsocketMethodEnum(final String  _value) {
        this.value = _value;
    }
    public String getValue() {
        return value;
    }
    public static String getValue(WebsocketMethodEnum enums) {
        return enums.value;
    }

}
