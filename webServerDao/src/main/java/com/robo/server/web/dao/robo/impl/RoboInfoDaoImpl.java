package com.robo.server.web.dao.robo.impl;

import com.common.base.exception.BusinessException;
import com.common.base.model.MyPageResult;
import com.robo.server.web.dao.BaseDaoImpl;
import com.robo.server.web.dao.robo.RoboInfoDao;
import com.robo.server.web.entity.robo.response.RoboInfoResponseDto;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by conor on 2017/8/1.
 */
@Component
public class RoboInfoDaoImpl extends BaseDaoImpl<RoboInfoResponseDto> implements RoboInfoDao {

    /**
     * 根据用户Id，查询所有的机器人登录列表
     * @param userId 用户Id
     * @param pageable
     * @return
     * @throws BusinessException
     */
    public MyPageResult<RoboInfoResponseDto> getRoboInfoPage(final String userId, final Pageable pageable) throws DataAccessException{

        final String sql = "select basic.robo_id,basic.uin,basic.nick_name,basic.current_status from t_robo_basic_info basic "+
                "where basic.user_id=:userId and basic.current_status!='9' order by basic.update_time DESC";

        Map<String,Object> params = new HashMap<String,Object>();
        params.put("userId",userId);

        return queryListEntityByPage(sql,params,RoboInfoResponseDto.class,pageable);

    }


}
