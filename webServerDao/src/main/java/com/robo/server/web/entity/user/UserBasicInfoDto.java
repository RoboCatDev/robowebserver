package com.robo.server.web.entity.user;

import com.robo.server.web.entity.BaseTimeDto;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by conor on 2017/7/18.
 */
@Entity
@Table(name = "t_user_basic_info")
public class UserBasicInfoDto extends BaseTimeDto{

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO, generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;//ID
    private String userId;//用户Id
    private String nickName;//昵称
    private String comWechatScale;//企业微信群规模
    private String profession;//行业
    private String trusteeshipCount;//拟托管运行群数目

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getComWechatScale() {
        return comWechatScale;
    }

    public void setComWechatScale(String comWechatScale) {
        this.comWechatScale = comWechatScale;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getTrusteeshipCount() {
        return trusteeshipCount;
    }

    public void setTrusteeshipCount(String trusteeshipCount) {
        this.trusteeshipCount = trusteeshipCount;
    }

}
