package com.robo.server.web.repository.user;

import com.robo.server.web.dao.BaseDaoImpl;
import com.robo.server.web.entity.user.UserBasicInfoDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.Table;

/**
 * Created by conor on 2017/7/12.
 */
@Repository
@Table(name = "t_user_basic_info")
public interface UserBasicInfoRepository extends JpaRepository<UserBasicInfoDto,String> {

    UserBasicInfoDto findByUserId(String userId);
}
