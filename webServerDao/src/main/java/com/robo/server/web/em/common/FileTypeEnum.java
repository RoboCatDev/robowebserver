package com.robo.server.web.em.common;

import java.util.HashMap;
import java.util.Map;

/**
 * 0:配置文件
 */
public enum FileTypeEnum {
    CONFIGURE("0");

    //定义私有变量
    private  String  value ;

    //构造函数，枚举类型只能为私有
    private FileTypeEnum(final String  _value) {
        this.value = _value;
    }
    public String getValue() {
        return value;
    }
    public Integer getIntValue() {
        return Integer.parseInt(value);
    }

    private static Map<String, String> desc = new HashMap<String, String>();
    static {
        desc.put(CONFIGURE.getValue(), "/configure/");
    }

    public static Map<String,String> getAllMap(){
        Map<String,String> resultMap = new HashMap<String,String>();
        resultMap.putAll(desc);
        return resultMap;
    }

    public static String getDesc(final String key) {
        if(desc.containsKey(key)){
            return desc.get(key);
        }else{
            return "";
        }
    }

}
