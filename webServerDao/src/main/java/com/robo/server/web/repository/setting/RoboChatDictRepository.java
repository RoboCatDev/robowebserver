package com.robo.server.web.repository.setting;

import com.robo.server.web.entity.setting.RoboChatDictDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.Table;
import javax.transaction.Transactional;

/**
 * Created by conor on 2017/8/15.
 */
@Repository
@Table(name="t_robo_chat_dict")
public interface RoboChatDictRepository extends JpaRepository<RoboChatDictDto,String> {

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update RoboChatDictDto t set t.chatKey=:chatKey, t.chatValue=:chatValue where t.id=:id")
    int updateRoboChatDictById(@Param("id") String id, @Param("chatKey") String chatKey, @Param("chatValue") String chatValue);

}
