package com.robo.server.web.repository.robo.group;

import com.robo.server.web.entity.robo.group.GroupBasicInfoDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.Table;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by conor on 2017/7/12.
 */
@Repository
@Table(name = "t_group_basic_info")
public interface GroupBasicInfoRepository extends JpaRepository<GroupBasicInfoDto,String> {

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update GroupBasicInfoDto t set t.authorizeStatus=:authorizeStatus,t.authorizeTime=:authorizeTime where t.groupId=:groupId")
    int updateGroupBasicInfoStatus(@Param("groupId") String groupId,@Param("authorizeStatus") String authorizeStatus,@Param("authorizeTime") String authorizeTime);

    @Query(value = "select * from t_group_basic_info where user_id=:userId and  uin=:uin and nick_name like CONCAT('%',:keyWord,'%') -- #pageable\n ",nativeQuery = true)
    Page<GroupBasicInfoDto> findAllByKeyWordPageable(@Param("userId") String userId,@Param("uin") String uin,@Param("keyWord") String keyWord, Pageable pageable);

    @Query(value = "select * from t_group_basic_info where user_id=:userId and uin=:uin and authorize_status = :authorizeStatus and nick_name like CONCAT('%',:keyWord,'%') -- #pageable\n order by create_time desc",nativeQuery = true)
    List<GroupBasicInfoDto> findAllByKeyWordAndAuthorizeStatus(@Param("userId") String userId,@Param("uin") String uin,@Param("keyWord") String keyWord,@Param("authorizeStatus") String authorizeStatus);

    @Query(value = "select * from t_group_basic_info where user_id=:userId and uin=:uin and is_owner = :isOwner and nick_name like CONCAT('%',:keyWord,'%') -- #pageable\n order by create_time desc",nativeQuery = true)
    List<GroupBasicInfoDto> findAllByKeyWordAndOwner(@Param("userId") String userId,@Param("uin") String uin,@Param("keyWord") String keyWord,@Param("isOwner") String isOwner);

    @Query(value = "select * from t_group_basic_info where user_id=:userId and uin=:uin and is_owner = :isOwner and authorize_status = :authorizeStatus and nick_name like CONCAT('%',:keyWord,'%') -- #pageable\n order by create_time desc",nativeQuery = true)
    List<GroupBasicInfoDto> findAllByKeyWordAndOwnerAndAuthorizeStatus(@Param("userId") String userId,@Param("uin") String uin,@Param("keyWord") String keyWord,@Param("isOwner") String isOwner,@Param("authorizeStatus") String authorizeStatus);



    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("delete from GroupBasicInfoDto t where t.userId=:userId and t.uin=:uin")
    void deleteGroupBasicInfoByUin(@Param("userId") String userId,@Param("uin") String uin);

    @Query(value = "select * from t_group_basic_info where user_id=:userId and uin=:uin and nick_name like CONCAT('%',:keyWord,'%') ",nativeQuery = true)
    List<GroupBasicInfoDto> findAllByKeyWord(@Param("userId") String userId,@Param("uin") String uin,@Param("keyWord") String keyWord);

}
