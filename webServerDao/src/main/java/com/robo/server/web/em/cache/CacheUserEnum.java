package com.robo.server.web.em.cache;


/**
 * Created by jianghaoming on 17/3/7.
 */
public enum CacheUserEnum {

    //用户相关的缓存key

    SIGNIN("user_sign_"),

    VERCODE("user_vercode_");


    private  String  value ;

    private CacheUserEnum(final String  _value) {
        this.value = _value;
    }

    public String getValue() {
        return value;
    }
}
