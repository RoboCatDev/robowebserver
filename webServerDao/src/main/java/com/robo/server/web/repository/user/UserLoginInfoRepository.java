package com.robo.server.web.repository.user;

import com.robo.server.web.entity.user.UserLoginInfoDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.Table;
import javax.transaction.Transactional;

/**
 * Created by conor on 2017/7/12.
 */
@Repository
@Table(name = "t_user_login_info")
public interface UserLoginInfoRepository extends JpaRepository<UserLoginInfoDto,String> {

    UserLoginInfoDto findOneByMobile(String mobile);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update UserLoginInfoDto u set u.password=:password where userId =:userId ")
    int updatePasswordByUserId(@Param("userId") String userId,@Param("password") String password);

}
