package com.robo.server.web.entity.robo.response;

import com.common.base.BaseDto;

/**
 * Created by conor on 2017/8/21.
 */
public class RoboBasicInfoResponseDto extends BaseDto {

    private String uin;//微信uin
    private String roboId;//机器人ID
    private String nickName;//昵称
    private String currentStatus;//当前状态 0: 未登录 1: 正常登录中 2：正常退出 3：异常退出 9:取消追踪

    public String getUin() {
        return uin;
    }

    public void setUin(String uin) {
        this.uin = uin;
    }

    public String getRoboId() {
        return roboId;
    }

    public void setRoboId(String roboId) {
        this.roboId = roboId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }
}
