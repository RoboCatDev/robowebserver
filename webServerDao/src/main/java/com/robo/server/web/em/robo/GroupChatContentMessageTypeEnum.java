package com.robo.server.web.em.robo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 返回前端的消息类型：
 *  0：文本（1） 1：表情（47） 2：文章（49） 3：图片（3） 4：红包（10000 & 内容为：收到红包，请在手机上查看） 5：视频（43） 6：语音（34）  7：其他
 *
 * 微信返回的消息类型：
 * 1文本消息 3图片消息 34语音消息 37好友确认消息 40POSSIBLEFRIEND_MSG 42共享名片 43视频消息
 * 47动画表情 48位置消息 49分享链接 50VOIPMSG 51微信初始化消息 52VOIPNOTIFY 53VOIPINVITE
 * 62小视频 9999SYSNOTICE 10000系统消息 10002	撤回消息
 */
public enum GroupChatContentMessageTypeEnum {
    text("0"),
    emoj("1"),
    article("2"),
    pic("3"),
    reward("4"),
    video("5"),
    voice("6"),
    other("7");

    //定义私有变量
    private  String  value ;

    private static Map<String, String> desc = new HashMap<String, String>();
    static {
        desc.put(text.getValue(),"文本");
        desc.put(emoj.getValue(),"表情");
        desc.put(article.getValue(),"文章");
        desc.put(pic.getValue(),"图片");
        desc.put(reward.getValue(),"红包");
        desc.put(video.getValue(),"视频");
        desc.put(voice.getValue(),"语音");
        desc.put(other.getValue(),"其他");
    }

    //构造函数，枚举类型只能为私有
    private GroupChatContentMessageTypeEnum(final String  _value) {
        this.value = _value;
    }
    public String getValue() {
        return value;
    }
    public Integer getIntValue() {
        return Integer.parseInt(value);
    }

    public static String getValue(GroupChatContentMessageTypeEnum enums) {
        return enums.value;
    }

    public static List<GroupChatContentMessageTypeEnum> getAllList() {
        GroupChatContentMessageTypeEnum[] types = GroupChatContentMessageTypeEnum.values();
        List<GroupChatContentMessageTypeEnum> result = new ArrayList<GroupChatContentMessageTypeEnum>();
        for (int i = 0; i < types.length; i++) {
            result.add(types[i]);
        }
        return result;
    }

    public static Map<String,String> getAllMap(){
        Map<String,String> resultMap = new HashMap<String,String>();
        resultMap.putAll(desc);
        return resultMap;
    }

    public static String getDesc(final String key) {
        if(desc.containsKey(key)){
            return desc.get(key);
        }else{
            return "";
        }
    }


    /**
     * 1文本消息 3图片消息 34语音消息 37好友确认消息 40POSSIBLEFRIEND_MSG 42共享名片 43视频消息
     * 47动画表情 48位置消息 49分享链接 50VOIPMSG 51微信初始化消息 52VOIPNOTIFY 53VOIPINVITE
     * 62小视频 9999SYSNOTICE 10000系统消息 10002	撤回消息
     */
    public enum wechatChatContentMessageTypeEnum{
        text("1"),
        pic("3"),
        voice("34"),
        friend("37"),
        POSSIBLEFRIEND_MSG("40"),
        shareCard("42"),
        video("43"),
        emoj("47"),
        address("48"),
        linkUrl("49"),
        VOIPMSG("50"),
        wechatInit("51"),
        VOIPNOTIFY("52"),
        VOIPINVITE("53"),
        smallVideo("62"),
        SYSNOTICE("9999"),
        systemMsg("10000"),
        withDrawMsg("10002");

        //定义私有变量
        private  String  value ;

        //构造函数，枚举类型只能为私有
        private wechatChatContentMessageTypeEnum(final String _value) {
            this.value = _value;
        }
        public String getValue() {
            return value;
        }
        public Integer getIntValue() {
            return Integer.parseInt(value);
        }
    }

}
