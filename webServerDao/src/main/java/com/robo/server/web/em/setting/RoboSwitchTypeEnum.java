package com.robo.server.web.em.setting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 开关类型 0：切换 1：自动回复 2：机器人当前状态 4：踢人 5：欢迎 6：规定 7：数据分析 8：聊天匹配 9：聊天模糊匹配
 */
public enum RoboSwitchTypeEnum {
    qieHuan("0"),
    autoReply("1"),
    roboStatus("2"),
//    tuLing("3"),
    tiRen("4"),
    welcome("5"),
    guiDing("6"),
    analyse("7"),
    piPei("8"),
    moHuPiPei("9");

    //定义私有变量
    private  String  value ;

    private static Map<String, String> desc = new HashMap<String, String>();
    static {
        desc.put(qieHuan.getValue(),"切换");
        desc.put(autoReply.getValue(),"自动回复");
        desc.put(roboStatus.getValue(),"机器人当前状态");
//        desc.put(tuLing.getValue(),"图灵机器人");
        desc.put(tiRen.getValue(),"踢人");
        desc.put(welcome.getValue(),"欢迎");
        desc.put(guiDing.getValue(),"规定");
        desc.put(analyse.getValue(),"数据分析");
        desc.put(piPei.getValue(),"聊天匹配");
        desc.put(moHuPiPei.getValue(),"聊天模糊匹配");
    }

    //构造函数，枚举类型只能为私有
    private RoboSwitchTypeEnum(final String  _value) {
        this.value = _value;
    }
    public String getValue() {
        return value;
    }
    public Integer getIntValue() {
        return Integer.parseInt(value);
    }

    public static String getValue(RoboSwitchTypeEnum enums) {
        return enums.value;
    }

    public static List<RoboSwitchTypeEnum> getAllList() {
        RoboSwitchTypeEnum[] types = RoboSwitchTypeEnum.values();
        List<RoboSwitchTypeEnum> result = new ArrayList<RoboSwitchTypeEnum>();
        for (int i = 0; i < types.length; i++) {
            result.add(types[i]);
        }
        return result;
    }

    public static Map<String,String> getAllMap(){
        Map<String,String> resultMap = new HashMap<String,String>();
        resultMap.putAll(desc);
        return resultMap;
    }

    public static String getDesc(final String key) {
        if(desc.containsKey(key)){
            return desc.get(key);
        }else{
            return "";
        }
    }

}
