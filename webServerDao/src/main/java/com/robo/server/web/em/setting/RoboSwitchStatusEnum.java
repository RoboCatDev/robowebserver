package com.robo.server.web.em.setting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 开关状态 0:open 1:close
 */
public enum RoboSwitchStatusEnum {
    open("0"),
    close("1");

    //定义私有变量
    private  String  value ;

    private static Map<String, String> desc = new HashMap<String, String>();
    static {
        desc.put(open.getValue(),"开启");
        desc.put(close.getValue(),"关闭");
    }

    //构造函数，枚举类型只能为私有
    private RoboSwitchStatusEnum(final String  _value) {
        this.value = _value;
    }
    public String getValue() {
        return value;
    }
    public Integer getIntValue() {
        return Integer.parseInt(value);
    }

    public static String getValue(RoboSwitchStatusEnum enums) {
        return enums.value;
    }

    public static List<RoboSwitchStatusEnum> getAllList() {
        RoboSwitchStatusEnum[] types = RoboSwitchStatusEnum.values();
        List<RoboSwitchStatusEnum> result = new ArrayList<RoboSwitchStatusEnum>();
        for (int i = 0; i < types.length; i++) {
            result.add(types[i]);
        }
        return result;
    }

    public static Map<String,String> getAllMap(){
        Map<String,String> resultMap = new HashMap<String,String>();
        resultMap.putAll(desc);
        return resultMap;
    }

    public static String getDesc(final String key) {
        if(desc.containsKey(key)){
            return desc.get(key);
        }else{
            return "";
        }
    }
}
