package com.robo.server.web.em.robo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  jiaru_yaoqing = 10
 *  jiaru_saomiao = 11
 *  tuichu_tuiqun = 20
 *  tuichu_tiren = 21
 */
public enum GroupPersonChangeTypeEnum {
    ADD_YAOQING("10"),
    ADD_SAOMIAO("11"),
    DEL_TUIQUN("20"),
    DEL_TIREN("21");

    //定义私有变量
    private  String  value ;

    private static Map<String, String> desc = new HashMap<String, String>();
    static {
        desc.put(ADD_YAOQING.getValue(),"邀请加入");
        desc.put(ADD_SAOMIAO.getValue(),"扫描二维码加入");
        desc.put(DEL_TUIQUN.getValue(),"主动退群");
        desc.put(DEL_TIREN.getValue(),"群主踢人");
    }

    //构造函数，枚举类型只能为私有
    private GroupPersonChangeTypeEnum(final String  _value) {
        this.value = _value;
    }
    public String getValue() {
        return value;
    }
    public Integer getIntValue() {
        return Integer.parseInt(value);
    }

    public static String getValue(GroupPersonChangeTypeEnum enums) {
        return enums.value;
    }

    public static List<GroupPersonChangeTypeEnum> getAllList() {
        GroupPersonChangeTypeEnum[] types = GroupPersonChangeTypeEnum.values();
        List<GroupPersonChangeTypeEnum> result = new ArrayList<GroupPersonChangeTypeEnum>();
        for (int i = 0; i < types.length; i++) {
            result.add(types[i]);
        }
        return result;
    }

    public static Map<String,String> getAllMap(){
        Map<String,String> resultMap = new HashMap<String,String>();
        resultMap.putAll(desc);
        return resultMap;
    }

    public static String getDesc(final String key) {
        if(desc.containsKey(key)){
            return desc.get(key);
        }else{
            return "";
        }
    }
}
