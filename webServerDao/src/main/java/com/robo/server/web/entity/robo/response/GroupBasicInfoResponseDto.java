package com.robo.server.web.entity.robo.response;

import com.common.base.BaseDto;

/**
 * Created by conor on 2017/8/24.
 */
public class GroupBasicInfoResponseDto extends BaseDto {

    private String groupId;//群组ID
    private String uin;//微信uin
    private String nickName;//群组名称
    private String authorizeStatus;//授权状态 0：未授权 1：已授权

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getUin() {
        return uin;
    }

    public void setUin(String uin) {
        this.uin = uin;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAuthorizeStatus() {
        return authorizeStatus;
    }

    public void setAuthorizeStatus(String authorizeStatus) {
        this.authorizeStatus = authorizeStatus;
    }

}
