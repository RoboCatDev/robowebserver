package com.robo.server.task.entity.statistics;

import com.robo.server.task.entity.BaseTimeDto;

import javax.persistence.*;

/**
 * 群组成员统计
 * Created by conor on 2017/8/29.
 */
@Entity
@Table(name = "t_statistics_person_info")
public class StatisticsPersonInfoDto extends BaseTimeDto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String personId;//群组成员Id
    private String groupId;//群组Id
    private Integer activeDaysTotal;//当前群组成员总活跃天数
    private Integer activeDaysContinue;//当前群组成员连续活跃天数
    private Integer messageTotal;//当日消息总数量
    private String currentDay;//当前日期

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public Integer getActiveDaysTotal() {
        return activeDaysTotal;
    }

    public void setActiveDaysTotal(Integer activeDaysTotal) {
        this.activeDaysTotal = activeDaysTotal;
    }

    public Integer getActiveDaysContinue() {
        return activeDaysContinue;
    }

    public void setActiveDaysContinue(Integer activeDaysContinue) {
        this.activeDaysContinue = activeDaysContinue;
    }

    public Integer getMessageTotal() {
        return messageTotal;
    }

    public void setMessageTotal(Integer messageTotal) {
        this.messageTotal = messageTotal;
    }

    public String getCurrentDay() {
        return currentDay;
    }

    public void setCurrentDay(String currentDay) {
        this.currentDay = currentDay;
    }
}



