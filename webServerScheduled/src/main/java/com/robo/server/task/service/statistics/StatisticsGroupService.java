package com.robo.server.task.service.statistics;

import com.common.base.exception.BusinessException;
import com.robo.server.task.entity.statistics.StatisticsGroupInfoDto;
import com.robo.server.task.entity.statistics.StatisticsGroupMessageDto;
import com.robo.server.task.entity.statistics.response.StatisticsBaseAnalyseResponseDto;
import com.robo.server.task.entity.statistics.response.StatisticsMessageAnalyseResponseDto;
import com.robo.server.task.entity.statistics.response.StatisticsPersonAnalyseResponseDto;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by conor on 2017/8/29.
 */
@Service
public interface StatisticsGroupService {

    /**
     * 保存群组统计
     * @param statisticsGroupInfoDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public StatisticsGroupInfoDto saveStaticsGroupInfo(StatisticsGroupInfoDto statisticsGroupInfoDto) throws BusinessException,DataAccessException,Exception;

    /**
     * 保存群组统计(批量)
     * @param statisticsGroupInfoDtoList
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public List<StatisticsGroupInfoDto> saveStaticsGroupInfo(List<StatisticsGroupInfoDto> statisticsGroupInfoDtoList) throws BusinessException,DataAccessException,Exception;

    /**
     * 保存群组消息的统计
     * @param statisticsGroupMessageDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public StatisticsGroupMessageDto saveStatisticsGroupMessage(StatisticsGroupMessageDto statisticsGroupMessageDto) throws BusinessException,DataAccessException,Exception;

    /**
     * 保存群组消息的统计（批量）
     * @param statisticsGroupMessageDtoList
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public List<StatisticsGroupMessageDto> saveStatisticsGroupMessage(List<StatisticsGroupMessageDto> statisticsGroupMessageDtoList) throws BusinessException,DataAccessException,Exception;

    /**
     * 查询群组基本统计
     * @param groupId 群组Id
     * @param startTime 起始时间
     * @param endTime 截止时间
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public StatisticsBaseAnalyseResponseDto getStatisticsBaseAnalyseInfo(final String groupId, final String startTime, final String endTime) throws BusinessException,DataAccessException,Exception;

    /**
     * 查询群组基本信息
     * @param groupId 群组Id
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public StatisticsBaseAnalyseResponseDto getStatisticsGroupBasicInfo(final String groupId) throws BusinessException,DataAccessException,Exception;

    /**
     * 查询群组成员统计
     * @param groupId 群组Id
     * @param startTime 起始时间
     * @param endTime 截止时间
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public StatisticsPersonAnalyseResponseDto getStatisticsPersonAnalyseInfo(String groupId, String startTime, String endTime) throws BusinessException,DataAccessException,Exception;

    /**
     * 查询群组消息统计
     * @param groupId 群组Id
     * @param startTime 起始时间
     * @param endTime 截止时间
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public StatisticsMessageAnalyseResponseDto getStatisticsMessageAnalyseInfo(String groupId, String startTime, String endTime) throws BusinessException,DataAccessException,Exception;

    /**
     * 删除当前日期的群组统计数据
     * @param currentDay 当前日期
     * @throws DataAccessException
     * @throws BusinessException
     * @throws Exception
     */
    public void deleteStatisticsGroupInfo(String currentDay) throws DataAccessException,BusinessException,Exception;

    /**
     * 删除当前日期的群组消息统计数据
     * @param currentDay 当前日期
     * @throws DataAccessException
     * @throws BusinessException
     * @throws Exception
     */
    public void deleteStatisticsGroupMessage(String currentDay) throws DataAccessException,BusinessException,Exception;

}
