package com.robo.server.task.service.statistics.impl;

import com.common.base.exception.BusinessException;
import com.common.utils.CheckUtils;
import com.common.utils.CommonUtils;
import com.common.utils.DateFormatUtil;
import com.robo.server.task.dao.statistics.StatisticsAnalyseDao;
import com.robo.server.task.em.PersonActiveStatusEnum;
import com.robo.server.task.entity.statistics.StatisticsGroupInfoDto;
import com.robo.server.task.entity.statistics.StatisticsGroupMessageDto;
import com.robo.server.task.entity.statistics.StatisticsPersonInfoDto;
import com.robo.server.task.entity.statistics.response.StatisticsBaseAnalyseResponseDto;
import com.robo.server.task.entity.statistics.response.StatisticsMessageAnalyseResponseDto;
import com.robo.server.task.entity.statistics.response.StatisticsPersonAnalyseResponseDto;
import com.robo.server.task.repository.StatisticsGroupInfoRepository;
import com.robo.server.task.repository.StatisticsGroupMessageRepository;
import com.robo.server.task.service.statistics.StatisticsGroupService;
import com.robo.server.task.service.statistics.StatisticsPersonService;
import com.robo.server.web.em.robo.GroupChatContentMessageTypeEnum;
import com.robo.server.web.entity.robo.group.GroupBasicInfoDto;
import com.robo.server.web.message.RoboMessage;
import com.robo.server.web.repository.robo.group.GroupBasicInfoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by conor on 2017/8/29.
 */
@Service
public class StatisticsGroupServiceImpl implements StatisticsGroupService {

    private static final Logger _logger = LoggerFactory.getLogger(StatisticsGroupServiceImpl.class);

    @Autowired
    private StatisticsGroupInfoRepository statisticsGroupInfoRepository;

    @Autowired
    private StatisticsGroupMessageRepository statisticsGroupMessageRepository;

    @Autowired
    private StatisticsAnalyseDao statisticsAnalyseDao;

    @Autowired
    private StatisticsPersonService statisticsPersonService;

    @Autowired
    private GroupBasicInfoRepository groupBasicInfoRepository;

    /**
     * 保存群组统计
     * @param statisticsGroupInfoDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public StatisticsGroupInfoDto saveStaticsGroupInfo(StatisticsGroupInfoDto statisticsGroupInfoDto) throws BusinessException,DataAccessException,Exception{

        _logger.info("保存群组统计 statisticsPersonDto===>"+ statisticsGroupInfoDto.toLogger());

        return statisticsGroupInfoRepository.save(statisticsGroupInfoDto);
    }

    /**
     * 保存群组统计(批量)
     * @param statisticsGroupInfoDtoList
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public List<StatisticsGroupInfoDto> saveStaticsGroupInfo(List<StatisticsGroupInfoDto> statisticsGroupInfoDtoList) throws BusinessException,DataAccessException,Exception{

        _logger.info("保存群组统计（批量） statisticsGroupDtoList.size===>"+ statisticsGroupInfoDtoList.size());

        return statisticsGroupInfoRepository.save(statisticsGroupInfoDtoList);
    }

    /**
     * 保存群组消息的统计
     * @param statisticsGroupMessageDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public StatisticsGroupMessageDto saveStatisticsGroupMessage(StatisticsGroupMessageDto statisticsGroupMessageDto) throws BusinessException,DataAccessException,Exception{

        _logger.info("保存群组消息的统计 statisticsMessageDto===>"+ statisticsGroupMessageDto.toLogger());

        return statisticsGroupMessageRepository.save(statisticsGroupMessageDto);
    }

    /**
     * 保存群组消息的统计（批量）
     * @param statisticsGroupMessageDtoList
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public List<StatisticsGroupMessageDto> saveStatisticsGroupMessage(List<StatisticsGroupMessageDto> statisticsGroupMessageDtoList) throws BusinessException,DataAccessException,Exception{

        _logger.info("保存群组消息的统计（批量） statisticsMessageDtoList.size===>"+ statisticsGroupMessageDtoList.size());

        return statisticsGroupMessageRepository.save(statisticsGroupMessageDtoList);
    }

    /**
     * 查询群组基本统计
     * @param groupId 群组Id
     * @param startTime 起始时间
     * @param endTime 截止时间
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public StatisticsBaseAnalyseResponseDto getStatisticsBaseAnalyseInfo(final String groupId, final String startTime, final String endTime) throws BusinessException,DataAccessException,Exception{

        _logger.info("查询群组基本统计 groupId=["+groupId+"] startTime=["+startTime+"] endTime=["+endTime+"]");
        CheckUtils.checkParamNull(groupId, RoboMessage.GROUP_ID);
        CheckUtils.checkParamNull(startTime,RoboMessage.start_time);
        CheckUtils.checkParamNull(endTime,RoboMessage.end_time);

        StatisticsBaseAnalyseResponseDto resultDto = new StatisticsBaseAnalyseResponseDto();

        int daysActiveTotal = 0;//群组总的活跃天数
        //根据群组id、起始时间、截止时间查询
        List<StatisticsGroupInfoDto> groupInfoDtoList = statisticsGroupInfoRepository.findAllByGroupIdAndCurrentDate(groupId,startTime,endTime);
        if(groupInfoDtoList!=null&&groupInfoDtoList.size()>0){
            //初始群组总人数
            resultDto.setPersonInitTotal(groupInfoDtoList.get(groupInfoDtoList.size()-1).getPersonTotal());
            daysActiveTotal = groupInfoDtoList.get(groupInfoDtoList.size()-1).getActiveDaysTotal();
            //当前群组总人数
            resultDto.setPersonCurTotal(groupInfoDtoList.get(0).getPersonTotal());
            daysActiveTotal = groupInfoDtoList.get(0).getActiveDaysTotal() - daysActiveTotal;
        }
        resultDto.setDaysActiveTotal(daysActiveTotal);//群组总的活跃天数

        //根据群组id，起始时间、截止时间，查询此段时间内群组人数信息、聊天信息等
        StatisticsBaseAnalyseResponseDto tempDto = statisticsAnalyseDao.getStatisticsBaseAnalyseInfo(groupId,startTime,endTime);
        if(tempDto!=null){
            resultDto.setPersonJoinCount(tempDto.getPersonJoinCount());//群组新增人数
            resultDto.setPersonLeaveCount(tempDto.getPersonLeaveCount());//群组离开人数
            resultDto.setChatContentTotal(tempDto.getChatContentTotal());//群组总的消息数

        }

        StatisticsBaseAnalyseResponseDto group = getStatisticsGroupBasicInfo(groupId);

        resultDto.setGroupId(group.getGroupId());
        resultDto.setGroupName(group.getGroupName());
        resultDto.setAnalyStartTime(group.getAnalyStartTime());
        resultDto.setAnalyEndTime(group.getAnalyEndTime());

        resultDto.NullReplaceBlank();
        return resultDto;

    }

    /**
     * 查询群组基本信息
     * @param groupId 群组Id
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public StatisticsBaseAnalyseResponseDto getStatisticsGroupBasicInfo(final String groupId) throws BusinessException,DataAccessException,Exception{

        _logger.info("查询群组基本信息 groupId=["+groupId+"]");
        CheckUtils.checkParamNull(groupId, RoboMessage.GROUP_ID);

        StatisticsBaseAnalyseResponseDto resultDto = new StatisticsBaseAnalyseResponseDto();

        resultDto.setGroupId(groupId);
        GroupBasicInfoDto groupBasicInfoDto = new GroupBasicInfoDto();
        groupBasicInfoDto.setGroupId(groupId);
        groupBasicInfoDto = groupBasicInfoRepository.findOne(Example.of(groupBasicInfoDto));
        String analyStartTime = "";
        String groupName = "";
        if(groupBasicInfoDto!=null){
            if(!CommonUtils.isBlank(groupBasicInfoDto.getAuthorizeTime())){
                analyStartTime = DateFormatUtil.dateToString(DateFormatUtil.string2date(groupBasicInfoDto.getAuthorizeTime(),DateFormatUtil.YYYY_MM_DD),DateFormatUtil.YYYY_MM_DD);
            }else{
                analyStartTime = DateFormatUtil.timestamp2string(groupBasicInfoDto.getCreateTime(),DateFormatUtil.YYYY_MM_DD);
            }
            groupName = groupBasicInfoDto.getNickName();
        }
        resultDto.setGroupName(groupName);
        resultDto.setAnalyStartTime(analyStartTime);
        final String yesterday = DateFormatUtil.dateToString(DateFormatUtil.addDays(DateFormatUtil.getNow(),-1),DateFormatUtil.YYYY_MM_DD);
        resultDto.setAnalyEndTime(yesterday);

        resultDto.NullReplaceBlank();
        return resultDto;

    }

    /**
     * 查询群组成员统计
     * @param groupId 群组Id
     * @param startTime 起始时间
     * @param endTime 截止时间
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public StatisticsPersonAnalyseResponseDto getStatisticsPersonAnalyseInfo(String groupId, String startTime, String endTime) throws BusinessException,DataAccessException,Exception{

        _logger.info("查询群组成员统计 groupId=["+groupId+"] startTime=["+startTime+"] endTime=["+endTime+"]");
        CheckUtils.checkParamNull(groupId, RoboMessage.GROUP_ID);
        CheckUtils.checkParamNull(startTime,RoboMessage.start_time);
        CheckUtils.checkParamNull(endTime,RoboMessage.end_time);

        StatisticsPersonAnalyseResponseDto resultDto = new StatisticsPersonAnalyseResponseDto();

        int personCurTotal = 0;//当前群组总用户数
        int personActiveCount = 0;//活跃用户数
        int personDivingCount = 0;//潜水用户数
        int personOtherCount = 0;//其他用户数（当前总用户数-活跃用户数-潜水用户数）

        //根据群组id、起始时间、截止时间查询群组基本统计信息
        List<StatisticsGroupInfoDto> groupInfoDtoList = statisticsGroupInfoRepository.findAllByGroupIdAndCurrentDate(groupId,startTime,endTime);
        if(groupInfoDtoList!=null&&groupInfoDtoList.size()>0){
            //当前群组总人数
            personCurTotal = groupInfoDtoList.get(0).getPersonTotal();
        }
        resultDto.setPersonCurTotal(personCurTotal);//当前群组总用户数

        Map<String,Integer> personMap = new HashMap<String,Integer>();
        //根据群组id，起始时间、截止时间查询群组成员基本统计信息
        List<StatisticsPersonInfoDto> personInfoDtoList = statisticsPersonService.getStatisticsPersonInfoList(groupId,startTime,endTime);
        if(personInfoDtoList!=null&&personInfoDtoList.size()>0){
            for(StatisticsPersonInfoDto dto:personInfoDtoList){
                if(dto!=null&&!CommonUtils.isBlank(dto.getPersonId())){
                    if(personMap.containsKey(dto.getPersonId())){
                        int messageCount = personMap.get(dto.getPersonId())+dto.getMessageTotal();
                        personMap.put(dto.getPersonId(),messageCount);
                    }else{
                        int messageCount = dto.getMessageTotal();
                        personMap.put(dto.getPersonId(),messageCount);
                    }
                }
            }
            for (Map.Entry<String, Integer> entry : personMap.entrySet()) {
                final String personId = entry.getKey();
                final int messageTotal = entry.getValue();
                //获取起始时间与截止时间之间的天数
                int days = DateFormatUtil.getBetweenTwoDays(startTime,endTime);
                if(messageTotal>=PersonActiveStatusEnum.diving_bottom.getIntValue()*days &&
                        messageTotal<=PersonActiveStatusEnum.diving_top.getIntValue()*days){
                    personDivingCount++;//潜水
                }else if(messageTotal>=PersonActiveStatusEnum.active_bottom.getIntValue()*days){
                    personActiveCount++;//活跃
                }
                _logger.info("统计活跃/潜水用户数 personId=["+personId+"] messageTotal=["+messageTotal+"] personActiveCount=["+personActiveCount+"] personDivingCount=["+personDivingCount+"]");
            }
        }

        resultDto.setPersonActiveCount(personActiveCount);//活跃用户数
        resultDto.setPersonDivingCount(personDivingCount);//潜水用户数

        personOtherCount = personCurTotal - personActiveCount - personDivingCount;
        resultDto.setPersonOtherCount(personOtherCount);//其他用户数（当前总用户数-活跃用户数-潜水用户数）

        String personActiveRate = "0";
        String personDivingRate = "0";
        String personOtherRate = "0";
        if(personCurTotal!=0){
            personActiveRate = String.format("%.2f", ((float)personActiveCount/(double)personCurTotal)*100);//活跃用户数占比（百分比）
            personDivingRate = String.format("%.2f", ((float)personDivingCount/(double)personCurTotal)*100);//潜水用户数占比（百分比）
            personOtherRate = String.format("%.2f",(100 - Double.parseDouble(personActiveRate) - Double.parseDouble(personDivingRate)));//其他用户数占比（百分比）
        }
        resultDto.setPersonActiveRate(personActiveRate+"%");
        resultDto.setPersonDivingRate(personDivingRate+"%");
        resultDto.setPersonOtherRate(personOtherRate+"%");

        resultDto.NullReplaceBlank();
        return resultDto;
    }

    /**
     * 查询群组消息统计
     * @param groupId 群组Id
     * @param startTime 起始时间
     * @param endTime 截止时间
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public StatisticsMessageAnalyseResponseDto getStatisticsMessageAnalyseInfo(String groupId, String startTime, String endTime) throws BusinessException,DataAccessException,Exception{

        _logger.info("查询群组消息统计 groupId=["+groupId+"] startTime=["+startTime+"] endTime=["+endTime+"]");
        CheckUtils.checkParamNull(groupId, RoboMessage.GROUP_ID);
        CheckUtils.checkParamNull(startTime,RoboMessage.start_time);
        CheckUtils.checkParamNull(endTime,RoboMessage.end_time);

        int textCount = 0;//文本消息数量
        int emojCount = 0;//表情消息数量
        int articleCount = 0;//文章消息数量
        int picCount = 0;//图片消息数量
        int rewardCount = 0;//红包消息数量
        int videoCount = 0;//视频消息数量
        int voiceCount = 0;//语音消息数量
        int otherCount = 0;//其他消息数量
        int messageTotal = 0;//消息总数量

        List<StatisticsGroupMessageDto> groupMessageDtoList = statisticsAnalyseDao.getStatisticsGroupMessageList(groupId,startTime,endTime);
        if(groupMessageDtoList!=null&&groupMessageDtoList.size()>0){
            for(StatisticsGroupMessageDto dto:groupMessageDtoList){
                if(dto!=null){
                    messageTotal += dto.getMessageCount();
                    if(dto.getMessageType().equals(GroupChatContentMessageTypeEnum.wechatChatContentMessageTypeEnum.text.getValue())){
                        textCount = dto.getMessageCount();
                        continue;
                    }else if(dto.getMessageType().equals(GroupChatContentMessageTypeEnum.wechatChatContentMessageTypeEnum.emoj.getValue())){
                        emojCount = dto.getMessageCount();
                        continue;
                    }else if(dto.getMessageType().equals(GroupChatContentMessageTypeEnum.wechatChatContentMessageTypeEnum.linkUrl.getValue())){
                        articleCount = dto.getMessageCount();
                        continue;
                    }else if(dto.getMessageType().equals(GroupChatContentMessageTypeEnum.wechatChatContentMessageTypeEnum.pic.getValue())){
                        picCount = dto.getMessageCount();
                        continue;
                    }else if(dto.getMessageType().equals(GroupChatContentMessageTypeEnum.wechatChatContentMessageTypeEnum.systemMsg.getValue()) &&
                            dto.getMessageCount().equals("收到红包，请在手机上查看")){
                        rewardCount = dto.getMessageCount();
                        continue;
                    }else if(dto.getMessageType().equals(GroupChatContentMessageTypeEnum.wechatChatContentMessageTypeEnum.video.getValue())){
                        videoCount = dto.getMessageCount();
                        continue;
                    }else if(dto.getMessageType().equals(GroupChatContentMessageTypeEnum.wechatChatContentMessageTypeEnum.voice.getValue())){
                        voiceCount = dto.getMessageCount();
                        continue;
                    }else{
                        otherCount = dto.getMessageCount();
                        continue;
                    }
                }
            }
        }

        StatisticsMessageAnalyseResponseDto resultDto = new StatisticsMessageAnalyseResponseDto();

        resultDto.setMessageTotal(messageTotal);//消息总数量
        resultDto.setTextCount(textCount);//文本消息数量
        resultDto.setEmojCount(emojCount);//表情消息数量
        resultDto.setArticleCount(articleCount);//文章消息数量
        resultDto.setPicCount(picCount);//图片消息数量
        resultDto.setRewardCount(rewardCount);//红包消息数量
        resultDto.setVideoCount(videoCount);//视频消息数量
        resultDto.setVoiceCount(voiceCount);//语音消息数量
        resultDto.setOtherCount(otherCount);//其他消息数量

        String textRate = "0";//消文本息数量占比（百分比）
        String emojRate = "0";//表情数量占比（百分比）
        String articleRate = "0";//文章数量占比（百分比）
        String picRate = "0";//图片数量占比（百分比）
        String rewardRate = "0";//红包数量占比（百分比）
        String videoRate = "0";//视频数量占比（百分比）
        String voiceRate = "0";//语音数量占比（百分比）
        String otherRate = "0";//其他数量占比（百分比）

        float kk = textCount;
        if(messageTotal!=0){
            textRate = String.format("%.2f", ((float)textCount/(double)messageTotal)*100);//消文本息数量占比（百分比）
            emojRate = String.format("%.2f", ((float)emojCount/(double)messageTotal)*100);//表情数量占比（百分比）
            articleRate = String.format("%.2f", ((float)articleCount/(double)messageTotal)*100);//文章数量占比（百分比）
            picRate = String.format("%.2f", ((float)picCount/(double)messageTotal)*100);//图片数量占比（百分比）
            rewardRate = String.format("%.2f", ((float)rewardCount/(double)messageTotal)*100);//红包数量占比（百分比）
            videoRate = String.format("%.2f", ((float)videoCount/(double)messageTotal)*100);//视频数量占比（百分比）
            voiceRate = String.format("%.2f", ((float)voiceCount/(double)messageTotal)*100);//语音数量占比（百分比）
            otherRate = String.format("%.2f",(100 - Double.parseDouble(textRate) - Double.parseDouble(emojRate) - Double.parseDouble(articleRate) - Double.parseDouble(picRate)
                    - Double.parseDouble(rewardRate) - Double.parseDouble(videoRate) - Double.parseDouble(voiceRate)));//其他数量占比（百分比）

        }
        resultDto.setTextRate(textRate+"%");
        resultDto.setEmojRate(emojRate+"%");
        resultDto.setArticleRate(articleRate+"%");
        resultDto.setPicRate(picRate+"%");
        resultDto.setRewardRate(rewardRate+"%");
        resultDto.setVideoRate(videoRate+"%");
        resultDto.setVoiceRate(voiceRate+"%");
        resultDto.setOtherRate(otherRate+"%");

        resultDto.NullReplaceBlank();
        return resultDto;

    }

    /**
     * 删除当前日期的群组统计数据
     * @param currentDay 当前日期
     * @throws DataAccessException
     * @throws BusinessException
     * @throws Exception
     */
    public void deleteStatisticsGroupInfo(String currentDay) throws DataAccessException,BusinessException,Exception{

        _logger.info("删除当前日期的群组统计数据 currentDay=["+currentDay+"]");
        CheckUtils.checkParamNull(currentDay,RoboMessage.current_day);

        statisticsGroupInfoRepository.deleteStatisticsGroupInfoByCurrentDay(currentDay);
    }

    /**
     * 删除当前日期的群组消息统计数据
     * @param currentDay 当前日期
     * @throws DataAccessException
     * @throws BusinessException
     * @throws Exception
     */
    public void deleteStatisticsGroupMessage(String currentDay) throws DataAccessException,BusinessException,Exception{

        _logger.info("删除当前日期的群组消息统计数据 currentDay=["+currentDay+"]");
        CheckUtils.checkParamNull(currentDay,RoboMessage.current_day);

        statisticsGroupMessageRepository.deleteStatisticsGroupMessageByCurrentDay(currentDay);
    }




}
