package com.robo.server.task.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

/**
 * Created by conor on 17/8/29.
 */
@Configuration
public class DataSourceConfig {

    @Bean(name = "roboDataSource")
    @Qualifier("roboDataSource")
    @ConfigurationProperties(prefix="spring.datasource.robo")
    @Primary
    public DataSource roboDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "statisticsDataSource")
    @Qualifier("statisticsDataSource")
    @ConfigurationProperties(prefix="spring.datasource.statistics")
    public DataSource statisticsDataSource() {
        return DataSourceBuilder.create().build();
    }

}
