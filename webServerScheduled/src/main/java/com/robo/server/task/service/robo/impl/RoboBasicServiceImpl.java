package com.robo.server.task.service.robo.impl;

import com.common.base.exception.BusinessException;
import com.common.utils.CheckUtils;
import com.common.utils.CommonUtils;
import com.robo.server.task.service.robo.RoboBasicService;
import com.robo.server.web.entity.robo.RoboBasicInfoDto;
import com.robo.server.web.repository.robo.RoboBasicInfoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jianghaoming on 17/9/11.
 */
@Service
public class RoboBasicServiceImpl implements RoboBasicService{

    private static final Logger _logger = LoggerFactory.getLogger(RoboBasicServiceImpl.class);

    @Autowired
    private RoboBasicInfoRepository roboBasicInfoRepository;

    public List<RoboBasicInfoDto> getPidListByStatus() throws DataAccessException, BusinessException, Exception{
        return roboBasicInfoRepository.getPidListByStatus();
    }

    public RoboBasicInfoDto updateStatusById(String id, String status)  throws DataAccessException, BusinessException, Exception{

        CheckUtils.checkParamNull(id,"id is not null");
        CheckUtils.checkParamNull(status, "status is not null");
        _logger.info("id=["+id+"]  status=["+status+"]");

        RoboBasicInfoDto dto = roboBasicInfoRepository.findOne(id);
        if(dto != null){
            dto.setId(id);
            dto.setCurrentStatus(status);
            roboBasicInfoRepository.saveAndFlush(dto);
        }
        return dto;
    }
}
