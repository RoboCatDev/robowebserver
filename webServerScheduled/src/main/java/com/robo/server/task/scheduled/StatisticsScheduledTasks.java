package com.robo.server.task.scheduled;

import com.common.base.CommConstants;
import com.common.base.exception.BusinessException;
import com.common.utils.CommonUtils;
import com.common.utils.DateFormatUtil;
import com.robo.server.task.em.GroupActiveStatusEnum;
import com.robo.server.task.em.PersonActiveStatusEnum;
import com.robo.server.task.entity.statistics.StatisticsGroupInfoDto;
import com.robo.server.task.entity.statistics.StatisticsGroupMessageDto;
import com.robo.server.task.entity.statistics.StatisticsPersonInfoDto;
import com.robo.server.task.entity.statistics.StatisticsPersonMessageDto;
import com.robo.server.task.service.robo.StatisticsRoboService;
import com.robo.server.task.service.statistics.StatisticsGroupService;
import com.robo.server.task.service.statistics.StatisticsPersonService;
import com.robo.server.web.entity.statistics.response.StatisticsGroupInfoResponseDto;
import com.robo.server.web.entity.statistics.response.StatisticsGroupMessageResponseDto;
import com.robo.server.web.entity.statistics.response.StatisticsPersonInfoResponseDto;
import com.robo.server.web.entity.statistics.response.StatisticsPersonMessageResponseDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.DataAccessException;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 群组相关 定时任务
 */
@Component
@Configurable
@EnableScheduling
@Profile({"pro","uat","sch"})
public class StatisticsScheduledTasks {

	private static final Logger _logger = LoggerFactory.getLogger(StatisticsScheduledTasks.class);

	@Autowired
	private StatisticsPersonService statisticsPersonService;

	@Autowired
	private StatisticsGroupService statisticsGroupService;

	@Autowired
	private StatisticsRoboService statisticsRoboService;

	/**
	 * 每天凌晨1点
	 */
	@Scheduled(cron = "0 0 1 * * ?")
	public void statisticsTask(){

		//首先获取当前日期前一天
		final String currentDay  = DateFormatUtil.dateToString(DateFormatUtil.addDays(DateFormatUtil.getNow(),-1),DateFormatUtil.YYYY_MM_DD);

		long time0=System.currentTimeMillis();   //获取开始时间

		/**
		 * 所有群组成员统计
		 */
		personInfoList(currentDay);

		long time1=System.currentTimeMillis(); //获取结束时间

		/**
		 * 所有群组成员消息统计
		 */
		personMessageList(currentDay);

		long time2=System.currentTimeMillis(); //获取结束时间

		/**
		 * 所有群组统计
		 */
		groupInfoList(currentDay);

		long time3=System.currentTimeMillis(); //获取结束时间

		/**
		 * 所有群组消息统计
		 */
		groupMessageList(currentDay);

		long time4=System.currentTimeMillis(); //获取结束时间


		_logger.info("=========程序运行时间： "+(time1-time0)+"ms，"+(time2-time1)+"ms，"+(time3-time2)+"ms，"+(time4-time3)+"ms");
	}

	/**
	 * 所有群组成员的统计
	 * @param currentDay 当前日期
	 * @return
	 */
	private void personInfoList(final String currentDay){
		try{
			_logger.info("＝＝＝＝＝＝开始所有群组成员的统计[" + currentDay + "]＝＝＝＝＝");

			//先判断当前日期的数据是否已存在，如果已存在，先删除
			statisticsPersonService.deleteStatisticsPersonInfo(currentDay);

			List<StatisticsPersonInfoResponseDto> responseList = statisticsRoboService.getStatisticsPersonInfoResponseList(currentDay);
			List<StatisticsPersonInfoDto> dataList = new ArrayList<>();
			if(responseList!=null&&responseList.size()>0){

				//查询所有群组成员的最新一条统计信息（按统计时间倒序排，取每位成员的第一条）
				Map<String,StatisticsPersonInfoDto> personMap = new HashMap<>();
				List<StatisticsPersonInfoDto> personAllList = statisticsRoboService.getStatisticsPersonInfoList("");
				if(personAllList!=null&&personAllList.size()>0){
					for(StatisticsPersonInfoDto personDto:personAllList){
						if(personDto!=null&&!CommonUtils.isBlank(personDto.getPersonId())){
							if(personMap.get(personDto.getPersonId())==null){
								personMap.put(personDto.getPersonId(),personDto);
							}
						}
					}
				}

				for(StatisticsPersonInfoResponseDto dto:responseList){
					if(dto!=null){
						dto.NullReplaceBlank();
						StatisticsPersonInfoDto statisticsPersonInfoDto = new StatisticsPersonInfoDto();
						statisticsPersonInfoDto.setPersonId(dto.getPersonId());//群组成员Id
						statisticsPersonInfoDto.setGroupId(dto.getGroupId());//群组Id
						statisticsPersonInfoDto.setMessageTotal(dto.getMessageTotal());//当日消息总数量
						statisticsPersonInfoDto.setCurrentDay(currentDay);//当前日期

						StatisticsPersonInfoDto personInfoDto = new StatisticsPersonInfoDto();

						//然后查询获取成员名下上一次的统计记录
						if(personMap!=null && personMap.size()>0){
							personInfoDto = personMap.get(dto.getPersonId());
						}
						if(personInfoDto==null){
							personInfoDto = new StatisticsPersonInfoDto();
						}
						personInfoDto.NullReplaceBlank();

						int activeDaysContinue = 0;//连续活跃天数
						int activeDaysTotal = 0;//总活跃天数
						if(personInfoDto!=null){
							final String lastDay  = DateFormatUtil.dateToString(DateFormatUtil.addDays(currentDay,-1),DateFormatUtil.YYYY_MM_DD);
							if(personInfoDto.getCurrentDay().equals(lastDay)){
								//连续
								activeDaysContinue = personInfoDto.getActiveDaysContinue();
								activeDaysTotal = personInfoDto.getActiveDaysTotal();
							}else{
								//连续活跃中断
								activeDaysContinue = 0;
								activeDaysTotal = personInfoDto.getActiveDaysTotal();
							}
						}

						//判断当日是否活跃
						int addNum = 0;
						if(dto.getMessageTotal()>=PersonActiveStatusEnum.active_bottom.getIntValue()){
							activeDaysContinue = activeDaysContinue + 1;
							activeDaysTotal = activeDaysTotal + 1;
						}else{
							//连续活跃中断
							activeDaysContinue = 0;
						}

						statisticsPersonInfoDto.setActiveDaysContinue(activeDaysContinue);//当前群组成员连续活跃天数
						statisticsPersonInfoDto.setActiveDaysTotal(activeDaysTotal);////当前群组成员总活跃天数

						statisticsPersonInfoDto.NullReplaceBlank();
						dataList.add(statisticsPersonInfoDto);
					}
				}
			}

			//批量保存
			if(dataList!=null&&dataList.size()>0) {
				statisticsPersonService.saveStaticsPersonInfo(dataList);
			}

			_logger.info("＝＝＝＝＝＝结束所有群组成员的统计[" + currentDay + "]＝＝＝ ＝＝＝＝＝");
		}catch (BusinessException ex) {
			final String message = ex.getErrorDesc();
			_logger.info(CommConstants.BUSINESS_ERROR + message,ex);
		}catch (DataAccessException ex) {
			final String message = CommConstants.DATA_ERROR;
			_logger.info(message,ex);
		}catch (Exception e) {
			final String message = CommConstants.SYSTEM_ERROR;
			_logger.error(message, e);
		}
	}


	/**
	 * 所有群组成员消息的统计
	 * @param currentDay 当前日期
	 * @return
	 */
	private void personMessageList(final String currentDay){
		try{
			_logger.info("＝＝＝＝＝＝开始所有群组成员消息的统计[" + currentDay + "]＝＝＝＝＝");

			//先判断当前日期的数据是否已存在，如果已存在，先删除
			statisticsPersonService.deleteStatisticsPersonMessage(currentDay);

			List<StatisticsPersonMessageResponseDto> responseList = statisticsRoboService.getStatisticsPersonMessageResponseList(currentDay);
			List<StatisticsPersonMessageDto> dataList = new ArrayList<>();
			if(responseList!=null&&responseList.size()>0){
				for(StatisticsPersonMessageResponseDto dto:responseList){
					if(dto!=null){
						dto.NullReplaceBlank();
						StatisticsPersonMessageDto statisticsPersonMessageDto = new StatisticsPersonMessageDto();
						statisticsPersonMessageDto.setPersonId(dto.getPersonId());//群组成员Id
						statisticsPersonMessageDto.setGroupId(dto.getGroupId());//群组Id
						/*
						 * 消息类型
						 * 1文本消息 3图片消息 34语音消息 37好友确认消息 40POSSIBLEFRIEND_MSG 42共享名片 43视频消息
						 * 47动画表情 48位置消息 49分享链接 50VOIPMSG 51微信初始化消息 52VOIPNOTIFY 53VOIPINVITE
						 * 62小视频 9999SYSNOTICE 10000系统消息 10002撤回消息
						 */
						statisticsPersonMessageDto.setMessageType(dto.getMessageType());
						statisticsPersonMessageDto.setMessageCount(dto.getMessageCount());//当前消息类型的消息数量
						statisticsPersonMessageDto.setCurrentDay(currentDay);//当前日期

						statisticsPersonMessageDto.NullReplaceBlank();
						dataList.add(statisticsPersonMessageDto);
					}
				}
			}

			//批量保存
			if(dataList!=null&&dataList.size()>0){
				statisticsPersonService.saveStatisticsPersonMessage(dataList);
			}

			_logger.info("＝＝＝＝＝＝结束所有群组成员消息的统计[" + currentDay + "]＝＝＝ ＝＝＝＝＝");
		}catch (BusinessException ex) {
			final String message = ex.getErrorDesc();
			_logger.info(CommConstants.BUSINESS_ERROR + message,ex);
		}catch (DataAccessException ex) {
			final String message = CommConstants.DATA_ERROR;
			_logger.info(message,ex);
		}catch (Exception e) {
			final String message = CommConstants.SYSTEM_ERROR;
			_logger.error(message, e);
		}
	}

	/**
	 * 所有群组的统计
	 * @param currentDay 当前日期
	 * @return
	 */
	private void groupInfoList(final String currentDay){
		try{
			_logger.info("＝＝＝＝＝＝开始所有群组的统计[" + currentDay + "]＝＝＝＝＝");

			//先判断当前日期的数据是否已存在，如果已存在，先删除
			statisticsGroupService.deleteStatisticsGroupInfo(currentDay);

			List<StatisticsGroupInfoResponseDto> responseList = statisticsRoboService.getStatisticsGroupInfoResponseList(currentDay);
			List<StatisticsGroupInfoDto> dataList = new ArrayList<>();
			if(responseList!=null&&responseList.size()>0){

				//查询所有群组的最新一条统计信息（按时间倒序排，取每个群组的第一条）
				Map<String,StatisticsGroupInfoDto> groupMap = new HashMap<String,StatisticsGroupInfoDto>();
				List<StatisticsGroupInfoDto> groupAllList = statisticsRoboService.getStatisticsGroupInfoList("");
				if(groupAllList!=null&&groupAllList.size()>0){
					for(StatisticsGroupInfoDto groupDto:groupAllList){
						if(groupDto!=null&&!CommonUtils.isBlank(groupDto.getGroupId())){
							if(groupMap.get(groupDto.getGroupId())==null){
								groupMap.put(groupDto.getGroupId(),groupDto);
							}
						}
					}
				}

				for(StatisticsGroupInfoResponseDto dto:responseList){
					if(dto!=null){
						dto.NullReplaceBlank();
						StatisticsGroupInfoDto statisticsGroupInfoDto = new StatisticsGroupInfoDto();
						statisticsGroupInfoDto.setGroupId(dto.getGroupId());//群组Id
						statisticsGroupInfoDto.setPersonTotal(dto.getPersonTotal());//当前群组总成员数
						statisticsGroupInfoDto.setJoinCount(dto.getJoinCount());//当前群组新增成员数
						statisticsGroupInfoDto.setLeaveCount(dto.getLeaveCount());//当前群组离开成员数
						statisticsGroupInfoDto.setMessageTotal(dto.getMessageTotal());//消息总数量
						statisticsGroupInfoDto.setCurrentDay(currentDay);//当前日期

						int divingCount = 0;//潜水用户数
						int activeCount = 0;//活跃用户数
						//查询群组成员当日的消息总数
						List<StatisticsPersonInfoDto> personInfoDtoList = statisticsPersonService.getStatisticsPersonInfoList(dto.getGroupId(),currentDay,currentDay);
						if(personInfoDtoList!=null&&personInfoDtoList.size()>0){
							for(StatisticsPersonInfoDto personInfoDto:personInfoDtoList){
								if(personInfoDto!=null&&!CommonUtils.isBlank(personInfoDto.getPersonId())){
									personInfoDto.NullReplaceBlank();
									int messageTotal = personInfoDto.getMessageTotal();
									if(messageTotal>=PersonActiveStatusEnum.diving_bottom.getIntValue() &&
											messageTotal<=PersonActiveStatusEnum.diving_top.getIntValue()){
										//潜水
										divingCount++;
									}else if(messageTotal>=PersonActiveStatusEnum.active_bottom.getIntValue()){
										//活跃
										activeCount++;
									}
								}
							}
						}
						statisticsGroupInfoDto.setDivingCount(divingCount);//当前群组潜水成员数
						statisticsGroupInfoDto.setActiveCount(activeCount);//当前群组活跃成员数

						//然后查询获取群组名下上一次的统计记录
						StatisticsGroupInfoDto groupInfoDto = new StatisticsGroupInfoDto();
						if(groupMap!=null&&groupMap.size()>0){
							groupInfoDto = groupMap.get(dto.getGroupId());
						}
						if(groupInfoDto==null){
							groupInfoDto = new StatisticsGroupInfoDto();
						}
						groupInfoDto.NullReplaceBlank();

						int activeDaysContinue = 0;//连续活跃天数
						int activeDaysTotal = 0;//总活跃天数
						if(groupInfoDto!=null){
							final String lastDay  = DateFormatUtil.dateToString(DateFormatUtil.addDays(currentDay,-1),DateFormatUtil.YYYY_MM_DD);
							if(groupInfoDto.getCurrentDay().equals(lastDay)){
								//连续
								activeDaysContinue = groupInfoDto.getActiveDaysContinue();
								activeDaysTotal = groupInfoDto.getActiveDaysTotal();
							}else{
								//连续活跃中断
								activeDaysContinue = 0;
								activeDaysTotal = groupInfoDto.getActiveDaysTotal();
							}
						}

						//判断当日是否活跃
						int addNum = 0;
						if(dto.getMessageTotal()>= GroupActiveStatusEnum.active_bottom.getIntValue()){
							activeDaysContinue = activeDaysContinue + 1;
							activeDaysTotal = activeDaysTotal + 1;
						}else{
							//连续活跃中断
							activeDaysContinue = 0;
						}

						statisticsGroupInfoDto.setActiveDaysContinue(activeDaysContinue);//当前群组连续活跃天数
						statisticsGroupInfoDto.setActiveDaysTotal(activeDaysTotal);//当前群组总活跃天数

						statisticsGroupInfoDto.NullReplaceBlank();
						dataList.add(statisticsGroupInfoDto);
					}
				}
			}

			//批量保存
			if(dataList!=null&&dataList.size()>0){
				statisticsGroupService.saveStaticsGroupInfo(dataList);
			}


			_logger.info("＝＝＝＝＝＝结束所有群组的统计[" + currentDay + "]＝＝＝ ＝＝＝＝＝");
		}catch (BusinessException ex) {
			final String message = ex.getErrorDesc();
			_logger.info(CommConstants.BUSINESS_ERROR + message,ex);
		}catch (DataAccessException ex) {
			final String message = CommConstants.DATA_ERROR;
			_logger.info(message,ex);
		}catch (Exception e) {
			final String message = CommConstants.SYSTEM_ERROR;
			_logger.error(message, e);
		}
	}

	/**
	 * 所有群组消息的统计
	 * @param currentDay 当前日期
	 * @return
	 */
	private void groupMessageList(final String currentDay){
		try{
			_logger.info("＝＝＝＝＝＝开始所有群组消息的统计[" + currentDay + "]＝＝＝＝＝");

			//先判断当前日期的数据是否已存在，如果已存在，先删除
			statisticsGroupService.deleteStatisticsGroupMessage(currentDay);

			List<StatisticsGroupMessageResponseDto> responseList = statisticsRoboService.getStatisticsGroupMessageResponseList(currentDay);
			List<StatisticsGroupMessageDto> dataList = new ArrayList<>();
			if(responseList!=null&&responseList.size()>0){
				for(StatisticsGroupMessageResponseDto dto:responseList){
					if(dto!=null){
						dto.NullReplaceBlank();
						StatisticsGroupMessageDto statisticsGroupMessageDto = new StatisticsGroupMessageDto();
						statisticsGroupMessageDto.setGroupId(dto.getGroupId());//群组Id
						 /*
						 * 消息类型
						 * 1文本消息 3图片消息 34语音消息 37好友确认消息 40POSSIBLEFRIEND_MSG 42共享名片 43视频消息
						 * 47动画表情 48位置消息 49分享链接 50VOIPMSG 51微信初始化消息 52VOIPNOTIFY 53VOIPINVITE
						 * 62小视频 9999SYSNOTICE 10000系统消息 10002撤回消息
						 */
						statisticsGroupMessageDto.setMessageType(dto.getMessageType());
						statisticsGroupMessageDto.setMessageCount(dto.getMessageCount());//当前消息类型的消息数量
						statisticsGroupMessageDto.setCurrentDay(currentDay);//当前日期

						statisticsGroupMessageDto.NullReplaceBlank();
						dataList.add(statisticsGroupMessageDto);
					}
				}
			}

			//批量保存
			if(dataList!=null&&dataList.size()>0){
				statisticsGroupService.saveStatisticsGroupMessage(dataList);
			}

			_logger.info("＝＝＝＝＝＝结束所有群组消息的统计[" + currentDay + "]＝＝＝ ＝＝＝＝＝");
		}catch (BusinessException ex) {
			final String message = ex.getErrorDesc();
			_logger.info(CommConstants.BUSINESS_ERROR + message,ex);
		}catch (DataAccessException ex) {
			final String message = CommConstants.DATA_ERROR;
			_logger.info(message,ex);
		}catch (Exception e) {
			final String message = CommConstants.SYSTEM_ERROR;
			_logger.error(message, e);
		}
	}

}
