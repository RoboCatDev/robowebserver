package com.robo.server.task.scheduled;

import com.robo.server.task.service.robo.RoboBasicService;
import com.robo.server.web.em.robo.RoboCurrentStatusEnum;
import com.robo.server.web.entity.robo.RoboBasicInfoDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jianghaoming on 17/9/11.
 *
 *  进程的相关定时操作
 */
@Component
@Configurable
@EnableScheduling
@Profile({"pro","uat"})
public class RoboProcess {


    private static final Logger _logger = LoggerFactory.getLogger(RoboProcess.class);

    @Autowired
    private RoboBasicService roboBasicService;

    /**
     * 检查python robot 进程的活动状态
     *  微信机器人在线状态
     */
    @Scheduled(cron = "0 0/1 * * * ?")
    public void checkPythonStatus(){

        //组装 shell 命令
        List<String> commands = new ArrayList<>();
        commands.add("sh");
        commands.add("-c");
        commands.add("ps -ef | grep python");

        StringBuffer result = new StringBuffer();
        try {
            List<RoboBasicInfoDto> roboList =  roboBasicService.getPidListByStatus();

            ProcessBuilder processBuilder = new ProcessBuilder(commands);
            java.lang.Process process = processBuilder.start();
            BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = "";
            while ((line = input.readLine()) != null) {
                result.append(line + "\n");
            }
            input.close();

            for(RoboBasicInfoDto robot : roboList){
                String pid = robot.getPid();
                if(result.indexOf(pid)>0){
                    _logger.info("fund the pid " + pid);
                }else{
                    _logger.info("not fund the pid " + pid);
                    roboBasicService.updateStatusById(robot.getId(), RoboCurrentStatusEnum.loginout_exception.getValue());
                }
            }

        }catch (Exception e){
            _logger.error(e.getMessage(),e);
        }

    }

}


