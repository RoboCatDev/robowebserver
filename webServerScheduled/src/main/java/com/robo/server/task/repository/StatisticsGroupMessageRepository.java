package com.robo.server.task.repository;

import com.robo.server.task.entity.statistics.StatisticsGroupMessageDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.Table;
import javax.transaction.Transactional;

/**
 * Created by conor on 2017/8/29.
 */
@Repository
@Table(name = "t_statistics_group_message")
public interface StatisticsGroupMessageRepository extends JpaRepository<StatisticsGroupMessageDto,Integer>{

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("delete from StatisticsGroupMessageDto t where t.currentDay=:currentDay")
    void deleteStatisticsGroupMessageByCurrentDay(@Param("currentDay") String currentDay);
}
