package com.robo.server.task.service.robo;

import com.common.base.exception.BusinessException;
import com.robo.server.task.entity.statistics.StatisticsGroupInfoDto;
import com.robo.server.task.entity.statistics.StatisticsPersonInfoDto;
import com.robo.server.web.entity.statistics.response.StatisticsGroupInfoResponseDto;
import com.robo.server.web.entity.statistics.response.StatisticsGroupMessageResponseDto;
import com.robo.server.web.entity.statistics.response.StatisticsPersonInfoResponseDto;
import com.robo.server.web.entity.statistics.response.StatisticsPersonMessageResponseDto;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface StatisticsRoboService {

    /**
     * 查询所有群组成员统计信息
     * @param currentDay 当前日期（yyyy-MM-dd）
     * @return
     * @throws DataAccessException
     * @throws BusinessException
     * @throws Exception
     */
    public List<StatisticsPersonInfoResponseDto> getStatisticsPersonInfoResponseList(final String currentDay) throws DataAccessException,BusinessException,Exception;

    /**
     * 查询所有群组成员消息统计信息
     * @param currentDay 当前日期（yyyy-MM-dd）
     * @return
     * @throws DataAccessException
     * @throws BusinessException
     * @throws Exception
     */
    public List<StatisticsPersonMessageResponseDto> getStatisticsPersonMessageResponseList(final String currentDay) throws DataAccessException,BusinessException,Exception;

    /**
     * 查询所有群组统计信息
     * @param currentDay 当前日期（yyyy-MM-dd）
     * @return
     * @throws DataAccessException
     * @throws BusinessException
     * @throws Exception
     */
    public List<StatisticsGroupInfoResponseDto> getStatisticsGroupInfoResponseList(final String currentDay) throws DataAccessException,BusinessException,Exception;

    /**
     * 查询所有群组消息统计信息
     * @param currentDay 当前日期（yyyy-MM-dd）
     * @return
     * @throws DataAccessException
     * @throws BusinessException
     * @throws Exception
     */
    public List<StatisticsGroupMessageResponseDto> getStatisticsGroupMessageResponseList(final String currentDay) throws DataAccessException,BusinessException,Exception;

    /**
     * 查询群组成员统计列表
     * @param personId 群组成员ID
     * @return
     * @throws DataAccessException
     * @throws BusinessException
     * @throws Exception
     */
    public List<StatisticsPersonInfoDto> getStatisticsPersonInfoList(String personId) throws DataAccessException,BusinessException,Exception;

    /**
     * 查询群组统计列表
     * @param groupId 群组Id
     * @return
     * @throws DataAccessException
     * @throws BusinessException
     * @throws Exception
     */
    public List<StatisticsGroupInfoDto> getStatisticsGroupInfoList(String groupId) throws DataAccessException,BusinessException,Exception;

}
