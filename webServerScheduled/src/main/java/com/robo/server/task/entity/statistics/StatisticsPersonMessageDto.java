package com.robo.server.task.entity.statistics;

import com.robo.server.task.entity.BaseTimeDto;

import javax.persistence.*;

/**
 * 群组成员消息的统计
 * Created by conor on 2017/8/29.
 */
@Entity
@Table(name = "t_statistics_person_message")
public class StatisticsPersonMessageDto extends BaseTimeDto {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
    private String personId;//群组成员Id
    private String groupId;//群组Id
    /*
     * 消息类型
     * 1文本消息 3图片消息 34语音消息 37好友确认消息 40POSSIBLEFRIEND_MSG 42共享名片 43视频消息
     * 47动画表情 48位置消息 49分享链接 50VOIPMSG 51微信初始化消息 52VOIPNOTIFY 53VOIPINVITE
     * 62小视频 9999SYSNOTICE 10000系统消息 10002撤回消息
     */
    private String messageType;
    private Integer messageCount;//当日对应消息类型的消息数量
    private String currentDay;//当前日期

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public Integer getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(Integer messageCount) {
        this.messageCount = messageCount;
    }

    public String getCurrentDay() {
        return currentDay;
    }

    public void setCurrentDay(String currentDay) {
        this.currentDay = currentDay;
    }
}



