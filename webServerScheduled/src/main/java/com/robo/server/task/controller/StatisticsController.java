package com.robo.server.task.controller;

import com.common.base.CommConstants;
import com.common.base.exception.BusinessException;
import com.common.base.response.BaseResponseDto;
import com.common.utils.CommonUtils;
import com.robo.server.task.controller.base.StatisticsBaseController;
import com.robo.server.task.entity.statistics.response.AnalyseReturnResponseDto;
import com.robo.server.task.entity.statistics.response.StatisticsBaseAnalyseResponseDto;
import com.robo.server.task.entity.statistics.response.StatisticsMessageAnalyseResponseDto;
import com.robo.server.task.entity.statistics.response.StatisticsPersonAnalyseResponseDto;
import com.robo.server.task.service.statistics.StatisticsGroupService;
import com.robo.server.web.em.robo.GroupChatContentMessageTypeEnum;
import com.robo.server.web.entity.user.UserLoginInfoDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by conor on 2017/6/13 23:14.
 */
@RestController
@RequestMapping("/v1/robocat/statistics")
public class StatisticsController extends StatisticsBaseController {

    private static final Logger _logger = LoggerFactory.getLogger(StatisticsController.class);

    @Autowired
    private StatisticsGroupService statisticsGroupService;

    /**
     * 群组基本数据统计分析
     * @return
     */
    @GetMapping("/group/analyse")
    public ResponseEntity<BaseResponseDto> statisticsBaseAnalyse(){
        try{
            final String groupId = getRequestParam("groupId");//群组ID
            final String startTime = getRequestParam("startTime");//统计起始时间
            final String endTime = getRequestParam("endTime");//统计截止时间

            UserLoginInfoDto user = getUserAndVerifyToken();

            _logger.info("groupId=["+groupId+"] startTime=["+startTime+"] endTime=["+endTime+"] userId=["+user.getUserId()+"]");

            StatisticsBaseAnalyseResponseDto responseDto = new StatisticsBaseAnalyseResponseDto();
            if(!CommonUtils.isBlank(groupId)){
                if(!CommonUtils.isBlank(startTime)&&!CommonUtils.isBlank(endTime)){
                    responseDto = statisticsGroupService.getStatisticsBaseAnalyseInfo(groupId,startTime,endTime);
                }else{
                    responseDto = statisticsGroupService.getStatisticsGroupBasicInfo(groupId);
                }
            }
            responseDto.NullReplaceBlank();

            Map<String, Object> resultMap = new HashMap<String, Object>();
            resultMap.put("BaseAnalyse", responseDto);

            return succResponse(CommConstants.QUERY_SUCC, resultMap);
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }

    /**
     * 群组成员统计分析
     * @return
     */
    @GetMapping("/group/person")
    public ResponseEntity<BaseResponseDto> statisticsPersonAnalyse(){
        try{
            final String groupId = getRequestParam("groupId");//群组ID
            final String startTime = getRequestParam("startTime");//统计起始时间
            final String endTime = getRequestParam("endTime");//统计截止时间

            UserLoginInfoDto user = getUserAndVerifyToken();

            _logger.info("groupId=["+groupId+"] startTime=["+startTime+"] endTime=["+endTime+"] userId=["+user.getUserId()+"]");

            Map<String, Object> resultMap = new HashMap<String, Object>();
            List<AnalyseReturnResponseDto> responseList = new ArrayList<AnalyseReturnResponseDto>();

            StatisticsPersonAnalyseResponseDto responseDto = new StatisticsPersonAnalyseResponseDto();
            if(!CommonUtils.isBlank(groupId)&&!CommonUtils.isBlank(startTime)&&!CommonUtils.isBlank(endTime)){
                responseDto = statisticsGroupService.getStatisticsPersonAnalyseInfo(groupId,startTime,endTime);
                responseDto.NullReplaceBlank();

                AnalyseReturnResponseDto activeDto = new AnalyseReturnResponseDto();
                activeDto.setName("活跃用户");
                activeDto.setValue(responseDto.getPersonActiveCount());
                activeDto.setRate(responseDto.getPersonActiveRate());

                AnalyseReturnResponseDto divingDto = new AnalyseReturnResponseDto();
                divingDto.setName("潜水用户");
                divingDto.setValue(responseDto.getPersonDivingCount());
                divingDto.setRate(responseDto.getPersonDivingRate());

                AnalyseReturnResponseDto otherDto = new AnalyseReturnResponseDto();
                otherDto.setName("其他用户");
                otherDto.setValue(responseDto.getPersonOtherCount());
                otherDto.setRate(responseDto.getPersonOtherRate());

                responseList.add(activeDto);
                responseList.add(divingDto);
                responseList.add(otherDto);
            }

            responseDto.NullReplaceBlank();;
            resultMap.put("personCurTotal", responseDto.getPersonCurTotal());
            resultMap.put("personActiveCount", responseDto.getPersonActiveCount());
            resultMap.put("list", responseList);

            return succResponse(CommConstants.QUERY_SUCC,resultMap,"personAnalyse");
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }


    @GetMapping("/group/message")
    public ResponseEntity<BaseResponseDto> statisticsMessageAnalyse(){
        try{
            final String groupId = getRequestParam("groupId");//群组ID
            final String startTime = getRequestParam("startTime");//统计起始时间
            final String endTime = getRequestParam("endTime");//统计截止时间

            UserLoginInfoDto user = getUserAndVerifyToken();

            _logger.info("groupId=["+groupId+"] startTime=["+startTime+"] endTime=["+endTime+"] userId=["+user.getUserId()+"]");

            Map<String, Object> resultMap = new HashMap<String, Object>();
            StatisticsMessageAnalyseResponseDto responseDto = new StatisticsMessageAnalyseResponseDto();
            List<AnalyseReturnResponseDto> responseList = new ArrayList<AnalyseReturnResponseDto>();

            if(!CommonUtils.isBlank(groupId)&&!CommonUtils.isBlank(startTime)&&!CommonUtils.isBlank(endTime)){
                responseDto = statisticsGroupService.getStatisticsMessageAnalyseInfo(groupId,startTime,endTime);
                responseDto.NullReplaceBlank();

                for(GroupChatContentMessageTypeEnum typeEnum:GroupChatContentMessageTypeEnum.getAllList()){
                    AnalyseReturnResponseDto returnResponsDto = new AnalyseReturnResponseDto();
                    returnResponsDto.setName(GroupChatContentMessageTypeEnum.getDesc(typeEnum.getValue()));
                    if(typeEnum.getValue().equals(GroupChatContentMessageTypeEnum.text.getValue())){
                        returnResponsDto.setValue(responseDto.getTextCount());
                        returnResponsDto.setRate(responseDto.getTextRate());
                    }else if(typeEnum.getValue().equals(GroupChatContentMessageTypeEnum.emoj.getValue())){
                        returnResponsDto.setValue(responseDto.getEmojCount());
                        returnResponsDto.setRate(responseDto.getEmojRate());
                    }else if(typeEnum.getValue().equals(GroupChatContentMessageTypeEnum.article.getValue())){
                        returnResponsDto.setValue(responseDto.getArticleCount());
                        returnResponsDto.setRate(responseDto.getArticleRate());
                    }else if(typeEnum.getValue().equals(GroupChatContentMessageTypeEnum.pic.getValue())){
                        returnResponsDto.setValue(responseDto.getPicCount());
                        returnResponsDto.setRate(responseDto.getPicRate());
                    }else if(typeEnum.getValue().equals(GroupChatContentMessageTypeEnum.reward.getValue())){
                        returnResponsDto.setValue(responseDto.getRewardCount());
                        returnResponsDto.setRate(responseDto.getRewardRate());
                    }else if(typeEnum.getValue().equals(GroupChatContentMessageTypeEnum.video.getValue())){
                        returnResponsDto.setValue(responseDto.getVideoCount());
                        returnResponsDto.setRate(responseDto.getVideoRate());
                    }else if(typeEnum.getValue().equals(GroupChatContentMessageTypeEnum.voice.getValue())){
                        returnResponsDto.setValue(responseDto.getVoiceCount());
                        returnResponsDto.setRate(responseDto.getVoiceRate());
                    }else if(typeEnum.getValue().equals(GroupChatContentMessageTypeEnum.other.getValue())){
                        returnResponsDto.setValue(responseDto.getOtherCount());
                        returnResponsDto.setRate(responseDto.getOtherRate());
                    }

                    returnResponsDto.NullReplaceBlank();
                    responseList.add(returnResponsDto);
                }
            }
            responseDto.NullReplaceBlank();

            resultMap.put("messageTotal", responseDto.getMessageTotal());
            resultMap.put("list", responseList);

            return succResponse(CommConstants.QUERY_SUCC, resultMap,"messageAnalyse");
        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }

}
