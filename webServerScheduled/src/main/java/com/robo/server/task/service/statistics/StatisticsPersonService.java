package com.robo.server.task.service.statistics;

import com.common.base.exception.BusinessException;
import com.robo.server.task.entity.statistics.StatisticsPersonInfoDto;
import com.robo.server.task.entity.statistics.StatisticsPersonMessageDto;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by conor on 2017/8/29.
 */
@Service
public interface StatisticsPersonService {

    /**
     * 保存群组成员统计
     * @param statisticsPersonInfoDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public StatisticsPersonInfoDto saveStaticsPersonInfo(StatisticsPersonInfoDto statisticsPersonInfoDto) throws BusinessException,DataAccessException,Exception;

    /**
     * 保存群组成员统计(批量)
     * @param statisticsPersonInfoDtoList
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public List<StatisticsPersonInfoDto> saveStaticsPersonInfo(List<StatisticsPersonInfoDto> statisticsPersonInfoDtoList) throws BusinessException,DataAccessException,Exception;

    /**
     * 保存群组成员消息的统计
     * @param statisticsPersonMessageDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public StatisticsPersonMessageDto saveStatisticsPersonMessage(StatisticsPersonMessageDto statisticsPersonMessageDto) throws BusinessException,DataAccessException,Exception;

    /**
     * 保存群组成员消息的统计（批量）
     * @param statisticsPersonMessageDtoList
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public List<StatisticsPersonMessageDto> saveStatisticsPersonMessage(List<StatisticsPersonMessageDto> statisticsPersonMessageDtoList) throws BusinessException,DataAccessException,Exception;

    /**
     * 查询群组成员基本信息列表
     * @param groupId 群组Id
     * @param startTime 起始时间
     * @param endTime 截止时间
     * @return
     */
    public List<StatisticsPersonInfoDto> getStatisticsPersonInfoList(String groupId, String startTime, String endTime) throws BusinessException,DataAccessException,Exception;


    /**
     * 删除当前日期的群组成员统计数据
     * @param currentDay 当前日期
     * @throws DataAccessException
     * @throws BusinessException
     * @throws Exception
     */
    public void deleteStatisticsPersonInfo(String currentDay) throws DataAccessException,BusinessException,Exception;

    /**
     * 删除当前日期的群组成员消息统计数据
     * @param currentDay 当前日期
     * @throws DataAccessException
     * @throws BusinessException
     * @throws Exception
     */
    public void deleteStatisticsPersonMessage(String currentDay) throws DataAccessException,BusinessException,Exception;

}
