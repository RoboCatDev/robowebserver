package com.robo.server.task.service.statistics.impl;

import com.common.base.exception.BusinessException;
import com.common.utils.CheckUtils;
import com.robo.server.task.entity.statistics.StatisticsPersonInfoDto;
import com.robo.server.task.entity.statistics.StatisticsPersonMessageDto;
import com.robo.server.task.repository.StatisticsPersonInfoRepository;
import com.robo.server.task.repository.StatisticsPersonMessageRepository;
import com.robo.server.task.service.statistics.StatisticsPersonService;
import com.robo.server.web.message.RoboMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by conor on 2017/8/29.
 */
@Service
public class StatisticsPersonServiceImpl implements StatisticsPersonService {

    private static final Logger _logger = LoggerFactory.getLogger(StatisticsPersonServiceImpl.class);

    @Autowired
    private StatisticsPersonInfoRepository statisticsPersonInfoRepository;

    @Autowired
    private StatisticsPersonMessageRepository statisticsPersonMessageRepository;

    /**
     * 保存群组成员统计
     * @param statisticsPersonInfoDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public StatisticsPersonInfoDto saveStaticsPersonInfo(StatisticsPersonInfoDto statisticsPersonInfoDto) throws BusinessException,DataAccessException,Exception{

        _logger.info("保存群组成员统计 statisticsPersonDto===>"+ statisticsPersonInfoDto.toLogger());

        return statisticsPersonInfoRepository.save(statisticsPersonInfoDto);
    }

    /**
     * 保存群组成员统计(批量)
     * @param statisticsPersonInfoDtoList
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public List<StatisticsPersonInfoDto> saveStaticsPersonInfo(List<StatisticsPersonInfoDto> statisticsPersonInfoDtoList) throws BusinessException,DataAccessException,Exception{

        _logger.info("保存群组成员统计（批量） statisticsPersonInfoDtoList.size===>"+ statisticsPersonInfoDtoList.size());

        return statisticsPersonInfoRepository.save(statisticsPersonInfoDtoList);
    }

    /**
     * 保存群组成员消息的统计
     * @param statisticsPersonMessageDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public StatisticsPersonMessageDto saveStatisticsPersonMessage(StatisticsPersonMessageDto statisticsPersonMessageDto) throws BusinessException,DataAccessException,Exception{

        _logger.info("保存群组成员消息的统计 statisticsPersonMessageDto===>"+ statisticsPersonMessageDto.toLogger());

        return statisticsPersonMessageRepository.save(statisticsPersonMessageDto);
    }

    /**
     * 保存群组成员消息的统计（批量）
     * @param statisticsPersonMessageDtoList
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public List<StatisticsPersonMessageDto> saveStatisticsPersonMessage(List<StatisticsPersonMessageDto> statisticsPersonMessageDtoList) throws BusinessException,DataAccessException,Exception{

        _logger.info("保存群组成员消息的统计（批量） statisticsPersonMessageDtoList.size===>"+ statisticsPersonMessageDtoList.size());

        return statisticsPersonMessageRepository.save(statisticsPersonMessageDtoList);
    }

    /**
     * 查询群组成员基本统计信息列表
     * @param groupId 群组Id
     * @param startTime 起始时间
     * @param endTime 截止时间
     * @return
     */
    public List<StatisticsPersonInfoDto> getStatisticsPersonInfoList(String groupId, String startTime, String endTime) throws BusinessException,DataAccessException,Exception{

        _logger.info("查询群组成员基本统计信息列表 groupId=["+groupId+"] startTime=["+startTime+"] endTime=["+endTime+"]");
        CheckUtils.checkParamNull(groupId, RoboMessage.GROUP_ID);
        CheckUtils.checkParamNull(startTime,RoboMessage.start_time);
        CheckUtils.checkParamNull(endTime,RoboMessage.end_time);

        return statisticsPersonInfoRepository.findAllByGroupIdAndCurrentDate(groupId,startTime,endTime);

    }

    /**
     * 删除当前日期的群组成员统计数据
     * @param currentDay 当前日期
     * @throws DataAccessException
     * @throws BusinessException
     * @throws Exception
     */
    public void deleteStatisticsPersonInfo(String currentDay) throws DataAccessException,BusinessException,Exception{

        _logger.info("删除当前日期的群组成员统计数据 currentDay=["+currentDay+"]");
        CheckUtils.checkParamNull(currentDay,RoboMessage.current_day);

        statisticsPersonInfoRepository.deleteStatisticsPersonInfoByCurrentDay(currentDay);
    }

    /**
     * 删除当前日期的群组成员消息统计数据
     * @param currentDay 当前日期
     * @throws DataAccessException
     * @throws BusinessException
     * @throws Exception
     */
    public void deleteStatisticsPersonMessage(String currentDay) throws DataAccessException,BusinessException,Exception{

        _logger.info("删除当前日期的群组成员消息统计数据 currentDay=["+currentDay+"]");
        CheckUtils.checkParamNull(currentDay,RoboMessage.current_day);

        statisticsPersonMessageRepository.deleteStatisticsPersonMessageByCurrentDay(currentDay);

    }

}
