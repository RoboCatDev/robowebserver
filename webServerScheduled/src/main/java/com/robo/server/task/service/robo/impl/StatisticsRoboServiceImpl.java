package com.robo.server.task.service.robo.impl;

import com.common.base.exception.BusinessException;
import com.common.utils.CommonUtils;
import com.robo.server.task.entity.statistics.StatisticsGroupInfoDto;
import com.robo.server.task.entity.statistics.StatisticsPersonInfoDto;
import com.robo.server.task.repository.StatisticsGroupInfoRepository;
import com.robo.server.task.repository.StatisticsPersonInfoRepository;
import com.robo.server.task.service.robo.StatisticsRoboService;
import com.robo.server.web.dao.statistics.StatisticsRoboDao;
import com.robo.server.web.entity.statistics.response.StatisticsGroupInfoResponseDto;
import com.robo.server.web.entity.statistics.response.StatisticsGroupMessageResponseDto;
import com.robo.server.web.entity.statistics.response.StatisticsPersonInfoResponseDto;
import com.robo.server.web.entity.statistics.response.StatisticsPersonMessageResponseDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatisticsRoboServiceImpl implements StatisticsRoboService {

    private static final Logger _logger = LoggerFactory.getLogger(StatisticsRoboServiceImpl.class);

    @Autowired
    private StatisticsRoboDao statisticsRoboDao;

    @Autowired
    private StatisticsPersonInfoRepository statisticsPersonInfoRepository;

    @Autowired
    private StatisticsGroupInfoRepository statisticsGroupInfoRepository;

    /**
     * 查询所有群组成员统计信息
     * @param currentDay 当前日期（yyyy-MM-dd）
     * @return
     * @throws DataAccessException
     * @throws BusinessException
     * @throws Exception
     */
    public List<StatisticsPersonInfoResponseDto> getStatisticsPersonInfoResponseList(final String currentDay) throws DataAccessException,BusinessException,Exception{

        _logger.info("查询所有群组成员统计信息 currentDay=["+currentDay+"]");

        return statisticsRoboDao.getStatisticsPersonInfoResponseList(currentDay);
    }

    /**
     * 查询所有群组成员消息统计信息
     * @param currentDay 当前日期（yyyy-MM-dd）
     * @return
     * @throws DataAccessException
     * @throws BusinessException
     * @throws Exception
     */
    public List<StatisticsPersonMessageResponseDto> getStatisticsPersonMessageResponseList(final String currentDay) throws DataAccessException,BusinessException,Exception{

        _logger.info("查询所有群组成员消息统计信息 currentDay=["+currentDay+"]");

        return statisticsRoboDao.getStatisticsPersonMessageResponseList(currentDay);
    }

    /**
     * 查询所有群组统计信息
     * @param currentDay 当前日期（yyyy-MM-dd）
     * @return
     * @throws DataAccessException
     * @throws BusinessException
     * @throws Exception
     */
    public List<StatisticsGroupInfoResponseDto> getStatisticsGroupInfoResponseList(final String currentDay) throws DataAccessException,BusinessException,Exception{

        _logger.info("查询所有群组统计信息 currentDay=["+currentDay+"]");

        return statisticsRoboDao.getStatisticsGroupInfoResponseList(currentDay);
    }

    /**
     * 查询所有群组消息统计信息
     * @param currentDay 当前日期（yyyy-MM-dd）
     * @return
     * @throws DataAccessException
     * @throws BusinessException
     * @throws Exception
     */
    public List<StatisticsGroupMessageResponseDto> getStatisticsGroupMessageResponseList(final String currentDay) throws DataAccessException,BusinessException,Exception{

        _logger.info("查询所有群组消息统计信息 currentDay=["+currentDay+"]");

        return statisticsRoboDao.getStatisticsGroupMessageResponseList(currentDay);

    }

    /**
     * 查询群组成员统计列表
     * @param personId 群组成员ID
     * @return
     * @throws DataAccessException
     * @throws BusinessException
     * @throws Exception
     */
    public List<StatisticsPersonInfoDto> getStatisticsPersonInfoList(String personId) throws DataAccessException,BusinessException,Exception{

        _logger.info("查询群组成员统计列表 personId=["+personId+"]");
//        CheckUtils.checkParamNull(personId, RoboMessage.GROUP_PERSON_ID);

        if(!CommonUtils.isBlank(personId)){
            StatisticsPersonInfoDto dataDto = new StatisticsPersonInfoDto();
            dataDto.setPersonId(personId);
            return statisticsPersonInfoRepository.findAll(Example.of(dataDto),new Sort(Sort.Direction.DESC,"currentDay"));
        }else{
            return statisticsPersonInfoRepository.findAll(new Sort(Sort.Direction.DESC,"currentDay"));
        }

    }

    /**
     * 查询群组统计列表
     * @param groupId 群组Id
     * @return
     * @throws DataAccessException
     * @throws BusinessException
     * @throws Exception
     */
    public List<StatisticsGroupInfoDto> getStatisticsGroupInfoList(String groupId) throws DataAccessException,BusinessException,Exception{

        _logger.info("查询群组统计列表 groupId=["+groupId+"]");
//        CheckUtils.checkParamNull(groupId,RoboMessage.GROUP_ID);

        if(!CommonUtils.isBlank(groupId)){
            StatisticsGroupInfoDto dataDto = new StatisticsGroupInfoDto();
            dataDto.setGroupId(groupId);
            return statisticsGroupInfoRepository.findAll(Example.of(dataDto),new Sort(Sort.Direction.DESC,"currentDay"));
        }else{
            return statisticsGroupInfoRepository.findAll(new Sort(Sort.Direction.DESC,"currentDay"));
        }
    }

}
