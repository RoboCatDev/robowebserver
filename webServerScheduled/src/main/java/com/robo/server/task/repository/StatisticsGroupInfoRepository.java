package com.robo.server.task.repository;

import com.robo.server.task.entity.statistics.StatisticsGroupInfoDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.Table;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by conor on 2017/8/29.
 */
@Repository
@Table(name = "t_statistics_group_info")
public interface StatisticsGroupInfoRepository extends JpaRepository<StatisticsGroupInfoDto,Integer> {

    @Query("select t from StatisticsGroupInfoDto t where t.groupId=:groupId and t.currentDay>=:startTime and t.currentDay<=:endTime order by t.currentDay desc")
    List<StatisticsGroupInfoDto> findAllByGroupIdAndCurrentDate(@Param("groupId") String groupId,@Param("startTime") String startTime,@Param("endTime") String endTime);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("delete from StatisticsGroupInfoDto t where t.currentDay=:currentDay")
    void deleteStatisticsGroupInfoByCurrentDay(@Param("currentDay") String currentDay);
}
