package com.robo.server.task.entity.statistics.response;

import com.common.base.BaseDto;

/**
 * 数据统计分析结果（某段时间内）
 * Created by conor on 2017/8/17.
 */
public class StatisticsBaseAnalyseResponseDto extends BaseDto {

    private String groupId;//群组Id
    private String groupName;//群组名称
    private String analyStartTime;//起始分析时间
    private String analyEndTime;//结束分析时间
    private Integer personInitTotal;//初始群组总人数
    private Integer personJoinCount;//群组新增人数
    private Integer personLeaveCount;//群组离开人数
    private Integer personCurTotal;//当前群组总人数
    private Integer daysActiveTotal;//群组总的活跃天数
    private Integer chatContentTotal;//群组总的消息数

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getAnalyStartTime() {
        return analyStartTime;
    }

    public void setAnalyStartTime(String analyStartTime) {
        this.analyStartTime = analyStartTime;
    }

    public String getAnalyEndTime() {
        return analyEndTime;
    }

    public void setAnalyEndTime(String analyEndTime) {
        this.analyEndTime = analyEndTime;
    }

    public Integer getPersonInitTotal() {
        return personInitTotal;
    }

    public void setPersonInitTotal(Integer personInitTotal) {
        this.personInitTotal = personInitTotal;
    }

    public Integer getPersonJoinCount() {
        return personJoinCount;
    }

    public void setPersonJoinCount(Integer personJoinCount) {
        this.personJoinCount = personJoinCount;
    }

    public Integer getPersonLeaveCount() {
        return personLeaveCount;
    }

    public void setPersonLeaveCount(Integer personLeaveCount) {
        this.personLeaveCount = personLeaveCount;
    }

    public Integer getPersonCurTotal() {
        return personCurTotal;
    }

    public void setPersonCurTotal(Integer personCurTotal) {
        this.personCurTotal = personCurTotal;
    }

    public Integer getDaysActiveTotal() {
        return daysActiveTotal;
    }

    public void setDaysActiveTotal(Integer daysActiveTotal) {
        this.daysActiveTotal = daysActiveTotal;
    }

    public Integer getChatContentTotal() {
        return chatContentTotal;
    }

    public void setChatContentTotal(Integer chatContentTotal) {
        this.chatContentTotal = chatContentTotal;
    }
}



