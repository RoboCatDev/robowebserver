package com.robo.server.task.dao.statistics;

import com.robo.server.task.entity.statistics.StatisticsGroupMessageDto;
import com.robo.server.task.entity.statistics.response.StatisticsBaseAnalyseResponseDto;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by conor on 2017/8/30.
 */
@Component
public interface StatisticsAnalyseDao {

    /**
     * 查询群组基本统计结果
     * @param groupId 群组Id
     * @param startTime 起始时间
     * @param endTime 截止时间
     * @return
     * @throws DataAccessException
     */
    public StatisticsBaseAnalyseResponseDto getStatisticsBaseAnalyseInfo(final String groupId, final String startTime, final String endTime) throws DataAccessException;

    /**
     * 查询群组消息统计结果
     * @param groupId 群组Id
     * @param startTime 起始时间
     * @param endTime 截止时间
     * @return
     * @throws DataAccessException
     */
    public List<StatisticsGroupMessageDto> getStatisticsGroupMessageList(final String groupId, final String startTime, final String endTime) throws DataAccessException;

}
