package com.robo.server.task.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import javax.sql.DataSource;
import java.util.Map;

/**
 * Created by conor on 17/8/29.
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef="entityManagerFactoryRobo",
        transactionManagerRef="transactionManagerRobo",
        basePackages= {"com.robo.server.web.repository","com.robo.server.web.dao"}) //设置Repository所在位置
public class RoboDataSourceConfig {

    @Autowired
    @Qualifier("roboDataSource")
    private DataSource roboDataSource;

    @Autowired
    private JpaProperties jpaProperties;

    @Bean(name = "entityManagerRobo")
    public EntityManager entityManager(EntityManagerFactoryBuilder builder) {
        return entityManagerFactoryRobo(builder).getObject().createEntityManager();
    }


    @Bean(name = "entityManagerFactoryRobo")
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryRobo (EntityManagerFactoryBuilder builder) {
        return builder
                .dataSource(roboDataSource)
                .properties(getVendorProperties(roboDataSource))
                .packages("com.robo.server.web.entity") //设置实体类所在位置
                .persistenceUnit("entityManagerFactory")
                .build();
    }

    private Map<String, String> getVendorProperties(DataSource dataSource) {
        return jpaProperties.getHibernateProperties(dataSource);
    }

    @Bean(name = "transactionManagerRobo")
    public PlatformTransactionManager transactionManagerRobo(EntityManagerFactoryBuilder builder) {
        return new JpaTransactionManager(entityManagerFactoryRobo(builder).getObject());
    }

}
