package com.robo.server.task.dao.statistics.impl;

import com.robo.server.task.dao.BaseStatisticsDaoImpl;
import com.robo.server.task.dao.statistics.StatisticsAnalyseDao;
import com.robo.server.task.entity.statistics.StatisticsGroupMessageDto;
import com.robo.server.task.entity.statistics.response.StatisticsBaseAnalyseResponseDto;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by conor on 2017/8/30.
 */
@Component
public class StatisticsAnalyseDaoImpl extends BaseStatisticsDaoImpl implements StatisticsAnalyseDao{

    /**
     * 查询群组基本统计结果
     * @param groupId 群组Id
     * @param startTime 起始时间
     * @param endTime 截止时间
     * @return
     * @throws DataAccessException
     */
    public StatisticsBaseAnalyseResponseDto getStatisticsBaseAnalyseInfo(final String groupId, final String startTime, final String endTime) throws DataAccessException{

        final String sql = "select sum(join_count) as person_join_count,sum(leave_count) as person_leave_count," +
                "sum(message_total) as chat_content_total from t_statistics_group_info " +
                "where current_day>=:startTime and current_day<=:endTime and group_id=:groupId";

        Map<String,Object> params = new HashMap<String,Object>();
        params.put("groupId",groupId);
        params.put("startTime",startTime);
        params.put("endTime",endTime);

        return (StatisticsBaseAnalyseResponseDto) queryEntity(sql,params,StatisticsBaseAnalyseResponseDto.class);

    }

    /**
     * 查询群组消息统计结果
     * @param groupId 群组Id
     * @param startTime 起始时间
     * @param endTime 截止时间
     * @return
     * @throws DataAccessException
     */
    public List<StatisticsGroupMessageDto> getStatisticsGroupMessageList(final String groupId, final String startTime, final String endTime) throws DataAccessException{

        final String sql = "select sum(message_count) as message_count,message_type,group_id,current_day from t_statistics_group_message " +
                "group by group_id,message_type having current_day>=:startTime and current_day<=:endTime and group_id=:groupId";

        Map<String,Object> params = new HashMap<String,Object>();
        params.put("groupId",groupId);
        params.put("startTime",startTime);
        params.put("endTime",endTime);

        return queryListEntity(sql,params,StatisticsGroupMessageDto.class);

    }


}
