package com.robo.server.task;

import com.robo.server.task.scheduled.StatisticsScheduledTasks;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Created by conor on 17/8/30.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringBootTest(classes = {ScheduledApplication.class})
public class StatisticsTest {

    @Autowired
    private StatisticsScheduledTasks statisticsScheduledTasks;

    @Test
    public void test(){
        statisticsScheduledTasks.statisticsTask();
    }

}
