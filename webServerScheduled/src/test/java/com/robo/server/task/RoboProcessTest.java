package com.robo.server.task;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jianghaoming on 17/9/11.
 */
public class RoboProcessTest {


    @Test
    public void getPID() throws IOException {

        List<String> serverPid = new ArrayList<>();
        serverPid.add("11456");
        serverPid.add("32783");


        List<String> commands = new ArrayList<String>();
        commands.add("sh");
        commands.add("-c");
        commands.add("ps -ef | grep python");

        StringBuffer result = new StringBuffer();
        try {
            ProcessBuilder processBuilder = new ProcessBuilder(commands);
            Process  process = processBuilder.start();
            BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = "";
            while ((line = input.readLine()) != null) {
                result.append(line + "\n");
            }
            input.close();
            System.out.println(result);
            for(String str : serverPid){
                if(result.indexOf(str)>0){
                    System.out.println("找到pid ：" + str);
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        }



    }
}
