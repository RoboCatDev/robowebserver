package com.common.utils;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by Adam on 12/05/2017.
 */
public class TranscodeUtils {

    private static final Logger LOG = LoggerFactory.getLogger(TranscodeUtils.class);

    private static final String TRANSCODE_TOOL_PATH = "/opt/silk-v3-decoder/converter.sh";
    private static final String FFMPEG_PATH = "/opt/silk-v3-decoder/ffmpeg";
    //command line parameters of converting silk to mp3, the parameter is silk file path
    private static final String COMMAND_LINE_PARAMETERS_CONVERT_SILK_TO_MP3 = " %s mp3";
    //command line parameters of converting mp3 to amr, the parameters are mp3 file path and amr file path
    private static final String COMMAND_LINE_PARAMETERS_CONVERT_MP3_TO_AMR = " -i %s -ar 8000 -ac 1 %s";

    public static boolean silkToMp3(String silkFilePath, String mp3FilePath) {
        if (StringUtils.isEmpty(silkFilePath) || StringUtils.isEmpty(mp3FilePath)) {
            LOG.error("Failed to convert silk to mp3, silkFilePath:{}, mp3FilePath:{}",
                    silkFilePath, mp3FilePath);
            return false;
        }

        if (!new File(TRANSCODE_TOOL_PATH).exists() && !new File(silkFilePath).exists()) {
            LOG.error("The files do not exist: TRANSCODE_TOOL_PATH:{}, silkFilePath:{}",
                    TRANSCODE_TOOL_PATH, silkFilePath);
            return false;
        }

        try {
            runExternalProcess(TRANSCODE_TOOL_PATH,
                    String.format(COMMAND_LINE_PARAMETERS_CONVERT_SILK_TO_MP3, silkFilePath));
            File silkFile = new File(silkFilePath);
            Files.move(Paths.get(silkFile.getParent() + File.separator + getMp3Filename(silkFilePath)),
                    Paths.get(mp3FilePath),
                    StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
            LOG.error("Failed to convert silk to mp3, silkFilePath:{}, mp3FilePath:{}",
                    silkFilePath, mp3FilePath, e);
            return false;
        }

        return true;
    }

    public static String getMp3Filename(String silkFilePath) {
        if (StringUtils.isEmpty(silkFilePath) || silkFilePath.endsWith("/"))
            return null;

        if (silkFilePath.indexOf('/') != -1) {
            return silkFilePath.substring(silkFilePath.lastIndexOf('/') + 1, silkFilePath.lastIndexOf(".")) + ".mp3";
        } else {
            return silkFilePath.substring(0, silkFilePath.lastIndexOf(".")) + ".mp3";
        }
    }

    public static boolean mp3ToAmr(String mp3FilePath, String amrFilePath) {
        if (StringUtils.isEmpty(mp3FilePath) || StringUtils.isEmpty(amrFilePath)) {
            LOG.error("Failed to convert silk to mp3, mp3FilePath:{}, amrFilePath:{}",
                    mp3FilePath, amrFilePath);
            return false;
        }

        if (!new File(FFMPEG_PATH).exists() && !new File(mp3FilePath).exists()) {
            LOG.error("The files do not exist: FFMPEG_PATH:{}, mp3FilePath:{}",
                    FFMPEG_PATH, mp3FilePath);
            return false;
        }

        try {
            runExternalProcess(FFMPEG_PATH,
                    String.format(COMMAND_LINE_PARAMETERS_CONVERT_MP3_TO_AMR, mp3FilePath, amrFilePath));
        } catch (Exception e) {
            LOG.error("Failed to convert mp3 to amr, mp3FilePath:{}, amrFilePath:{}",
                    mp3FilePath, amrFilePath, e);
            return false;
        }

        return true;
    }

    private static String runExternalProcess(String exec, String param) throws Exception {
        LOG.info("Start process {} {} ", exec, param);

        List<String> cmd = new ArrayList<>();
        cmd.add(exec);
        if (param != null && !param.isEmpty()) {
            param = param.trim();
            cmd.addAll(Arrays.asList(param.split("\\s+")));
        }

        //Print the output from
        ProcessBuilder pb = new ProcessBuilder(cmd.toArray(new String[0]));
        pb.redirectErrorStream(true);
        Process process;
        try {
            process = pb.start();
        } catch (IOException e) {
            LOG.error("Failed to start process: exec={}, param={}, exception={}", exec, param, e);
            throw new Exception("Failed to start process");
        }
        Timer timer = null;
        String output = "";
        try (
                BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()))
        ) {
            String line;

            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    process.destroy();
                }
            }, TimeUnit.SECONDS.toMillis(60 * 60));

            while ((line = br.readLine()) != null) {
                output += line;
                LOG.info("Exec result - {} ", line);
            }
        } catch (IOException e) {
            LOG.error("Failed to read the output of the process.", e);
            //Metric
        } finally {
            if (timer != null) {
                timer.cancel();
            }
            try {
                process.waitFor();
            } catch (InterruptedException e) {
                LOG.error("The process is interrupted: exec={}, param={}", exec, param);
            }
        }

        //Igore the return of 100 from transform
        //Map<String, Integer> exitWhiteList = ImmutableMap.of("transform", 100);

        if (process.exitValue() != 0/* && !Integer.valueOf(process.exitValue()).equals(exitWhiteList.get(exec))*/) {
            //Something wrong
            LOG.error("Failed to read run the process: exec={}, param={}, exitValue={}.", exec, param, process.exitValue());
            throw new Exception("Unable to transcode the media data when call "
                    + exec + " Exit value: " + process.exitValue());
        }
        return output;
    }
}
