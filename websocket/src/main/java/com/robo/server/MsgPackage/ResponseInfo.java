package com.robo.server.MsgPackage;

/**
 * Created by yoki on 6/28/17.
 */
public class ResponseInfo {

    public  int type;
    public  Object data;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
