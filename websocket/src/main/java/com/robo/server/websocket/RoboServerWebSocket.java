package com.robo.server.websocket;


import com.common.base.response.BaseResponseDto;
import com.common.utils.CommonUtils;
import com.common.utils.GsonUtils;
import com.common.utils.HttpClientUtils;
import com.robo.server.MsgPackage.RoboConnectStatus;
import com.robo.server.model.ClientListModel;
import com.robo.server.model.ClientRequestModel;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


/**
 * Created by jianghaoming on 17/4/13.
 * @description:
 *
 * 1: if  run locally , please comment @component,since conflict between @ServerEndpoint and @Component.
 */

@ServerEndpoint(value = "/server")
@Component
public class RoboServerWebSocket extends WebSocketService{

    private static final Logger _logger = LoggerFactory.getLogger(RoboServerWebSocket.class);

    public static Map<String,Session> sessionMap = new HashMap<>();


    private static Session mySession; //单独session


    public Session getSession(){
        if(mySession != null){
            return mySession;
        }
        return null;
    }

    public static void closeSession(Session session){
        if(session == mySession){
            mySession = null;
        }
    }


    /**
     * 连接建立成功调用的方法*/
    @OnOpen
    public void onOpen(Session session) {
        if(mySession == null){
            mySession = session;
            _logger.info("mysession ==> " + mySession);
        }
        _logger.info("有新连接加入！"+session);
    }


    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose(Session session) {
        _logger.info("有一连接关闭！"+session);

        closeSession(session);

        for(Map.Entry<String ,Session> entry : sessionMap.entrySet()){
            if(session.equals(entry.getValue())){
                sessionMap.remove(entry.getKey());
                List<ClientListModel> clientList = getClientList();
                if(clientList!=null) {
                    Iterator<ClientListModel> iterator = clientList.iterator();
                    while (iterator.hasNext()) {
                        ClientListModel client = iterator.next();
                        if(client.getClientId().equals(entry.getKey())){
                            iterator.remove();
                        }
                    }
                   setClientList(clientList);
                }

            }
        }
        _logger.info("当前在线数为 ："+sessionMap.size());
    }


    /**
     * 发生错误时调用
     * */
    @OnError
    public void onError(Session session, Throwable error) throws IOException {
        closeSession(session);
        _logger.error("发生错误:"+error.getMessage(),error);
    }

    /**
     * 收到客户端消息后调用的方法
     * @param message 客户端发送过来的消息*/
    @OnMessage
    public void onMessage(String message, Session session) throws IOException {
        _logger.info("接收客户端 "+session+" 消息："+message +"");
        BaseResponseDto responseDto = null;
        try {

            if(session != mySession){
                this.sendMessage(failResponse("非法客户端连接"),session);
                return;
            }

            if(CommonUtils.isBlank(message)) {
                this.sendMessage(failResponse("消息内容不能为空"), session);
                return;
            }

            ClientRequestModel clientRequestModel = GsonUtils.convertObj(message, ClientRequestModel.class);
            if(null == clientRequestModel){
                this.sendMessage(failResponse("json内容转换错误"), session);
                return;
            }

            Map<String,Object> dataMap = clientRequestModel.getData();

            if(RoboConnectStatus.INIT_CONNECT.val() == clientRequestModel.getType()){
                responseDto = this.clienConnection(dataMap.get("id")+"",session);
            }

            /*if(message.isEmpty())
                return;
            BaseResponseDto responseDto = msgHandler.handle(session, message);*/


            /*  request.setCode(200);
            request.setData(resultMap);
            request.setMessage("this a server send message");
            request.setSuccess(true);*/

            this.sendMessage(responseDto, session);
            //sendMessage(GsonUtils.toJson(resultMap),session);
            //sessionMap.put("1",session);

           /*/WebSocketMsgModel requestModel = new WebSocketMsgModel();
            requestModel.setMessageId("1");
            requestModel.setMethod("1");
            requestModel.setUserId("1");
            requestModel.setTimeStamp(System.currentTimeMillis()+"");
            this.sendMessageToServer(GsonUtils.toJson(requestModel));*/
        }catch (Exception e){
            _logger.error(e.getMessage(),e);
            this.sendMessage(failResponse("server Exception： "+e.getMessage()),session);
        }

    }


    /**
     * ws客户端连接
     *
     */
    private BaseResponseDto clienConnection(String clientId,Session session){
        List<ClientListModel> list = getClientList();
        ClientListModel client = new ClientListModel();
        client.setClientId(clientId);
        client.setRoboId("");
        list.add(client);
        sessionMap.put(clientId,session);
        setClientList(list);
        _logger.info("当前在线数,"+sessionMap.size());
        return succResponse("客户端连接成功，id = "+clientId);
    }


    //发送消息至webserver
    private String sendMessageToServer(String requestCont) throws Exception {
        StringEntity entity = new StringEntity(requestCont, ContentType.APPLICATION_JSON);
        String result = HttpClientUtils.getMethodPostResponse("http://localhost:8081/webserver/websocket/v1/message",entity);
        return result;
    }


    /**
     * 向客户端发送消息
     * @throws IOException
     */
     public void sendMessage(BaseResponseDto dto, Session session) throws IOException {
         String message = GsonUtils.toJson(dto);
         _logger.info("sendMessage  : " + message);
         session.getBasicRemote().sendText(message);

     }


    public synchronized void subOnlineCount(Session session) {
        String clientId = "";
        for(Map.Entry<String,Session> entry : sessionMap.entrySet()){
            if(entry.getValue().equals(session)){
                clientId = entry.getKey();
                break;
            }
        }
        if(!CommonUtils.isBlank(clientId)) {
            //删除在线状态
            sessionMap.remove(clientId);
            _logger.info(clientId+" 退出成功");
        }
    }





}
