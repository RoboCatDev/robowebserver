package com.robo.server.websocket;

import com.common.base.response.BaseResponseDto;
import com.common.utils.GsonUtils;
import com.robo.server.MsgPackage.DataKillRobot;
import com.robo.server.MsgPackage.DataStartRobot;
import com.robo.server.MsgPackage.ResponseInfo;
import com.robo.server.MsgPackage.RoboConnectStatus;

import javax.websocket.Session;
import java.io.InterruptedIOException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by yoki on 7/2/17.
 */
public class MockOps extends  Thread {

    public Session session;
    @Override
    public void run() {
        TimerTask testTask = new TimerTask() {

            public void run() {
                try {
                  if(session != null) {
                      DataKillRobot  data = new DataKillRobot();
                      data.setVal("id1", "user1");
                      BaseResponseDto responseDto = new BaseResponseDto();
                      responseDto.setCode(0);
                      responseDto.setSuccess(true);
                      responseDto.setMessage("succeed: init connection");
                      ResponseInfo responseInfo = new ResponseInfo();
                      responseInfo.setType(RoboConnectStatus.KICK.val());
                      responseInfo.setData(data);
                      responseDto.setData(responseInfo);
                      String text = GsonUtils.toJson(responseDto);

                      session.getBasicRemote().sendText(text);
                      System.out.println("---complete:" + text);
                  }
                } catch (Exception ex) {
                }
            }
        };

        Timer increaserTimer = new Timer("MyTimer");
        //start a 3 seconds timer 10ms later
        increaserTimer.scheduleAtFixedRate(testTask, 20000, 100);

        while (true) {
            //give it some time to see timer triggering
           try{
               sleep(10);
           } catch (Exception ex) {

           }
        }

    }

}
