package com.robo.server.model;

import com.common.base.BaseDto;

import java.util.Map;

/**
 * Created by jianghaoming on 17/8/9.
 */
public class ClientRequestModel extends BaseDto {

    private Integer type;

    private Map<String,Object> data;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }
}
