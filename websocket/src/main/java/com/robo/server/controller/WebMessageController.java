package com.robo.server.controller;

import com.common.base.BaseController;
import com.common.base.CommConstants;
import com.common.base.exception.BusinessException;
import com.common.base.model.WebSocketMsgModel;
import com.common.base.response.BaseResponseDto;
import com.robo.server.MsgPackage.RoboConnectStatus;
import com.robo.server.model.ClientListModel;
import com.robo.server.model.ClientRequestModel;
import com.robo.server.web.em.cache.CacheWebScoketEnum;
import com.robo.server.web.em.websocket.WebsocketMethodEnum;
import com.robo.server.websocket.RoboServerWebSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.Session;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jianghaoming on 2017/6/13 23:14.
 */

@RestController
@RequestMapping("/api/v1")
public class WebMessageController extends BaseController{


    private static final Logger _logger = LoggerFactory.getLogger(WebMessageController.class);


    private static RoboServerWebSocket roboServerWebSocket;

    private RoboServerWebSocket getRoboServerWebSocket(){
        if(null == roboServerWebSocket){
            roboServerWebSocket = new RoboServerWebSocket();
        }
        return roboServerWebSocket;
    }

    //private Map<String, Session> sessionMap = RoboServerWebSocket.sessionMap;

    private static final String cache_client_list = CacheWebScoketEnum.client_lsit.getValue();

    @PostMapping("/message")
    public ResponseEntity<BaseResponseDto> message(@RequestBody WebSocketMsgModel requestModel){

        try {
            _logger.info("request.log == > "+requestModel.toLogger());


            requestModel.checkFieldNull();
            boolean isSnedMessage = false; //是否需要发送消息

            String messageId = requestModel.getMessageId();
            String method = requestModel.getMethod();
            String timeStamp = requestModel.getTimeStamp();
            String userId = requestModel.getUserId();

            checkParamNull(messageId,"messageId is null");
            checkParamNull(method,"method is null");
            checkParamNull(timeStamp,"timeStamp is null");
            checkParamNull(userId,"userId is null");

            List<ClientListModel> clientList = redis.getList(cache_client_list);
            if(clientList == null){
                clientList = new ArrayList<>();
            }
            String clientId = "";

            BaseResponseDto dto = new BaseResponseDto();
            dto.setSuccess(true);
            dto.setCode(200);

           /* //发送uuid
            if(WebsocketMethodEnum.sendUUId.getValue().equals(method)){
                dto.setMessage("server need uuid");
                String webchatId = requestModel.getDate()+"";
                //从客户端列表中遍历取出还未使用的wsclient
                for(int i=0; i<clientList.size(); i++){
                    ClientListModel client = clientList.get(i);
                   // if(isBlank(client.getWechatId())){
                        client.setWechatId(webchatId);
                        clientId = client.getClientId();
                        clientList.set(i,client);
                        break;
                    //}
                }
                ClientRequestModel sendModel = new ClientRequestModel();
                sendModel.setType(RoboConnectStatus.SENDUUID.val());
                sendModel.setData(new HashMap());
                dto.setData(sendModel);
                isSnedMessage = true;
            }
            //用户登录
            else if(WebsocketMethodEnum.userLogin.getValue().equals(method)){
                dto.setMessage("server login info");
                String token = requestModel.getDate()+"";
                //从客户端列表中遍历取出还未使用的wsclient
                for(int i=0; i<clientList.size(); i++){
                    ClientListModel client = clientList.get(i);
                    //if(isBlank(client.getUserId())){
                        client.setUserId(userId);
                        clientId = client.getClientId();
                        clientList.set(i,client);
                        break;
                    //}
                }
                ClientRequestModel sendModel = new ClientRequestModel();
                sendModel.setType(RoboConnectStatus.LOGIN.val());
                Map<String,Object> data = new HashMap();
                data.put("token",token);
                data.put("userId",userId);
                sendModel.setData(data);
                dto.setData(sendModel);
                isSnedMessage = true;

            }*/

            //获取uuid
            if(WebsocketMethodEnum.sendUUId.getValue().equals(method)){
                dto.setMessage("server need uuid");
                ClientRequestModel sendModel = new ClientRequestModel();
                String robotId = requestModel.getDate()+"";
                sendModel.setType(RoboConnectStatus.SENDUUID.val());
                Map<String,Object> data = new HashMap();
                data.put("robotId",robotId);
                sendModel.setData(data);
                dto.setData(sendModel);
                isSnedMessage = true;
            }
            //用户登录
            else if(WebsocketMethodEnum.userLogin.getValue().equals(method)){
                dto.setMessage("server login info");
                String token = requestModel.getDate()+"";
                ClientRequestModel sendModel = new ClientRequestModel();
                sendModel.setType(RoboConnectStatus.LOGIN.val());
                Map<String,Object> data = new HashMap();
                data.put("token",token);
                data.put("userId",userId);
                sendModel.setData(data);
                dto.setData(sendModel);
                isSnedMessage = true;
            }else{
                dto.setMessage("server send type "+ method);
                ClientRequestModel sendModel = new ClientRequestModel();
                sendModel.setType(RoboConnectStatus.getDesc(method));
                Map<String,Object> data = new HashMap();
                sendModel.setData(data);
                dto.setData(sendModel);
                isSnedMessage = true;
            }

            //redis.set(cache_client_list,clientList);


            Session session = getRoboServerWebSocket().getSession();

            if(isSnedMessage && session != null) {
                /*for (Map.Entry<String, Session> entry : sessionMap.entrySet()) {
                    _logger.info(" = " + entry.getKey() + " = " + entry.getValue());
                    if (entry.getKey().equals(clientId)) {
                        getRoboServerWebSocket().sendMessage(dto, entry.getValue());
                        _logger.info("websocket send message ok， clientId = " + clientId);
                    }
                }*/
                getRoboServerWebSocket().sendMessage(dto,session);
                return succResponse(CommConstants.OPERATOR_SUCC);
            }else{
                return failResponse(CommConstants.OPERATOR_FAIL);
            }


        }catch (BusinessException ex) {
            final String message = ex.getErrorDesc();
            _logger.info(CommConstants.BUSINESS_ERROR + message,ex);
            return failResponse(ex.getErrorCode(),message);
        }catch (DataAccessException ex) {
            final String message = CommConstants.DATA_ERROR;
            _logger.info(message,ex);
            return errorResponse(message);
        }catch (Exception e) {
            final String message = CommConstants.SYSTEM_ERROR;
            _logger.error(message, e);
            return errorResponse(message);
        }
    }

}
