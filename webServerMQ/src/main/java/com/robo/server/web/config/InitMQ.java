package com.robo.server.web.config;


import com.common.base.CommConstants;
import com.common.base.exception.BusinessException;
import com.robo.server.mq.EndPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 初始化mq配置
 */
@Order(value=9)
@Component
public final class InitMQ implements CommandLineRunner {

	private static final Logger _logger = LoggerFactory.getLogger(InitMQ.class);

	@Value("${rabbitmq.host}")
	private String host;

	@Value("${rabbitmq.port}")
	private String port;

	@Value("${rabbitmq.username}")
	private String username;

	@Value("${rabbitmq.password}")
	private String password;


	@Override
	public void run(String... strings) throws BusinessException,Exception {
		_logger.info("init mq config .....");
		try {
			EndPoint.init(host,Integer.parseInt(port),username,password);
		}catch (Exception e) {
			final String message = CommConstants.SYSTEM_ERROR;
			_logger.error(message, e);
		}
	}








}
