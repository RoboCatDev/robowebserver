import com.common.base.BaseRequestModel;
import com.robo.server.mq.EndPoint;
import com.robo.server.mq.MQProducer;
import org.junit.Test;

/**
 * Created by jianghaoming on 17/8/24.
 */
public class MQTest {


    @Test
    public void mq() throws Exception{


        EndPoint.init("127.0.0.1",5672,"robo","robo");

        String endPointName = "topic_robo";
        String clientRoutingKey = "robo.client";

       /* QueueConsumer consumer = new QueueConsumer(endPointName);
        Thread consumerThread = new Thread(consumer);
        consumerThread.start();*/

        MQProducer producer = new MQProducer(endPointName);


        for (int i = 0; i < 10; i++) {
            BaseRequestModel requestModel = new BaseRequestModel();
            requestModel.setUserId("testuserid "+i);
            producer.sendMessage(requestModel,endPointName,clientRoutingKey);
            System.out.println("Message Number "+ i +" sent.");
        }
    }

}
