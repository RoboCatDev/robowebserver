package com.robo.server.web.robo.impl;

import com.common.base.exception.BusinessException;
import com.common.base.model.MyPageResult;
import com.common.utils.CheckUtils;
import com.common.utils.CommonUtils;
import com.robo.server.web.dao.robo.RoboInfoDao;
import com.robo.server.web.em.robo.RoboCurrentStatusEnum;
import com.robo.server.web.em.robo.RoboLoginStatusEnum;
import com.robo.server.web.entity.robo.RoboBasicInfoDto;
import com.robo.server.web.entity.robo.RoboLoginInfoDto;
import com.robo.server.web.entity.robo.response.RoboInfoResponseDto;
import com.robo.server.web.message.RoboMessage;
import com.robo.server.web.message.UserMessage;
import com.robo.server.web.mq.RabbitMQUtils;
import com.robo.server.web.repository.robo.RoboBasicInfoRepository;
import com.robo.server.web.repository.robo.RoboLoginInfoRepository;
import com.robo.server.web.robo.RoboInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by conor on 2017/7/12.
 */
@Service
public class RoboInfoServiceImpl implements RoboInfoService {

    private static final Logger _logger = LoggerFactory.getLogger(RoboInfoServiceImpl.class);

    @Autowired
    private RoboBasicInfoRepository roboBasicInfoRepository;

    @Autowired
    private RoboLoginInfoRepository roboLoginInfoRepository;

    @Autowired
    private RoboInfoDao roboInfoDao;
    
    /**
     * 保存机器人登录/登出信息
     * @param roboLoginInfoDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public RoboLoginInfoDto saveUserLoginInfo(RoboLoginInfoDto roboLoginInfoDto) throws BusinessException,DataAccessException,Exception{

        _logger.info("保存机器人登出信息 personLoginInfoDto===>"+ roboLoginInfoDto.toLogger());

        return roboLoginInfoRepository.save(roboLoginInfoDto);
    }

    /**
     * 根据机器人Id，查询机器人登录/登出信息
     * @param roboId 机器人Id
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public RoboLoginInfoDto getRoboLoginInfoByRoboId(final String roboId) throws BusinessException,DataAccessException,Exception{

        _logger.info("根据机器人Id，查询机器人登出信息 roboId=["+roboId+"]");
        CheckUtils.checkParamNull(roboId,RoboMessage.ROBO_ID);

        RoboLoginInfoDto dataDto = new RoboLoginInfoDto();
        dataDto.setRoboId(roboId);

        return roboLoginInfoRepository.findOne(Example.of(dataDto));
    }

    /**
     * 根据用户Id，查询所有的机器人登录/登出列表
     * @param userId 用户Id
     * @param pageable
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public MyPageResult<RoboInfoResponseDto> getRoboInfoPage(final String userId, final Pageable pageable) throws BusinessException,DataAccessException,Exception{

        _logger.info("根据用户Id，查询所有的机器人登出列表 userId=["+userId+"]");
        CheckUtils.checkParamNull(userId, UserMessage.FieldVALID.USERID);

        return roboInfoDao.getRoboInfoPage(userId,pageable);
    }

    /**
     * 更新/保存机器人基本信息
     * @param roboBasicInfoDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public RoboBasicInfoDto saveUserBasicInfo(RoboBasicInfoDto roboBasicInfoDto) throws BusinessException,DataAccessException,Exception{

        _logger.info("更新/保存机器人基本信息 personBasicInfoDto===>"+ roboBasicInfoDto.toLogger());

        RoboBasicInfoDto dataDto = getRoboBasicInfoByUin(roboBasicInfoDto.getUserId(),roboBasicInfoDto.getUin(),"");
        if(dataDto!=null){
            if(!dataDto.getRoboId().equals(roboBasicInfoDto.getRoboId())) {
                //当uin不为空时，kill掉之前的robotId进程，并保存最新的robotId
                RabbitMQUtils.closeRobot(dataDto.getRoboId());
            }
            roboBasicInfoDto.setId(dataDto.getId());
            roboBasicInfoDto.setCreateTime(dataDto.getCreateTime());
        }

        if(roboBasicInfoDto!=null&&roboBasicInfoDto.getCurrentStatus().equals(RoboCurrentStatusEnum.login_ing.getValue())){
            //将本微信其他账号下的机器人登录置为异常退出
            RoboBasicInfoDto tempDto = new RoboBasicInfoDto();
            tempDto.setUin(roboBasicInfoDto.getUin());
            tempDto.setCurrentStatus(RoboCurrentStatusEnum.login_ing.getValue());
            List<RoboBasicInfoDto> dataList = roboBasicInfoRepository.findAll(Example.of(tempDto));
            if(dataList!=null&&dataList.size()>0){
                for(RoboBasicInfoDto dto:dataList){
                    int count = updateRoboCurrentStatusByRoboId(dto.getRoboId(),RoboCurrentStatusEnum.loginout_exception.getValue(),"");
                    if(count>0){
                        //登出
                        RoboLoginInfoDto roboLoginInfoDto = new RoboLoginInfoDto();
                        roboLoginInfoDto.setRoboId(dto.getRoboId());
                        roboLoginInfoDto.setUserId(dto.getUserId());
                        roboLoginInfoDto.setLoginStatus(RoboLoginStatusEnum.loginout.getValue());
                        roboLoginInfoDto.setDeviceType("");
                        saveUserLoginInfo(roboLoginInfoDto);
                    }
                    //通知python kill 进程
                    RabbitMQUtils.closeRobot(dto.getRoboId());
                }
            }
        }

        return roboBasicInfoRepository.save(roboBasicInfoDto);

    }

    /**
     * 根据机器人Id，查询机器人基本信息
     * @param roboId 机器人Id
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public RoboBasicInfoDto getRoboBasicInfo(final String roboId) throws BusinessException,DataAccessException,Exception{

        _logger.info("根据机器人Id，查询机器人基本信息 roboId=["+roboId+"]");

        CheckUtils.checkParamNull(roboId, RoboMessage.ROBO_ID);

        RoboBasicInfoDto dataDto = new RoboBasicInfoDto();
        dataDto.setRoboId(roboId);
        dataDto.setCurrentStatus(RoboCurrentStatusEnum.login_ing.getValue());

        return roboBasicInfoRepository.findOne(Example.of(dataDto));

    }

    public RoboBasicInfoDto getRoboBasicInfoNoStatus(final String roboId) throws BusinessException,DataAccessException,Exception{

        _logger.info("根据机器人Id，查询机器人基本信息 roboId=["+roboId+"]");

        CheckUtils.checkParamNull(roboId, RoboMessage.ROBO_ID);

        RoboBasicInfoDto dataDto = new RoboBasicInfoDto();
        dataDto.setRoboId(roboId);

        return roboBasicInfoRepository.findOne(Example.of(dataDto));

    }

    /**
     * 根据微信uin，查询机器人基本信息
     * @param userId 用户Id
     * @param uin 微信uin
     * @param status 当前状态 0: 未登录 1: 正常登录中 2：正常退出 3：异常退出 9:取消追踪 为空时查询所有
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public RoboBasicInfoDto getRoboBasicInfoByUin(final String userId,final String uin,final String status) throws BusinessException,DataAccessException,Exception{

        _logger.info("根据机器人uin，查询机器人基本信息 userId=["+userId+"] uin=["+uin+"]");
        CheckUtils.checkParamNull(userId, UserMessage.FieldVALID.USERID);
        CheckUtils.checkParamNull(uin, RoboMessage.UIN);

        RoboBasicInfoDto dataDto = new RoboBasicInfoDto();
        dataDto.setUserId(userId);
        dataDto.setUin(uin);
        if(!CommonUtils.isBlank(status)){
            dataDto.setCurrentStatus(status);
        }

        return roboBasicInfoRepository.findOne(Example.of(dataDto));
    }

    /**
     * 根据用户Id,查询所有微信机器人信息(不包括取消追踪的机器人)
     * @param userId 用户ID
     * @param currentStatus 当前状态 0: 未登录 1: 正常登录中 2：正常退出 3：异常退出 9:取消追踪  可为空，为空时，查询所有有效的机器人
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public List<RoboBasicInfoDto> getRoboBasicInfoList(final String userId,final String currentStatus) throws BusinessException,DataAccessException,Exception{

        _logger.info("根据用户Id,查询所有微信机器人信息 userId=["+userId+"]");
        CheckUtils.checkParamNull(userId,UserMessage.FieldVALID.USERID);

        if(CommonUtils.isBlank(currentStatus)){
            return roboBasicInfoRepository.getRoboBasicInfoList(userId);
        }else{
            RoboBasicInfoDto dataDto = new RoboBasicInfoDto();
            dataDto.setUserId(userId);
            dataDto.setCurrentStatus(currentStatus);
            return roboBasicInfoRepository.findAll(Example.of(dataDto),new Sort(Sort.Direction.ASC,"updateTime"));
        }

    }

    /**
     * 根据roboId，更新机器人当前状态
     * @param roboId 机器人id
     * @param currentStatus 当前状态 0: 未登录 1: 正常登录中 2：正常退出 3：异常退出 9:取消追踪
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public int updateRoboCurrentStatusByRoboId(String roboId, String currentStatus, String memo) throws BusinessException,DataAccessException,Exception{

        _logger.info("根据roboId，更新机器人当前状态 roboId=["+roboId+"] currentStatus=["+currentStatus+"]");
        CheckUtils.checkParamNull(roboId,UserMessage.FieldVALID.USERID);
        if(!RoboCurrentStatusEnum.getAllMap().containsKey(currentStatus)){
            throw new BusinessException(RoboMessage.ROBO_CURRENT_STATUS_NOT_EXIST);
        }
        return roboBasicInfoRepository.updateRoboCurrentStatusByRoboId(roboId,memo,currentStatus);

    }


    /**
     * 获取在线机器人列表
     * @return
     * @throws DataAccessException
     * @throws BusinessException
     * @throws Exception
     */
    public List<RoboBasicInfoDto> getListByOnlie() throws DataAccessException, BusinessException, Exception{
        return roboBasicInfoRepository.getPidListByStatus();
    }


}
