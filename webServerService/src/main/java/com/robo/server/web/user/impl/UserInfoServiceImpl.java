package com.robo.server.web.user.impl;

import com.common.base.exception.BusinessException;
import com.common.utils.CheckUtils;
import com.robo.server.web.entity.user.UserBasicInfoDto;
import com.robo.server.web.entity.user.UserLoginInfoDto;
import com.robo.server.web.message.UserMessage;
import com.robo.server.web.repository.user.UserBasicInfoRepository;
import com.robo.server.web.repository.user.UserLoginInfoRepository;
import com.robo.server.web.user.UserInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

/**
 * Created by conor on 2017/7/12.
 */
@Service
public class UserInfoServiceImpl implements UserInfoService{

    private static final Logger _logger = LoggerFactory.getLogger(UserInfoServiceImpl.class);

    @Autowired
    private UserLoginInfoRepository userLoginInfoRepository;

    @Autowired
    private UserBasicInfoRepository userBasicInfoRepository;

    /**
     * 保存用户信息
     * @param userLoginInfoDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public UserLoginInfoDto saveUserLoginInfo(UserLoginInfoDto userLoginInfoDto) throws BusinessException,DataAccessException,Exception{

        _logger.info("userLoginInfoDto===>"+userLoginInfoDto.toLogger());

        return userLoginInfoRepository.save(userLoginInfoDto);
    }

    /**
     * 根据用户Id，查询用户信息
     * @param userId 用户Id
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public UserLoginInfoDto getUserLoginInfoByUserId(final String userId) throws BusinessException,DataAccessException,Exception{

        _logger.info("userId=["+userId+"]");

        CheckUtils.checkParamNull(userId, UserMessage.FieldVALID.USERID);

        return userLoginInfoRepository.findOne(userId);
    }


    /**
     * 根据手机号码，查询用户信息
     * @param mobile 手机号码
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public UserLoginInfoDto getUserLoginInfoByMobile(final String mobile) throws BusinessException,DataAccessException,Exception{

        _logger.info("mobile=["+mobile+"]");

        CheckUtils.checkMoible(mobile);

        return userLoginInfoRepository.findOneByMobile(mobile);

    }


    /**
     * 根据用户ID，更新用户密码
     * @param userId 用户Id
     * @param password 用户密码
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public int updatePasswordByUserId(final String userId,final String password) throws BusinessException,DataAccessException,Exception{

        _logger.info("userId=["+userId+"] password=["+password+"]");

        CheckUtils.checkParamNull(userId,UserMessage.FieldVALID.USERID);
        CheckUtils.checkParamNull(password,UserMessage.FieldVALID.PASSWORD);

        return userLoginInfoRepository.updatePasswordByUserId(userId,password);

    }

    public int updateMobileByUserId(final String userId, final String mobile) throws BusinessException,DataAccessException,Exception{
        _logger.info("userId=["+userId+"] mobile=["+mobile+"]");
        CheckUtils.checkParamNull(userId,UserMessage.FieldVALID.USERID);
        CheckUtils.checkParamNull(mobile,UserMessage.FieldVALID.MOBILE);

        UserLoginInfoDto user = this.getUserLoginInfoByUserId(userId);
        user.setMobile(mobile);
        userLoginInfoRepository.saveAndFlush(user);
        return 1;
    }



    /**
     * 保存用户基本信息
     *
     * @param userBasicInfoDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public UserBasicInfoDto saveUserBasicInfo(UserBasicInfoDto userBasicInfoDto) throws BusinessException, DataAccessException, Exception{

        _logger.info("userBasicInfoDto===>"+userBasicInfoDto.toLogger());

        return userBasicInfoRepository.save(userBasicInfoDto);
    }


    /**
     * 保存用户基本信息
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public UserBasicInfoDto getUserBasicInfoById(final String userId) throws BusinessException, DataAccessException, Exception{
        _logger.info("userId = ["+userId+"]");

        return userBasicInfoRepository.findByUserId(userId);
    }

}
