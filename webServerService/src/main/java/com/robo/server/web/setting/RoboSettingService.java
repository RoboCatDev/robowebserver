package com.robo.server.web.setting;

import com.common.base.exception.BusinessException;
import com.robo.server.web.entity.setting.RoboChatDictDto;
import com.robo.server.web.entity.setting.RoboSettingContentDto;
import com.robo.server.web.entity.setting.RoboSwitchStatsticsDto;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by conor on 2017/8/15.
 */
@Service
public interface RoboSettingService {

    /**
     * 更新/保存机器人相关操作开关状态及开关切换次数统计
     * @param roboSwitchStatsticsDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public RoboSwitchStatsticsDto updateRoboFuncStatstics(RoboSwitchStatsticsDto roboSwitchStatsticsDto) throws BusinessException,DataAccessException,Exception;

    /**
     * 查询机器人相关操作开关状态及开关切换次数
     * @param userId 用户Id
     * @param uin 微信uin
     * @param switchType 开关类型 0：切换 1：自动回复 2：机器人当前状态 4：踢人 5：欢迎 6：规定 7：数据分析 8：聊天匹配 9：聊天模糊匹配
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public RoboSwitchStatsticsDto getRoboFuncStatstics(final String userId,final String uin,final String switchType) throws BusinessException,DataAccessException,Exception;

    /**
     * 查询所有的机器人开关配置信息
     * @param userId 用户ID
     * @param uin 微信uin
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public List<RoboSwitchStatsticsDto> getAllRoboSwitchStaticsList(final String userId,final String uin) throws BusinessException,DataAccessException,Exception;

    /**
     * 新增保存机器人聊天回复自动匹配信息
     * @param roboChatDictDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public RoboChatDictDto saveRoboChatDict(RoboChatDictDto roboChatDictDto) throws BusinessException,DataAccessException,Exception;

    /**
     * 查询机器人聊天回复自动匹配信息列表
     * @param userId 用户ID
     * @param uin 微信uin
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public List<RoboChatDictDto> getRoboChatDictList(final String userId, final String uin) throws BusinessException,DataAccessException,Exception;

    /**
     * 更新机器人聊天回复自动匹配信息
     * @param id 聊天匹配ID
     * @param chatKey 聊天内容
     * @param chatValue 聊天匹配内容
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public int updateRoboChatDictById(final String id, final String chatKey,final String chatValue) throws BusinessException,DataAccessException,Exception;

    /**
     * 根据id查询
     * @param id
     * @return
     */
    public RoboChatDictDto getRoboChatDictById(final String id) throws BusinessException,DataAccessException,Exception;

    /**
     * 删除机器人聊天回复自动匹配信息
     * @param id 聊天匹配ID
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public void deleteRoboChatDictById(final String id) throws BusinessException,DataAccessException,Exception;

    /**
     * 新增保存机器人设置信息（例如欢迎语，规定等）
     * @param roboSettingContentDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public RoboSettingContentDto saveRoboSettingContent(RoboSettingContentDto roboSettingContentDto) throws BusinessException,DataAccessException,Exception;

    /**
     * 查询保存机器人设置提示语信息列表（例如欢迎语，规定等）
     * @param userId 用户ID
     * @param uin 微信uin
     * @param type 类型 0：踢人 1：欢迎 2：规定 98:切换 99：图灵机器人Key
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public List<RoboSettingContentDto> getRoboSettingContentList(final String userId,final String uin,final String type) throws BusinessException,DataAccessException,Exception;

    /**
     * 查询保存机器人设置提示语信息列表（例如欢迎语，规定等）
     * @param userId 用户ID
     * @param uin 微信uin
     * @param status 状态 0：待启用 1：启用  为空时，查询所有
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public List<RoboSettingContentDto> getAllRoboSettingContentList(final String userId,final String uin,final String status) throws BusinessException,DataAccessException,Exception;

    /**
     * 根据ID，更新机器人提示语设置启用状态
     * @param id 设置ID
     * @param status 状态 0：待启用 1：启用
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public int updateRoboSettingContentStatusById(final String id,final String status) throws BusinessException,DataAccessException,Exception;


    public RoboSettingContentDto getRoboSettingContentById(final String id) throws BusinessException,DataAccessException,Exception;

    /**
     * 根据ID，删除机器人提示语设置信息
     * @param id 机器人设置ID
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public void deleteRoboSettingContentById(final String id) throws BusinessException,DataAccessException,Exception;

    /**
     * 根据ID，删除机器人提示语设置信息
     * @param id 机器人设置ID
     * @param content 提示语内容
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public int updateRoboSettingContentById(final String id,final String content) throws BusinessException,DataAccessException,Exception;

}


