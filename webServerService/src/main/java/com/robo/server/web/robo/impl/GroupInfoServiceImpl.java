package com.robo.server.web.robo.impl;

import com.common.base.CommConstants;
import com.common.base.exception.BusinessException;
import com.common.utils.CheckUtils;
import com.common.utils.CommonUtils;
import com.common.utils.DateFormatUtil;
import com.robo.server.web.em.robo.GroupAuthorizeStatusEnum;
import com.robo.server.web.entity.robo.group.GroupBasicInfoDto;
import com.robo.server.web.entity.robo.group.GroupChatContentDto;
import com.robo.server.web.entity.robo.group.GroupPersonChangeDto;
import com.robo.server.web.entity.robo.group.GroupPersonInfoDto;
import com.robo.server.web.message.RoboMessage;
import com.robo.server.web.message.UserMessage;
import com.robo.server.web.repository.robo.group.GroupBasicInfoRepository;
import com.robo.server.web.repository.robo.group.GroupChatContentRepository;
import com.robo.server.web.repository.robo.group.GroupPersonChangeRepository;
import com.robo.server.web.repository.robo.group.GroupPersonInfoRepository;
import com.robo.server.web.robo.GroupInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by conor on 2017/7/12.
 */
@Service
public class GroupInfoServiceImpl implements GroupInfoService {

    private static final Logger _logger = LoggerFactory.getLogger(GroupInfoServiceImpl.class);

    @Autowired
    private GroupBasicInfoRepository groupBasicInfoRepository;

    @Autowired
    private GroupPersonInfoRepository groupPersonInfoRepository;

    @Autowired
    private GroupChatContentRepository groupChatContentRepository;

    @Autowired
    private GroupPersonChangeRepository groupPersonChangeRepository;

    /**
     * 保存群组基本信息
     * @param groupBasicInfoDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public GroupBasicInfoDto saveGroupBasicInfo(GroupBasicInfoDto groupBasicInfoDto) throws BusinessException,DataAccessException,Exception{

        if(groupBasicInfoDto!=null&&CommonUtils.isBlank(groupBasicInfoDto.getAuthorizeStatus())){
            groupBasicInfoDto.setAuthorizeStatus(GroupAuthorizeStatusEnum.NO.getValue());
        }

        _logger.info("保存群组基本信息 groupBasicInfoDto===>"+ groupBasicInfoDto.toLogger());

        return groupBasicInfoRepository.save(groupBasicInfoDto);
    }

    /**
     * 保存群组基本信息(批量)
     * @param groupBasicInfoDtoList
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public List<GroupBasicInfoDto> saveGroupBasicInfo(List<GroupBasicInfoDto> groupBasicInfoDtoList) throws BusinessException,DataAccessException,Exception{

        if(groupBasicInfoDtoList == null){
            throw new BusinessException(CommConstants.NOT_NULL);
        }

        _logger.info("保存群组基本信息(批量) groupBasicInfoDtoList.size===>"+ groupBasicInfoDtoList.size());

        return groupBasicInfoRepository.save(groupBasicInfoDtoList);
    }

    /**
     * 删除群组基本信息
     * @param userId 用户Id
     * @param uin 微信uin
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public void deleteGroupBasicInfoByUin(final String userId,final String uin) throws BusinessException,DataAccessException,Exception{

        _logger.info("根据微信uin,删除群组基本信息 userId=["+userId+"] uin=["+uin+"]");
        CheckUtils.checkParamNull(userId,UserMessage.FieldVALID.USERID);
        CheckUtils.checkParamNull(uin,RoboMessage.UIN);

        groupBasicInfoRepository.deleteGroupBasicInfoByUin(userId,uin);
    }

    /** 删除群组基本信息
     * @param id
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public void deleteGroupBasicInfoById(final String id) throws BusinessException,DataAccessException,Exception{

        _logger.info("根据id,删除群组基本信息 id=["+id+"]");
        CheckUtils.checkParamNull(id,RoboMessage.ID);

        groupBasicInfoRepository.delete(id);
    }

    public void deleteGroupBasicInfoByUserId(final String userId) throws BusinessException,DataAccessException,Exception{
        _logger.info("删除群组基本信息 userid=["+userId+"]");
        CheckUtils.checkParamNull(userId,UserMessage.FieldVALID.USERID);
        GroupBasicInfoDto dto = new GroupBasicInfoDto();
        dto.setUserId(userId);
        groupBasicInfoRepository.delete(dto);
    }


    /**
     * 根据群组ID，更新群组的授权状态
     * @param groupId 群组ID
     * @param authorizeStatus 授权状态 0：未授权 1：已授权
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public int updateGroupBasicInfoStatus(final String groupId,final String authorizeStatus) throws BusinessException,DataAccessException,Exception{

        _logger.info("根据群组ID，更新群组的授权状态 groupId=["+groupId+"] authorizeStatus=["+authorizeStatus+"]");
        CheckUtils.checkParamNull(groupId,RoboMessage.GROUP_ID);
        if(!GroupAuthorizeStatusEnum.getAllMap().containsKey(authorizeStatus)){
            throw new BusinessException(RoboMessage.authorizeStatus_not_exist);
        }

        GroupBasicInfoDto dto = getGroupBasicInfoByGroupId(groupId);
        String authorizeTime = "";
        if(GroupAuthorizeStatusEnum.YES.getValue().equals(authorizeStatus)){
            if(dto!=null&&!CommonUtils.isBlank(dto.getAuthorizeTime())){
                authorizeTime = dto.getAuthorizeTime();
            }else{
                authorizeTime = DateFormatUtil.getCurrentDate();
            }
        }

        return groupBasicInfoRepository.updateGroupBasicInfoStatus(groupId,authorizeStatus,authorizeTime);

    }

    /**
     * 根据群组Id，查询群组基本信息
     * @param groupId 群组Id
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public GroupBasicInfoDto getGroupBasicInfoByGroupId(final String groupId) throws BusinessException,DataAccessException,Exception{

        _logger.info("根据群组Id，查询群组基本信息 groupId=["+groupId+"]");
        CheckUtils.checkParamNull(groupId, RoboMessage.GROUP_ID);

        GroupBasicInfoDto dataDto = new GroupBasicInfoDto();
        dataDto.setGroupId(groupId);

        return groupBasicInfoRepository.findOne(Example.of(dataDto));
    }

    /**
     * 根据userId和groupName，查询群组基本信息
     * @param userId 用户Id
     * @param groupName 群组名称
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public GroupBasicInfoDto getGroupBasicInfoByUserIdAndName(final String userId,final String groupName) throws BusinessException,DataAccessException,Exception{
        _logger.info("根据userId和groupName，查询群组基本信息 userId=["+userId+"] groupName=["+groupName+"]");
        CheckUtils.checkParamNull(userId, UserMessage.FieldVALID.USERID);

        GroupBasicInfoDto dataDto = new GroupBasicInfoDto();
        dataDto.setUserId(userId);
        dataDto.setNickName(groupName);

        GroupBasicInfoDto resultDto = new GroupBasicInfoDto();

        List<GroupBasicInfoDto> list = groupBasicInfoRepository.findAll(Example.of(dataDto));
        if(list!=null&&list.size()>0){
            resultDto = list.get(0);
        }

        return resultDto;
    }

    /**
     * 根据微信uin，查询群组基本信息列表
     * @param userId 用户Id
     * @param uin 微信uin
     * @param keyWord 查询条件，例如群组昵称，可为空
     * @param pageable
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public Page<GroupBasicInfoDto> getGroupBasicInfoPage(final String userId,final String uin, final String keyWord, final Pageable pageable) throws BusinessException,DataAccessException,Exception{

        _logger.info("根据微信uin，查询群组基本信息列表 userId=["+userId+"] uin=["+uin+"] keyWord=["+keyWord+"]");
        CheckUtils.checkParamNull(uin, RoboMessage.UIN);

        GroupBasicInfoDto dataDto = new GroupBasicInfoDto();
        dataDto.setUserId(userId);
        dataDto.setUin(uin);
        if(!CommonUtils.isBlank(keyWord)){
            //keyWord查询条件存在
            return groupBasicInfoRepository.findAllByKeyWordPageable(userId,uin,keyWord,new PageRequest(pageable.getPageNumber(),pageable.getPageSize(),new Sort(Sort.Direction.DESC,"create_time")));
//            return groupBasicInfoRepository.findAllByUinAndNickNameLike(uin,keyWord,pageable);
        }else{
           //keyWord查询条件不存在
            return groupBasicInfoRepository.findAll(Example.of(dataDto),pageable);
        }
    }

    /**
     * 根据微信uin和授权状态，查询符合约束条件的群组列表（不分页）
     * @param userId 用户ID
     * @param uin 微信uin
     * @param keyWord 查询条件，例如群组昵称，可为空
     * @param authorizeStatus 授权状态 0：未授权 1：已授权；可为空，为空时，查询所有授权状态
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public List<GroupBasicInfoDto> getAuthorizedGroupBasicInfoList(final String userId,final String uin,final String keyWord,final String authorizeStatus,final String isOwner) throws BusinessException,DataAccessException,Exception{

        _logger.info("根据微信uin和授权状态，查询符合约束条件的群组列表 userId=["+userId+"] uin=["+uin+"] keyWord=["+keyWord+"] authorizeStatus=["+authorizeStatus+"]");
        CheckUtils.checkParamNull(uin, RoboMessage.UIN);

        GroupBasicInfoDto dataDto = new GroupBasicInfoDto();
        dataDto.setUserId(userId);
        dataDto.setUin(uin);
        if(!CommonUtils.isBlank(authorizeStatus)){
            dataDto.setAuthorizeStatus(authorizeStatus);
        }
        if(!CommonUtils.isBlank(isOwner)){
            dataDto.setIsOwner(isOwner);
        }

        if(!CommonUtils.isBlank(keyWord)){
            //keyWord查询条件存在
            if(!CommonUtils.isBlank(authorizeStatus) && CommonUtils.isBlank(isOwner)){
                return groupBasicInfoRepository.findAllByKeyWordAndAuthorizeStatus(userId,uin,keyWord,authorizeStatus);
            }else if(!CommonUtils.isBlank(authorizeStatus) && !CommonUtils.isBlank(isOwner)){
                return groupBasicInfoRepository.findAllByKeyWordAndOwnerAndAuthorizeStatus(userId,uin,keyWord,authorizeStatus,isOwner);
            }else if( CommonUtils.isBlank(authorizeStatus) && !CommonUtils.isBlank(isOwner)){
                return groupBasicInfoRepository.findAllByKeyWordAndOwner(userId,uin,keyWord,isOwner);
            }else{
                return groupBasicInfoRepository.findAllByKeyWord(userId,uin,keyWord);
            }
        }else{
            //keyWord查询条件不存在
            return groupBasicInfoRepository.findAll(Example.of(dataDto),new Sort(Sort.Direction.DESC,"createTime"));
        }

    }

    /**
     * 保存群组用户信息
     * @param groupPersonInfoDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public GroupPersonInfoDto saveGroupPersonInfo(GroupPersonInfoDto groupPersonInfoDto) throws BusinessException,DataAccessException,Exception{

        _logger.info("保存群组用户信息 groupPersonInfoDto===>"+ groupPersonInfoDto.toLogger());
        return groupPersonInfoRepository.save(groupPersonInfoDto);

    }


    /**
     * 保存群组用户信息(批量)
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public List<GroupPersonInfoDto> saveGroupPersonInfo(List<GroupPersonInfoDto> groupPersonInfoDtoList) throws BusinessException,DataAccessException,Exception{
        if(groupPersonInfoDtoList == null){
            throw new BusinessException(CommConstants.NOT_NULL);
        }
        _logger.info("批量保存群组信息 groupPersonInfoDtoList.size = " +groupPersonInfoDtoList.size());
        return groupPersonInfoRepository.save(groupPersonInfoDtoList);
    }

    /**
     * 根据群组用户Id，查询群组用户信息
     * @param groupPersonId 群组用户Id
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public GroupPersonInfoDto getGroupPersonInfoByGroupPersonId(final String groupPersonId) throws BusinessException,DataAccessException,Exception{

        _logger.info("根据群组用户Id，查询群组用户信息 groupPersonId=["+groupPersonId+"]");
        CheckUtils.checkParamNull(groupPersonId,RoboMessage.GROUP_PERSON_ID);

        return groupPersonInfoRepository.findOne(groupPersonId);

    }

    /**
     * 根据群组Id，查询群组成员列表（不分页）
     * @param groupId 群组Id
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public List<GroupPersonInfoDto> getGroupPersonInfoList(final String groupId) throws BusinessException,DataAccessException,Exception{

        _logger.info("根据群组Id，查询群组成员列表（不分页） groupId=["+groupId+"]");

        CheckUtils.checkParamNull(groupId, RoboMessage.GROUP_ID);

        GroupPersonInfoDto dataDto = new GroupPersonInfoDto();
        dataDto.setGroupId(groupId);

        return groupPersonInfoRepository.findAll(Example.of(dataDto));

    }

    /**
     * 根据群组Id，查询群组成员列表
     * @param groupId 群组Id
     * @param pageable
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public Page<GroupPersonInfoDto> getGroupPersonInfoPage(final String groupId,final Pageable pageable) throws BusinessException,DataAccessException,Exception{

        _logger.info("根据群组Id，查询群组成员列表（分页） groupId=["+groupId+"]");
        CheckUtils.checkParamNull(groupId, RoboMessage.GROUP_ID);

        GroupPersonInfoDto dataDto = new GroupPersonInfoDto();
        dataDto.setGroupId(groupId);

        return groupPersonInfoRepository.findAll(Example.of(dataDto),pageable);

    }

    /**
     * 根据群组id，删除群组成员信息
     * @param groupId
     * @throws DataAccessException
     * @throws BusinessException
     * @throws Exception
     */
    public void deleteGroupPersonInfoByGroupId(final String groupId) throws DataAccessException,BusinessException,Exception{

        _logger.info("根据群组id，删除群组成员信息 groupId=["+groupId+"]");
        CheckUtils.checkParamNull(groupId, RoboMessage.GROUP_ID);

        groupPersonInfoRepository.deleteGroupPersonInfoByGroupId(groupId);
    }


    public void deleteGroupPersonInfoByUserId(final String userId) throws DataAccessException,BusinessException,Exception{
        _logger.info("根据用户id，删除群组成员信息 userId=["+userId+"]");
        CheckUtils.checkParamNull(userId, UserMessage.FieldVALID.USERID);
        groupPersonInfoRepository.deleteGroupPersonInfoByUserId(userId);
    }


    /**
     * 根据群组id，删除群组成员信息
     * @param personId 群组成员ID
     * @throws DataAccessException
     * @throws BusinessException
     * @throws Exception
     */
    public void deleteGroupPersonInfoByPersonId(final String personId) throws DataAccessException,BusinessException,Exception{

        _logger.info("根据群组成员id，删除群组成员信息 personId=["+personId+"]");
        CheckUtils.checkParamNull(personId, RoboMessage.GROUP_PERSON_ID);

        groupPersonInfoRepository.delete(personId);
    }

    /**
     * 保存群组聊天信息
     * @param groupChatContentDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public GroupChatContentDto saveGroupChatContent(GroupChatContentDto groupChatContentDto) throws BusinessException,DataAccessException,Exception{

        _logger.info("保存群组聊天信息 groupChatContentDto===>"+ groupChatContentDto.toLogger());

        return groupChatContentRepository.save(groupChatContentDto);

    }

    /**
     * 根据聊天Id，查询群组聊天信息
     * @param chatId 群组聊天Id
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public GroupChatContentDto getGroupChatContentByChatId(final String chatId) throws BusinessException,DataAccessException,Exception{

        _logger.info("根据聊天Id，查询群组聊天信息 chatId=["+chatId+"]");
        CheckUtils.checkParamNull(chatId,RoboMessage.CHAT_ID);

        return groupChatContentRepository.findOne(chatId);
    }

    /**
     * 根据群组Id，查询群组聊天信息列表
     * @param groupId 群组Id
     * @param pageable
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public Page<GroupChatContentDto> getGroupChatContentPage(final String groupId,final Pageable pageable) throws BusinessException,DataAccessException,Exception{

        _logger.info("根据群组Id，查询群组聊天信息列表 groupId=["+groupId+"]");
        CheckUtils.checkParamNull(groupId, RoboMessage.GROUP_ID);

        GroupChatContentDto dataDto = new GroupChatContentDto();
        dataDto.setGroupId(groupId);

        return groupChatContentRepository.findAll(Example.of(dataDto),pageable);

    }

    /**
     * 保存群组成员变更信息
     * @param groupPersonChangeDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public GroupPersonChangeDto saveGroupPersonChange(GroupPersonChangeDto groupPersonChangeDto) throws BusinessException,DataAccessException,Exception{

        _logger.info("groupPersonChangeDto===>"+groupPersonChangeDto.toLogger());

        return groupPersonChangeRepository.save(groupPersonChangeDto);

    }

    /**
     * 根据群组名称和成员名称，查询群组成员信息列表
     * @param userId 用户Id
     * @param groupName 群组名称
     * @param personName 成员名称
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public List<GroupPersonInfoDto> getGroupPersonInfoListByName(final String userId,final String groupName, final String personName) throws BusinessException,DataAccessException,Exception{

        _logger.info("userId=["+userId+"] groupName=["+groupName+"] personName=["+personName+"]");

        GroupPersonInfoDto dataDto = new GroupPersonInfoDto();
        dataDto.setUserId(userId);
        dataDto.setGroupName(groupName);
        dataDto.setPersonNickName(personName);

        return groupPersonInfoRepository.findAll(Example.of(dataDto));

    }



}
