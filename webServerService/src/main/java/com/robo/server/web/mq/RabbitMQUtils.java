package com.robo.server.web.mq;

import com.common.base.exception.BusinessException;
import com.robo.server.mq.MQProducer;
import com.robo.server.web.em.mq.MqRobotCommond;
import com.robo.server.web.mq.model.MqRobotRequestModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

/**
 * rabbitMqUtils
 * Created by jianghaoming on 17/8/24.
 */
public class RabbitMQUtils {


    private static final Logger _logger = LoggerFactory.getLogger(RabbitMQUtils.class);

    private static MQProducer mqProducer = null;

    private static final String exchageName = "topic_robo";

    private static final String primaryRoutingKey = "robo.host";
    private static final String clientRoutingKey = "robo.client";


    private static MQProducer getMQ() {
        if (null == mqProducer) {
            try {
                mqProducer = new MQProducer(exchageName);
            } catch (BusinessException e) {
               _logger.error(e.getErrorDesc(),e);
            } catch (TimeoutException e) {
                _logger.error(e.getMessage(),e);
            } catch (IOException e) {
                _logger.error(e.getMessage(),e);
            }
        }
        return mqProducer;
    }

    static {
        getMQ();
    }


    /**
     * 启动机器人
     */
    public static void startRobot(final String roboId,final String uin) throws BusinessException, TimeoutException, IOException {
        _logger.info("startRobot ==> roboId : " + roboId);
        MqRobotRequestModel requestModel = new MqRobotRequestModel();
        requestModel.setRobotId(roboId);
        requestModel.setUin(uin);
        requestModel.setCommand(MqRobotCommond.START.val());
        mqProducer.sendMessage(requestModel,exchageName,primaryRoutingKey);
    }

    /**
     * 关闭机器人
     */
    public static void closeRobot(final String roboId) throws BusinessException, TimeoutException, IOException {
        _logger.info("closeRobot ==> roboId : " + roboId);
        MqRobotRequestModel requestModel = new MqRobotRequestModel();
        requestModel.setRobotId(roboId);
        requestModel.setCommand(MqRobotCommond.QUIT.val());
        mqProducer.sendMessage(requestModel,exchageName,primaryRoutingKey);
    }

    /**
     * 重启机器人
     */
    public static void reloadRobot(final String roboId) throws BusinessException, TimeoutException, IOException {
        _logger.info("reloadRobot ==> roboId : " + roboId);
        MqRobotRequestModel requestModel = new MqRobotRequestModel();
        requestModel.setRobotId(roboId);
        requestModel.setCommand(MqRobotCommond.RELOAD.val());
        mqProducer.sendMessage(requestModel,exchageName,primaryRoutingKey);
    }



    //＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊  具体操作命令 ＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

    /**
     * 更新机器人的token 和 userId
     * @param roboId
     */
    public static void updateToken(String roboId, String userId, String token) throws BusinessException, TimeoutException, IOException {
        _logger.info("bingdUin ==> roboId : " + roboId);
        MqRobotRequestModel requestModel = new MqRobotRequestModel();
        requestModel.setRobotId(roboId);
        Map<String,Object> data = new HashMap<>();
        data.put("userId",userId);
        data.put("token",token);
        requestModel.setData(data);
        requestModel.setCommand(MqRobotCommond.BINGDING.val());
        mqProducer.sendMessage(requestModel,exchageName,clientRoutingKey);
    }



    /**
     * 更新配置
     */
    public static void updateConfig(String roboId) throws BusinessException, TimeoutException, IOException {
        _logger.info("updateConfig ==> roboId : " + roboId);
        MqRobotRequestModel requestModel = new MqRobotRequestModel();
        requestModel.setRobotId(roboId);
        requestModel.setCommand(MqRobotCommond.UPDATE_CONFIG.val());
        mqProducer.sendMessage(requestModel,exchageName,clientRoutingKey);
    }

    /**
     * 踢人操作
     */
    public static void tiRenOperate(String roboId,String groupName,List<String> personNameList) throws BusinessException, TimeoutException, IOException {
        _logger.info("tiRenOperate ==> roboId : " + roboId);
        MqRobotRequestModel requestModel = new MqRobotRequestModel();
        requestModel.setRobotId(roboId);
        Map<String,Object> data = new HashMap<>();
        data.put("groupName",groupName);
        data.put("personNameList",personNameList);
        requestModel.setData(data);
        requestModel.setCommand(MqRobotCommond.TIREN.val());
        mqProducer.sendMessage(requestModel,exchageName,clientRoutingKey);
    }

    /**
     * 刷新群组操作
     */
    public static void refreshGroupInfo(String robotId) throws BusinessException, TimeoutException, IOException {
        _logger.info("refreshGroupInfo  robotId ==> "+robotId);
        MqRobotRequestModel requestModel = new MqRobotRequestModel();
        requestModel.setRobotId(robotId);
        Map<String,Object> data = new HashMap<>();
        data.put("group","");
        requestModel.setData(data);
        requestModel.setCommand(MqRobotCommond.REFRESH_GROUP.val());
        mqProducer.sendMessage(requestModel,exchageName,clientRoutingKey);

    }

    /**
     * 刷新授权群组
     * @param robotId
     * @throws BusinessException
     * @throws TimeoutException
     * @throws IOException
     */
    public static void refreshGroupInfo(String robotId,String groupCommand) throws BusinessException, TimeoutException, IOException {
        _logger.info("refreshGroupInfo  robotId ==> "+robotId);
        MqRobotRequestModel requestModel = new MqRobotRequestModel();
        requestModel.setRobotId(robotId);
        Map<String,Object> data = new HashMap<>();
        data.put("group",groupCommand);
        requestModel.setData(data);
        requestModel.setCommand(MqRobotCommond.REFRESH_GROUP.val());
        mqProducer.sendMessage(requestModel,exchageName,clientRoutingKey);

    }

}
