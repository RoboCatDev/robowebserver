package com.robo.server.web.statistics;

import com.common.base.exception.BusinessException;
import com.robo.server.web.entity.statistics.StatisticsCommandDto;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

/**
 *
 * @user jianghaoming 
 * @date 17/8/4  下午3:15
 *
 */
@Service
public interface StatisticsCommandService {

    /**
     * 保存统计信息
     * @param dto
     * @return
     * @throws DataAccessException
     * @throws BusinessException
     * @throws Exception
     */
    public StatisticsCommandDto save(StatisticsCommandDto dto) throws DataAccessException,BusinessException,Exception;

}
