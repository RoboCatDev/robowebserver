package com.robo.server.web.setting.impl;

import com.common.base.CommConstants;
import com.common.base.exception.BusinessException;
import com.common.utils.CheckUtils;
import com.common.utils.CommonUtils;
import com.robo.server.web.em.setting.RoboSettingContentStatusEnum;
import com.robo.server.web.em.setting.RoboSettingContentTypeEnum;
import com.robo.server.web.em.setting.RoboSwitchTypeEnum;
import com.robo.server.web.entity.setting.RoboChatDictDto;
import com.robo.server.web.entity.setting.RoboSettingContentDto;
import com.robo.server.web.entity.setting.RoboSwitchStatsticsDto;
import com.robo.server.web.message.RoboMessage;
import com.robo.server.web.message.UserMessage;
import com.robo.server.web.repository.setting.RoboChatDictRepository;
import com.robo.server.web.repository.setting.RoboSettingContentRepository;
import com.robo.server.web.repository.setting.RoboSwitchStatsticsRepository;
import com.robo.server.web.setting.RoboSettingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by conor on 2017/8/15.
 */
@Service
public class RoboSettingServiceImpl implements RoboSettingService{

    private static final Logger _logger = LoggerFactory.getLogger(RoboSettingServiceImpl.class);

    @Autowired
    private RoboSwitchStatsticsRepository roboSwitchStatsticsRepository;

    @Autowired
    private RoboSettingContentRepository roboSettingContentRepository;

    @Autowired
    private RoboChatDictRepository roboChatDictRepository;


    /**
     * 更新/保存机器人相关操作开关状态及切换次数统计
     * @param roboSwitchStatsticsDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public RoboSwitchStatsticsDto updateRoboFuncStatstics(RoboSwitchStatsticsDto roboSwitchStatsticsDto) throws BusinessException,DataAccessException,Exception{

        _logger.info("更新/保存机器人相关操作开关状态及切换次数统计 roboSwitchStatsticsDto===>"+roboSwitchStatsticsDto.toLogger());

        CheckUtils.checkParamNull(roboSwitchStatsticsDto.getUserId(), UserMessage.FieldVALID.USERID);
        CheckUtils.checkParamNull(roboSwitchStatsticsDto.getUin(), RoboMessage.UIN);
        if(!RoboSwitchTypeEnum.getAllMap().containsKey(roboSwitchStatsticsDto.getSwitchType())){
            throw new BusinessException(RoboMessage.SWITCH_TYPE_NOT_EXIST);
        }

        RoboSwitchStatsticsDto dataDto = new RoboSwitchStatsticsDto();
        dataDto.setUserId(roboSwitchStatsticsDto.getUserId());
        dataDto.setUin(roboSwitchStatsticsDto.getUin());
        dataDto.setSwitchType(roboSwitchStatsticsDto.getSwitchType());
        RoboSwitchStatsticsDto resultDto = roboSwitchStatsticsRepository.findOne(Example.of(dataDto));
        if(resultDto!=null){
            //说明是更新，将主键ID存到dto中
            if(!CommonUtils.isBlank(resultDto.getId())){
                roboSwitchStatsticsDto.setId(resultDto.getId());
            }
            //判断开关状态 0:open 1:close;开关状态发生切换时，切换次数+1
            if(!resultDto.getSwitchStatus().equals(roboSwitchStatsticsDto.getSwitchStatus())){
                if(!CommonUtils.isBlank(resultDto.getSwitchNum())){
                    //切换总次数+1
                    int switchNum = Integer.parseInt(resultDto.getSwitchNum()) + 1;
                    roboSwitchStatsticsDto.setSwitchNum(switchNum + "");
                }else{
                    roboSwitchStatsticsDto.setSwitchNum("1");
                }
            }else{
                if(!CommonUtils.isBlank(resultDto.getSwitchNum())){
                    int switchNum = Integer.parseInt(resultDto.getSwitchNum());
                    roboSwitchStatsticsDto.setSwitchNum(switchNum + "");
                }else{
                    roboSwitchStatsticsDto.setSwitchNum("0");
                }
            }
        }else{
            //切换总次数+1
            roboSwitchStatsticsDto.setSwitchNum("0");
        }
        return roboSwitchStatsticsRepository.save(roboSwitchStatsticsDto);
    }

    /**
     * 查询机器人相关操作开关状态及开关切换次数
     * @param userId 用户Id
     * @param uin 微信uin
     * @param switchType 开关类型 0：切换 1：自动回复 2：机器人当前状态 4：踢人 5：欢迎 6：规定 7：数据分析 8：聊天匹配 9：聊天模糊匹配
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public RoboSwitchStatsticsDto getRoboFuncStatstics(final String userId,final String uin,final String switchType) throws BusinessException,DataAccessException,Exception{

        _logger.info("userId=["+userId+"] uin=["+uin+"] switchType=["+switchType+"]");

        CheckUtils.checkParamNull(userId, UserMessage.FieldVALID.USERID);
        CheckUtils.checkParamNull(uin, RoboMessage.UIN);
        if(!RoboSwitchTypeEnum.getAllMap().containsKey(switchType)){
            throw new BusinessException(RoboMessage.SWITCH_TYPE_NOT_EXIST);
        }

        RoboSwitchStatsticsDto dataDto = new RoboSwitchStatsticsDto();
        dataDto.setUserId(userId);
        dataDto.setUin(uin);
        dataDto.setSwitchType(switchType);

        return roboSwitchStatsticsRepository.findOne(Example.of(dataDto));

    }

    /**
     * 查询所有的机器人开关配置信息
     * @param userId 用户ID
     * @param uin 微信uin
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public List<RoboSwitchStatsticsDto> getAllRoboSwitchStaticsList(final String userId,final String uin) throws BusinessException,DataAccessException,Exception{

        _logger.info("userId=["+userId+"] uin=["+uin+"]");

        CheckUtils.checkParamNull(userId, UserMessage.FieldVALID.USERID);
        CheckUtils.checkParamNull(uin, RoboMessage.UIN);

        RoboSwitchStatsticsDto dataDto = new RoboSwitchStatsticsDto();
        dataDto.setUserId(userId);
        dataDto.setUin(uin);

        return roboSwitchStatsticsRepository.findAll(Example.of(dataDto),new Sort(Sort.Direction.DESC,"createTime"));

    }

    /**
     * 新增保存机器人聊天回复自动匹配信息
     * @param roboChatDictDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public RoboChatDictDto saveRoboChatDict(RoboChatDictDto roboChatDictDto) throws BusinessException,DataAccessException,Exception{

        _logger.info("新增保存机器人聊天回复自动匹配信息 roboChatDictDto===>"+roboChatDictDto.toLogger());

        CheckUtils.checkParamNull(roboChatDictDto.getUserId(), UserMessage.FieldVALID.USERID);
        CheckUtils.checkParamNull(roboChatDictDto.getUin(), RoboMessage.UIN);

        return roboChatDictRepository.save(roboChatDictDto);

    }

    /**
     * 查询机器人聊天回复自动匹配信息列表
     * @param userId 用户ID
     * @param uin 微信uin
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public List<RoboChatDictDto> getRoboChatDictList(final String userId, final String uin) throws BusinessException,DataAccessException,Exception{

        _logger.info("userId=["+userId+"] uin=["+uin+"]");
        CheckUtils.checkParamNull(userId, UserMessage.FieldVALID.USERID);
        CheckUtils.checkParamNull(uin, RoboMessage.UIN);

        RoboChatDictDto dictDto = new RoboChatDictDto();
        dictDto.setUserId(userId);
        dictDto.setUin(uin);

        return roboChatDictRepository.findAll(Example.of(dictDto),new Sort(Sort.Direction.DESC,"createTime"));
    }

    /**
     * 更新机器人聊天回复自动匹配信息
     * @param id 聊天匹配ID
     * @param chatKey 聊天内容
     * @param chatValue 聊天匹配内容
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public int updateRoboChatDictById(final String id, final String chatKey,final String chatValue) throws BusinessException,DataAccessException,Exception{

        _logger.info("id=["+id+"] chatKey=["+chatKey+"] chatValue=["+chatValue+"]");

        CheckUtils.checkParamNull(id,RoboMessage.CHAT_DICT_ID);

        return roboChatDictRepository.updateRoboChatDictById(id,chatKey,chatValue);

    }

    /**
     * 根据id查询
     * @param id
     * @return
     */
    public RoboChatDictDto getRoboChatDictById(final String id) throws BusinessException,DataAccessException,Exception{
        _logger.info("id = ["+id+"]");
        CheckUtils.checkParamNull(id,RoboMessage.CHAT_DICT_ID);
        return roboChatDictRepository.findOne(id);
    }

    /**
     * 删除机器人聊天回复自动匹配信息
     * @param id 聊天匹配ID
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public void deleteRoboChatDictById(final String id) throws BusinessException,DataAccessException,Exception{

        _logger.info("id=["+id+"]");

        CheckUtils.checkParamNull(id,RoboMessage.CHAT_DICT_ID);

       roboChatDictRepository.delete(id);
    }


    /**
     * 新增保存机器人设置信息（例如欢迎语，规定等）
     * @param roboSettingContentDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public RoboSettingContentDto saveRoboSettingContent(RoboSettingContentDto roboSettingContentDto) throws BusinessException,DataAccessException,Exception{

        _logger.info("新增保存机器人设置信息（例如欢迎语，规定等）roboSettingContentDto===>"+roboSettingContentDto.toLogger());

        CheckUtils.checkParamNull(roboSettingContentDto.getUserId(), UserMessage.FieldVALID.USERID);
        CheckUtils.checkParamNull(roboSettingContentDto.getUin(), RoboMessage.UIN);
        if(!RoboSettingContentTypeEnum.getAllMap().containsKey(roboSettingContentDto.getType())){
            throw new BusinessException(RoboMessage.SETTING_TYPE_NOT_EXIST);
        }

        //首先需要判断类型是否为99：图灵机器人key（同一个用户只能录入一个图灵机器人key）、98：切换（人工与机器人之间的切换）
//        if(RoboSettingContentTypeEnum.tulingKey.getValue().equals(roboSettingContentDto.getType()) ||
//                RoboSettingContentTypeEnum.qieHuan.getValue().equals(roboSettingContentDto.getType())){
            //先判断是否已录入保存过，从而进行更新/保存
            RoboSettingContentDto dataDto = new RoboSettingContentDto();
            dataDto.setUserId(roboSettingContentDto.getUserId());
            dataDto.setType(roboSettingContentDto.getType());
            dataDto = roboSettingContentRepository.findOne(Example.of(dataDto));
            if(dataDto!=null&&!CommonUtils.isBlank(dataDto.getId())){
                roboSettingContentDto.setId(dataDto.getId());
            }
            roboSettingContentDto.setStatus(RoboSettingContentStatusEnum.OPEN.getValue());
//        }else{
//            roboSettingContentDto.setStatus(RoboSettingContentStatusEnum.CLOSE.getValue());
//        }

        return roboSettingContentRepository.save(roboSettingContentDto);
    }

    /**
     * 查询保存机器人设置提示语信息列表（例如欢迎语，规定等）
     * @param userId 用户ID
     * @param uin 微信uin
     * @param type 类型 0：踢人 1：欢迎 2：规定 98:切换 99：图灵机器人Key
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public List<RoboSettingContentDto> getRoboSettingContentList(final String userId,final String uin,final String type) throws BusinessException,DataAccessException,Exception{

        _logger.info("userId=["+userId+"] uin=["+uin+"] type=["+type+"]");

        CheckUtils.checkParamNull(userId, UserMessage.FieldVALID.USERID);
        CheckUtils.checkParamNull(uin, RoboMessage.UIN);
        if(!RoboSettingContentTypeEnum.getAllMap().containsKey(type)){
            throw new BusinessException(RoboMessage.SETTING_TYPE_NOT_EXIST);
        }

        RoboSettingContentDto dataDto = new RoboSettingContentDto();
        dataDto.setUserId(userId);
        dataDto.setUin(uin);
        dataDto.setType(type);

        return roboSettingContentRepository.findAll(Example.of(dataDto),new Sort(Sort.Direction.DESC,"createTime"));
    }

    /**
     * 查询保存机器人设置提示语信息列表（例如欢迎语，规定等）
     * @param userId 用户ID
     * @param uin 微信uin
     * @param status 状态 0：待启用 1：启用 为空时，查询所有
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public List<RoboSettingContentDto> getAllRoboSettingContentList(final String userId,final String uin,final String status) throws BusinessException,DataAccessException,Exception{

        _logger.info("userId=["+userId+"] uin=["+uin+"] status=["+status+"]");

        CheckUtils.checkParamNull(userId, UserMessage.FieldVALID.USERID);
        CheckUtils.checkParamNull(uin, RoboMessage.UIN);

        RoboSettingContentDto dataDto = new RoboSettingContentDto();
        dataDto.setUserId(userId);
        dataDto.setUin(uin);
        if(!CommonUtils.isBlank(status)){
            dataDto.setStatus(status);
        }
        return roboSettingContentRepository.findAll(Example.of(dataDto),new Sort(Sort.Direction.DESC,"createTime"));

    }

    /**
     * 根据ID，更新机器人提示语设置启用状态
     * @param id 机器人设置ID
     * @param status 状态 0：待启用 1：启用
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public int updateRoboSettingContentStatusById(final String id,final String status) throws BusinessException,DataAccessException,Exception{

        _logger.info("id=["+id+"] status=["+status+"]");
        CheckUtils.checkParamNull(id, RoboMessage.SETTING_ID);
        if(!RoboSettingContentStatusEnum.getAllMap().containsKey(status)){
            throw new BusinessException(RoboMessage.SETTING_CONTENT_TYPE_NOT_EXIST);
        }

        RoboSettingContentDto roboSettingContentDto = roboSettingContentRepository.findOne(id);
        if(roboSettingContentDto==null){
           throw new BusinessException(CommConstants.NOT_FUND);
        }

        final String userId = roboSettingContentDto.getUserId();
        final String uin = roboSettingContentDto.getUin();

        if(status.equals(RoboSettingContentStatusEnum.OPEN.getValue())){
            //启用之前，首先将用户+机器人名下的全部机器人设置信息状态置为待启用
            roboSettingContentRepository.updateAllRoboSettingContentStatus(userId,uin,RoboSettingContentStatusEnum.CLOSE.getValue());
        }

        return roboSettingContentRepository.updateRoboSettingContentStatusById(id,status);

    }


    public RoboSettingContentDto getRoboSettingContentById(final String id) throws BusinessException,DataAccessException,Exception{
        _logger.info("id=["+id+"]");
        CheckUtils.checkParamNull(id, RoboMessage.SETTING_ID);
        return roboSettingContentRepository.findOne(id);

    }



    /**
     * 根据ID，删除机器人提示语设置信息
     * @param id 机器人设置ID
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public void deleteRoboSettingContentById(final String id) throws BusinessException,DataAccessException,Exception{

        _logger.info("id=["+id+"]");
        CheckUtils.checkParamNull(id, RoboMessage.SETTING_ID);

        roboSettingContentRepository.delete(id);

    }

    /**
     * 根据ID，删除机器人提示语设置信息
     * @param id 机器人设置ID
     * @param content 提示语内容
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public int updateRoboSettingContentById(final String id,final String content) throws BusinessException,DataAccessException,Exception{

        _logger.info("id=["+id+"]");
        CheckUtils.checkParamNull(id, RoboMessage.SETTING_ID);

        return roboSettingContentRepository.updateRoboSettingContentById(id,content);

    }



}
